import logging
import subprocess
import os
import sys
import re
import time
import datetime
import argparse

# To be executed on lxplus, or on a local machine with the proper paths mounted

MAX_PROCS = 2   # maximum number of processes to run at the same time
CHECK_RATE = 30  # seconds between file checks
# path to the folder where all the waveform files are
WFM_DIR_PATH = '/afs/cern.ch/user/a/abellora/workarea/Work/TIMESPOT/timespot-tb-sps-2024-04/raw_data'
# path to the folder containing the analysis_scripts and data sub-folders (where trees are stored).
ANALYSIS_DIR_PATH = '/afs/cern.ch/user/a/abellora/workarea/Work/TIMESPOT/timespot-tb-sps-2024-04/analysis_area'
# path to the folder where all the logs are
LOG_DIR_PATH = '/afs/cern.ch/user/a/abellora/workarea/Work/TIMESPOT/timespot-tb-sps-2024-04/TS_macro/logs'

def extractDateTime(name):
    """Extract the date string from the file name in format
    YYYY-MM-DD and HHmmss. 

    :param name: name from which to extract the date
    :type name: string
    """
    # Match a 4-digit year, followed by a dash, followed by a 2-digit month,
    # followed by a dash, followed by a 2-digit day.
    logging.debug('Parsing name: {}'.format(name))
    date = re.search(r'\d{4}-\d{2}-\d{2}', name)
    if date is None:
        logging.error('Invalid date string: {}'.format(name))
        return None, None
    logging.debug('Date: {}'.format(date.group(0)))
    # Match a 2-digit hour, followed by a 2-digit minute, followed by a 2-digit second.
    time = re.search(r'\d{6}', name)
    if time is None:
        logging.error('Invalid time string: {}'.format(name))
        return None, None
    # Check that this is compatible with a time (hours are less than 24, etc.)
    if int(time.group(0)[:2]) > 23 or int(time.group(0)[2:4]) > 59 or int(time.group(0)[4:]) > 59:
        logging.error('Invalid time string : {}'.format(time.group(0)))
        return None, None
    logging.debug('Time: {}'.format(time.group(0)))
    return date.group(0), time.group(0)


def indexTreeFiles(path=ANALYSIS_DIR_PATH+'/data'):
    """Return a
     list of the date-time strings of the files in the directory.

    :param path: path to the folder where all the tree files are, defaults to '/afs/cern.ch/user/a/abellora/workarea/Work/TIMESPOT/timespot-tb-sps-2024-04/analysis_area/data'
    :type path: str, optional
    :return: list of the date-time strings of the files in the directory
    :rtype: list
    """
    logging.debug('Watching destination dir: {}'.format(path))
    # List all files in the directory
    files = os.listdir(path)
    # Filter out only the files that are not root files
    files = [f for f in files if f.endswith('.root')]
    # Create the date-time list
    dateTimes = [extractDateTime(f)
                 for f in files if extractDateTime(f) != (None, None)]

    return set(dateTimes)


def indexWfmFiles(path=WFM_DIR_PATH):
    logging.debug('Watching source dir: {}'.format(path))
    # List all files in the directory
    files = os.listdir(path)
    # Filter out only the files that are not root files
    files = [f for f in files if f.endswith('Wfm.csv')]
    # Create the date-time list
    dateTimes = [extractDateTime(f)
                 for f in files if extractDateTime(f) != (None, None)]

    return set(dateTimes)


def treesToProcess(treeIdxs, wfmIdxs):
    """Return the list of waveform files that need to be processed.

    :param treeIdxs: list of the date-time strings of the tree files
    :type treeIdxs: list
    :param wfmIdxs: list of the date-time strings of the waveform files
    :type wfmIdxs: list
    :return: list of the date-time strings of the trees that need to be processed
    :rtype: list
    """
    return wfmIdxs - treeIdxs

def wfmToProcess(wfmIdx, path=WFM_DIR_PATH):
    """Return the path of the waveform to process

    :param wfmIdx: date-time index of the waveform to process
    :type wfmIdx: tuple
    """
    wfm_file = [f for f in os.listdir(
        path) if wfmIdx[0] in f and wfmIdx[1] in f and f.endswith('.Wfm.csv') and f.startswith('RefCurve')]
    if len(wfm_file) > 1:
        logging.error(
            'More than one waveform file found for index {}. Operate manually!'.format(wfmIdx))
        logging.error(
            'Files found: {}'.format(wfm_file)
        )
        input('Press any key to continue...')
        return None
    wfm_file = os.path.join(path, wfm_file[0])
    return wfm_file

def countWfms(wfmfile):
    """Count the number of waveforms in the file

    :param wfmfile: path to the waveform file
    :type wfmfile: str
    """
    # Each waveform starts from a line where there are no commas.
    # Count the number of lines that do not contain commas, reading one line at a time
    with open(wfmfile, 'r') as f:
        n_wfms = sum(1 for line in f if ',' not in line)
    return n_wfms

def expectedWfms(wfmfile):
    """Return the expected number of waveforms in the file from the .csv config dump

    :param wfmfile: path to the waveform file
    :type wfmfile: str
    """
    # The expected number of waveforms is given in a line like this:
    # NumberOfAcquisitions:30000:

    config_wfmfile  = wfmfile.replace('Wfm.csv','csv')
    with open(config_wfmfile, 'r') as f:
        for line in f:
            if line.startswith('NumberOfAcquisitions'):
                exp_wfms = int(line.split(':')[1])
                return exp_wfms

def wfmIsReady(wfm_file):
    """Check if the waveform file is ready to be processed.

    :param wfm_file: path to the waveform file
    :type wfm_file: str
    """
    n_wfms = countWfms(wfm_file)
    n_wfms_expected = expectedWfms(wfm_file)
    logging.debug('Number of waveforms in file: {} ({} expected)'.format(n_wfms,n_wfms_expected))
    if n_wfms == n_wfms_expected:
        return True
    logging.debug('Waveform file is not ready to be processed')
    return False


def subProcWfm(wfm_file,
               analysis_area=ANALYSIS_DIR_PATH,
               logdir=LOG_DIR_PATH):
    """Run the waveform processing macro on the waveform file.

    :param wfm_file: path to the waveform file
    :type wfm_file: str
    """
    logging.debug('Processing waveform: {}'.format(wfm_file))
    command = 'root -l -b -e \'gROOT->LoadMacro("analysis_scripts/AnalisiWaveform.C");AnalisiWaveform(false, "{}",0,500000)\' -q'.format(
        wfm_file)
    logging.info('Command: {}'.format(command))
    # Run the subprocess asynchronously and return its handle
    # Extract the wfm file name without the extension
    wfm_file = os.path.basename(wfm_file).split('.')[0]
    if not os.path.exists(logdir):
        os.makedirs(logdir)
    stdout_file = os.path.join(logdir, wfm_file+'.out')
    stderr_file = os.path.join(logdir, wfm_file+'.err')
    with open(stdout_file, "w") as stdout_f, open(stderr_file, "w") as stderr_f:
        return subprocess.Popen(command, shell=True, cwd=analysis_area, stdout=stdout_f, stderr=stderr_f)


def parseArgs():
    parser = argparse.ArgumentParser(
        description='Monitor waveform files produced by the oscilloscope and process them into trees.',
        epilog='Made by A. Bellora - 02/05/2024')
    parser.add_argument('--debug', action='store_true',
                        default=False, help='Enable debug mode.')
    parser.add_argument('--check-rate', type=int, default=CHECK_RATE,
                        help='Seconds between file checks. Default: 30')
    parser.add_argument('--max-procs', type=int, default=MAX_PROCS,
                        help='Maximum number of processes to run at the same time. Default: 2')
    parser.add_argument('--analysis-dir', type=str, default=ANALYSIS_DIR_PATH,
                        help='Path to the folder containing the analysis_scripts and data sub-folders (where trees are stored). Default: {}'.format(ANALYSIS_DIR_PATH))
    parser.add_argument('--log-dir', type=str, default=LOG_DIR_PATH,
                        help='Path to the folder where all the logs are. Default: {}'.format(LOG_DIR_PATH))
    parser.add_argument('--wfms-dir', type=str, default=WFM_DIR_PATH,
                        help='Path to the folder where all the waveform files are. Default: {}'.format(ANALYSIS_DIR_PATH))
    return parser.parse_args()


if __name__ == '__main__':
    args = parseArgs()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s - [%(levelname)s] %(message)s')
        logging.info('Debug mode enabled')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - [%(levelname)s] %(message)s')
    subprocesses = []
    blacklist = []
    n_subprocesses = 0
    try:
        while True:
            # Monitor waveform to process
            logging.info('Monitoring waveform to process ({} subprocesses running)'.format(
                n_subprocesses))
            treeIdxs = indexTreeFiles(args.analysis_dir+'/data')
            logging.debug('Found {} tree files'.format(len(treeIdxs)))
            wfmIdxs = indexWfmFiles(args.wfms_dir)
            logging.debug('Found {} waveform files'.format(len(wfmIdxs)))
            toProcess = treesToProcess(treeIdxs, wfmIdxs)
            logging.info(
                'Found {} waveform files to process (will submit if ready)'.format(len(toProcess)))
            for wfmIdx in toProcess:
                if wfm_file := wfmToProcess(wfmIdx, args.wfms_dir):
                    if wfm_file in blacklist:
                        continue
                    if n_subprocesses < MAX_PROCS and wfm_file not in [p[0] for p in subprocesses]:
                        if wfmIsReady(wfm_file):
                            n_wfms = countWfms(wfm_file)
                            logging.info(
                                'Submitting waveform file processing: {} ({} waveforms)'.format(wfm_file,n_wfms))
                            subprocesses.append(
                                (wfm_file, subProcWfm(wfm_file, args.analysis_dir, args.log_dir), datetime.datetime.now()))
                            n_subprocesses += 1

            # Check if any subprocess has finished
            for i, p_tup in enumerate(subprocesses):
                wfm_file, p, p_start_time = p_tup
                if p.poll() is not None:
                    end_time = datetime.datetime.now()
                    logging.info('Finished processing waveform file: {} ({} s)'.format(
                        wfm_file, (end_time - p_start_time).total_seconds()))
                    exit_code = p.returncode
                    if exit_code != 0:
                        logging.error('Error processing waveform file: {}. Exit code {}. Operate manually!'.format(
                            wfm_file, exit_code))
                        blacklist.append(wfm_file)
                        input('Press any key to continue...')
                    subprocesses.pop(i)
                    n_subprocesses -= 1

            time.sleep(CHECK_RATE)

    # Catch any keyboard interrupt
    except KeyboardInterrupt:
        logging.warning(
            'Keyboard interrupt detected. Killing all processes...')
        for wfm_file, p, _ in subprocesses:
            logging.info('Killing processing for file: {}. Delete the output trees manually to reproduce them at the next execution'.format(wfm_file))
            p.kill()
        sys.exit(0)
