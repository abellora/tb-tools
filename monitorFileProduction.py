import logging
import subprocess
import os
import sys
import re
import time
import datetime
from pprint import pprint

MAX_PROCS = 2

def extractDateTime(name):
    """Extract the date string from the file name in format
    YYYY-MM-DD and HHmmss. 

    :param name: name from which to extract the date
    :type name: string
    """
    # Match a 4-digit year, followed by a dash, followed by a 2-digit month,
    # followed by a dash, followed by a 2-digit day.
    logging.debug('Parsing name: {}'.format(name))
    date = re.search(r'\d{4}-\d{2}-\d{2}', name)
    logging.debug('Date: {}'.format(date.group(0)))
    # Match a 2-digit hour, followed by a 2-digit minute, followed by a 2-digit second.
    time = re.search(r'\d{6}', name)
    # Check that this is compatible with a time (hours are less than 24, etc.)
    if int(time.group(0)[:2]) > 23 or int(time.group(0)[2:4]) > 59 or int(time.group(0)[4:]) > 59:
        logging.error('Invalid time string : {}'.format(time.group(0)))
        return None, None
    logging.debug('Time: {}'.format(time.group(0)))
    return date.group(0), time.group(0)

def indexWfmFiles(path='/eos/user/m/mverdogl/TestBeam_TimeSPOT_2024/'):
    logging.debug('Watching source dir: {}'.format(path))
    # List all files in the directory
    files = os.listdir(path)
    # Filter out only the files that are not root files
    files = [f for f in files if f.endswith('Wfm.csv')]
    # Create the date-time list
    dateTimes = [extractDateTime(f)
                 for f in files if extractDateTime(f) != (None, None)]

    return set(dateTimes)

def wfmToProcess(wfmIdx, path='/eos/user/m/mverdogl/TestBeam_TimeSPOT_2024/'):
    """Return the path of the waveform to process

    :param wfmIdx: date-time index of the waveform to process
    :type wfmIdx: tuple
    """
    wfm_file = [f for f in os.listdir(
        path) if wfmIdx[0] in f and wfmIdx[1] in f and f.endswith('.Wfm.csv')]
    if len(wfm_file) > 1:
        logging.error(
            'More than one waveform file found for index {}. Operate manually!'.format(wfmIdx))
        input('Press any key to continue...')
        return None
    wfm_file = os.path.join(path, wfm_file[0])
    return wfm_file

def countWfms(wfmfile):
    """Count the number of waveforms in the file

    :param wfmfile: path to the waveform file
    :type wfmfile: str
    """
    # Each waveform starts from a line where there are no commas. 
    # Count the number of lines that do not contain commas, reading one line at a time
    with open(wfmfile, 'r') as f:
        n_wfms = sum(1 for line in f if ',' not in line)
    return n_wfms
        
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - [%(levelname)s] %(message)s')
    subprocesses = []
    blacklist = []
    n_subprocesses = 0
    try:
        # Monitor waveform to process
        logging.info('Monitoring waveform to process')
        wfmIdxs_0 = indexWfmFiles()
        logging.debug('Found {} waveform files at t_0'.format(len(wfmIdxs_0)))
        
        new_wfmIdxs = set()
        # while True:
        #     time.sleep(10)
        #     wfmIdxs = indexWfmFiles()
        #     logging.info('Checking for new waveform files ({} at t_0)'.format(len(wfmIdxs_0)))            
        #     new_wfmIdxs = wfmIdxs - wfmIdxs_0
        #     if len(new_wfmIdxs) > 0:
        #         break
        logging.info('Found {} new waveform files'.format(len(new_wfmIdxs)))
        # wfm_file = wfmToProcess(list(new_wfmIdxs)[0])
        wfm_file = '/eos/user/m/mverdogl/TestBeam_TimeSPOT_2024/RefCurve_2024-05-03_3_100514.Wfm.csv'
        logging.info('Monitoring waveform file: {}'.format(wfm_file))
        
        while True:
            last_mtime = datetime.datetime.fromtimestamp(os.path.getmtime(wfm_file))
            logging.info('Last modified time: {}'.format(last_mtime))
            logging.info('Number of waveforms in file: {}'.format(countWfms(wfm_file)))

            # Append it to a log file in /afs/cern.ch/work/a/abellora/Work/TIMESPOT/timespot-tb-sps-2024-04/TS_macro/logs
            with open('/afs/cern.ch/work/a/abellora/Work/TIMESPOT/timespot-tb-sps-2024-04/TS_macro/logs/mtime_log.txt', 'a') as f:
                f.write('{}\n'.format(last_mtime))
            
            time.sleep(15)
    # Catch any keyboard interrupt
    except KeyboardInterrupt:
        logging.warning(
            'Keyboard interrupt detected.')
        sys.exit(0)
