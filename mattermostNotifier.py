import logging
import argparse
import os
import sys
import re
import time
import requests
import pytz
import threading
import subprocess
import tkinter as tk
from datetime import datetime
from tkinter import messagebox

MM_WEBHOOK = 'https://mattermost.web.cern.ch/hooks/np7m7if4if8gfmcbqeybrqx54o'
# Path to the directory where waveform files are created
WFM_DIR_PATH = '/afs/cern.ch/user/a/abellora/workarea/Work/TIMESPOT/timespot-tb-sps-2024-04/raw_data'
MSG_INTERVAL = 30  # seconds between two mattermost messages
CHECK_INTERVAL = 30  # minimum seconds between two n_event checks
EVENTS_TO_COMPLETION = 500  # Number of events to completion
LOG_FILE = 'mattermostNotifier.log'  # Log file for the application


def extractDateTime(name):
    """Extract the date string from the file name.

    :param name: name from which to extract the date
    :type name: string
    :return: datetime object, date string, time string
    :rtype: tuple
    """
    # Match a 4-digit year, followed by a dash, followed by a 2-digit month,
    # followed by a dash, followed by a 2-digit day.
    logger.debug('Parsing name: {}'.format(name))
    date = re.search(r'\d{4}-\d{2}-\d{2}', name)
    if date is None:
        logger.warning('No date found in name: {}'.format(name))
        return None, None, None
    logger.debug('Date: {}'.format(date.group(0)))
    # Match a 2-digit hour, followed by a 2-digit minute, followed by a 2-digit second.
    time = re.search(r'\d{6}', name)
    if time is None:
        logger.warning('No time found in name: {}'.format(name))
        return None, None, None
    # Check that this is compatible with a time (hours are less than 24, etc.)
    if int(time.group(0)[:2]) > 23 or int(time.group(0)[2:4]) > 59 or int(time.group(0)[4:]) > 59:
        logger.error('Invalid time string : {}'.format(time.group(0)))
        return None, None
    # Parse into the datetime object
    dt = datetime.strptime(date.group(0) + ' ' +
                           time.group(0), '%Y-%m-%d %H%M%S')

    logger.debug('Date and time: {}'.format(dt))
    return dt, date.group(0), time.group(0)


def indexWfmFiles(path=WFM_DIR_PATH):
    """Index the waveform files in the directory

    :param path: path to the folder where wfms are stored, defaults to WFM_DIR_PATH
    :type path: string, optional
    :return: list of date-time tuples
    :rtype: set
    """
    logger.debug('Watching source dir: {}'.format(path))
    # List all files in the directory
    files = os.listdir(path)
    # Filter out only the files that are not root files
    files = [f for f in files if f.endswith('Wfm.csv')]
    # Create the date-time list
    dateTimes = [extractDateTime(f)
                 for f in files if extractDateTime(f) != (None, None,None)]

    return set(dateTimes)


def wfmFileName(wfmIdx, path=WFM_DIR_PATH):
    """Return the path of the waveform file

    :param wfmIdx: date-time index of the waveform to process
    :type wfmIdx: tuple
    :param path: path to the folder where wfms are stored, defaults to WFM_DIR_PATH
    :type path: string, optional
    """
    wfm_file = [f for f in os.listdir(
        path) if wfmIdx[0] in f and wfmIdx[1] in f and f.endswith('.Wfm.csv') and f.startswith('RefCurve')]
    if len(wfm_file) > 1:
        logger.error(
            'More than one waveform file found for index {}. Operate manually!'.format(wfmIdx))
        logger.error(
            'Files found: {}'.format(wfm_file)
        )
        input('Press any key to continue...')
        return None
    wfm_file = os.path.join(path, wfm_file[0])
    return wfm_file


def countWfms(wfmfile):
    """Count the number of waveforms in the file

    :param wfmfile: path to the waveform file
    :type wfmfile: str
    :return: number of waveforms
    :rtype: int
    """
    mode = 3  # 1: line by line, 2: wc command, 3: file size

    n_wfms = -1
    if mode == 1:
        # Each waveform starts from a line where there are no commas.
        # Count the number of lines that do not contain commas, reading one line at a time
        # This is very slow and not recommended for large files, but it's the most reliable way
        with open(wfmfile, 'r') as f:
            n_wfms = sum(1 for line in f if ',' not in line)
    elif mode == 2:
        # Use the wc command to count the number of lines in the file and divide it by 1001
        # which is the number of lines per waveform expected. This is faster but less reliable
        result = subprocess.run(
            ['wc', '-l', wfmfile], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        if result.returncode != 0:
            logger.error('Error counting waveforms: {}'.format(result.stderr))
            return -1
        n_wfms = int(result.stdout.split()[0]) // expectedPointsInWfm(wfmfile)
    elif mode == 3:
        # Compute the number of waveforms by dividing the file size by the expected avg event size
        # The avg event size has been (under)estimated from 20k events
        avg_evt_size = 56800  # bytes
        file_size = os.path.getsize(wfmfile)
        n_wfms = file_size // avg_evt_size

    return n_wfms


def expectedWfms(wfmfile):
    """Return the expected number of waveforms in the file from the .csv config dump

    :param wfmfile: path to the waveform file
    :type wfmfile: str
    :return: expected number of waveforms
    :rtype: int
    """
    # The expected number of waveforms is given in a line like this:
    # NumberOfAcquisitions:30000:

    config_wfmfile = wfmfile.replace('Wfm.csv', 'csv')
    with open(config_wfmfile, 'r') as f:
        for line in f:
            if line.startswith('NumberOfAcquisitions'):
                exp_wfms = int(line.split(':')[1])
                return exp_wfms


def expectedPointsInWfm(wfmfile):
    """Return the expected number of points in the wfm from the .csv config dump

    :param wfmfile: path to the waveform file
    :type wfmfile: str
    :return: expected number of points in wfm
    :rtype: int
    """
    # The expected number of waveforms is given in a line like this:
    # NumberOfAcquisitions:30000:

    config_wfmfile = wfmfile.replace('Wfm.csv', 'csv')
    with open(config_wfmfile, 'r') as f:
        for line in f:
            if line.startswith('RecordLength'):
                exp_wfms = int(line.split(':')[1])
                return exp_wfms


def sendAlarm(dt, pretext, text, mm_webhook=MM_WEBHOOK):
    """Send an alarm to a mattermost channel

    :param dt: datetime of the alarm
    :type dt: datetime
    :param pretext: text to display before the message
    :type pretext: string
    :param text: text of the message
    :type text: string
    :param mm_webhook: incoming webhook to the channel, defaults to MM_WEBHOOK
    :type mm_webhook: string, optional
    """
    color = '#ff9100'

    text = dt.strftime('%Y-%m-%d %H:%M:%S (%Z)')+' - '+text

    json_data = {
        "attachments": [
            {"color": color, "text": text, "pretext": pretext}
        ]
    }
    requests.post(url=mm_webhook, json=json_data)


def parseArgs():
    """Parse the command line arguments

    :return: parsed arguments
    :rtype: argparse.Namespace
    """
    parser = argparse.ArgumentParser(
        description='Monitor a directory for waveform files and send alarms to a mattermost channel when the number of events is almost reached.',
        epilog='Made by A. Bellora - 04/05/2024')
    parser.add_argument('--debug', help='Enable debug mode',
                        action='store_true')
    parser.add_argument(
        '--webhook', help='The webhook to use', default=MM_WEBHOOK)
    parser.add_argument(
        '--wfm-dir', help='The directory to watch for Wfm files', default=WFM_DIR_PATH)
    parser.add_argument(
        '--msg-interval', help='The interval to spam the messages', default=MSG_INTERVAL)
    parser.add_argument(
        '--check-interval', help='The interval to check the number of events', default=CHECK_INTERVAL)
    parser.add_argument(
        '--events-to-completion', help='The number of events to completion', default=EVENTS_TO_COMPLETION)
    parser.add_argument(
        '--log-file', help='The log file to write to', default=LOG_FILE)
    return parser.parse_args()


def close_popup(popup):
    """Close the popup window

    :param popup: Tk popup object
    :type popup: Tk
    """
    popup.destroy()


def stopSpam(spam):
    """Interrupt message spamming by stopping the thread

    :param spam: thread object
    :type spam: threading.Thread
    """
    logger.info('Stopping spam')
    global spam_messages
    spam_messages = False
    spam.join()


def rearm():
    """Rearm the alarm system by resetting the global variables
    """
    global found_file_to_monitor
    global spam_messages
    spam_messages = False
    found_file_to_monitor = False
    logger.info('Rearming alarm system')


def on_closing(spam, popup):
    """Actions taken when the popup is closed

    :param spam: thread object
    :type spam: threading.Thread
    :param popup: Tk popup object
    :type popup: Tk
    """
    logger.warning(
        'Popup closed with no action. Alarms will be stopped and the application will exit')
    global spam_messages
    spam_messages = False
    spam.join()
    close_popup(popup)
    sys.exit()


def showPopup(spam):
    """Show a popup window that allows to stop the alarm and rearm it

    :param spam: thread object
    :type spam: threading.Thread
    """
    popup = tk.Tk()

    width = 300
    height = 100
    x_offset = (popup.winfo_screenwidth() - width) // 2
    y_offset = (popup.winfo_screenheight() - height) // 2
    popup.geometry(f"{width}x{height}+{x_offset}+{y_offset}")

    popup.title("ALARM!")
    popup.protocol("WM_DELETE_WINDOW", lambda: on_closing(spam, popup))

    label = tk.Label(popup, text="Waveform file is almost ready.")
    label.pack(pady=10)

    button1 = tk.Button(popup, text="Stop alarm",
                        command=lambda: [stopSpam(spam)])
    button1.pack(side="left", padx=10)

    button2 = tk.Button(popup, text="Rearm alarm", command=lambda: [
                        rearm(), close_popup(popup)])
    button2.pack(side="right", padx=10)

    popup.mainloop()


def spamAlarms(webhook, wfmToMonitor, interval):
    """Function to be run in a separate thread that sends alarms to the mattermost channel

    :param webhook: mattermost webhook
    :type webhook: string
    :param wfmToMonitor: wfm file 
    :type wfmToMonitor: string
    :param interval: interval between two messages in seconds
    :type interval: int
    """
    global spam_messages
    global last_alarm_dt
    spam_counter = 0

    while spam_messages:
        now = datetime.now(tz=pytz.timezone('Europe/Paris'))

        if 'last_alarm_dt' not in globals():
            preText = 'WAKE UP!'
            text = '**Waveform file is almost ready:** {}'.format(wfmToMonitor)
            spam_counter += 1
            logger.debug('Spamming... ({})'.format(spam_counter))
            sendAlarm(now, preText, text, webhook)
            last_alarm_dt = now
            time.sleep(0.1)
        elif int((now-last_alarm_dt).seconds)> int(interval):
            preText = 'WAKE UP!'
            text = '**Waveform file is almost ready:** {}'.format(wfmToMonitor)
            spam_counter += 1
            logger.debug('Spamming... ({})'.format(spam_counter))
            sendAlarm(now, preText, text, webhook)
            last_alarm_dt = now
            time.sleep(0.1)


if __name__ == '__main__':
    args = parseArgs()
    logger = logging.getLogger('')
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(args.log_file)
    file_handler.setFormatter(formatter)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)
    if args.debug:
        logger.setLevel(logging.DEBUG)
        logger.info('Debug mode enabled')
    else:
        logger.setLevel(logging.INFO)

    logger.debug('Webhook: {}'.format(args.webhook))
    logger.debug('Wfm dir: {}'.format(args.wfm_dir))
    logger.debug('Msg interval: {}'.format(args.msg_interval))

    found_file_to_monitor = False
    spam_messages = False

    wfmToMonitor = None
    try:
        while True:
            while found_file_to_monitor is False:
                dateTimes = indexWfmFiles(args.wfm_dir)
                # Extract the latest file
                latest = max(dateTimes, key=lambda x: x[0])
                wfmToMonitor = wfmFileName(
                    (latest[1], latest[2]), args.wfm_dir)
                found_file_to_monitor = True
                logger.info('Monitoring file: {}'.format(wfmToMonitor))

            while spam_messages is False:
                t_start = datetime.now(tz=pytz.timezone('Europe/Paris'))
                n_wfms = countWfms(wfmToMonitor)
                exp_wfms = expectedWfms(wfmToMonitor)
                logger.info(
                    'Number of waveforms in file: {} (expected {})'.format(n_wfms, exp_wfms))
                if exp_wfms - n_wfms < 500:
                    logger.info(
                        'Waveform target almost reached. Initiate spamming')
                    spam_messages = True
                else:
                    # Set a ~30 seconds check interval
                    if int((datetime.now(tz=pytz.timezone('Europe/Paris'))-t_start).seconds) < args.check_interval:
                        time.sleep(
                            args.check_interval - (datetime.now(tz=pytz.timezone('Europe/Paris'))-t_start).seconds)

            while spam_messages:

                spam = threading.Thread(target=spamAlarms, args=(
                    args.webhook, wfmToMonitor, args.msg_interval))
                spam.start()

                # Enter in the popup loop
                showPopup(spam)

    except KeyboardInterrupt:
        logger.info('Exiting...')

    logger.info('Monitoring file: {}'.format(wfmToMonitor))
