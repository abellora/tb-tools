void plot_RF_t(TH1F* RF_tzero, TString filename){

  TCanvas *c_RF_t = new TCanvas("c_RF_t","Time_RF",dim_canvas,dim_canvas);   
  
  TF1 *MYgaussiana = new TF1("gaussiana",Gaussiana,-10,10,3);
  MYgaussiana->SetLineColor(1);
  MYgaussiana->SetParNames("Norm","#mu","#sigma"); MYgaussiana->SetLineColor(2);
  MYgaussiana->SetParameters(10,RF_tzero->GetMean(),RF_tzero->GetRMS());
  RF_tzero->Fit(MYgaussiana,"Q","",RF_tzero->GetMean()-2.*RF_tzero->GetRMS(),RF_tzero->GetMean()+2.*RF_tzero->GetRMS());    RF_tzero->Draw();

  c_RF_t->Print(filename+".pdf");
  c_RF_t->Print(filename+".C");
  c_RF_t->Print(filename+".root");
  return;
}

void plot_Noise(TH1F* h_RMS[_N_], TString filename){
  
  int ikk=1;
  TCanvas *c_Noise = new TCanvas("c_Noise","c_Noise",dim_canvas*_N_,dim_canvas);  c_Noise->Divide(_N_,1);
  gStyle->SetOptStat("mr");
  for(int kk=0; kk<_N_; kk++){
    if(kk==iRF) continue;
    c_Noise->cd(ikk);
    h_RMS[kk]->SetLineColor(kk+1);   h_RMS[kk]->GetXaxis()->SetTitle("RMS Noise [mV]");   h_RMS[kk]->Draw();   
    ikk++;
  }

  c_Noise->Print(filename+".pdf");
  c_Noise->Print(filename+".C");
  c_Noise->Print(filename+".root");
  return;
}

void plot_Amplitude(TH1F* h_Amp[_N_], TH1F* h_AmpNoise[_N_],TString filename){
  int ikk=1;
  TCanvas *c_Amplitude = new TCanvas("c_Amplitude","Amplitude",2*dim_canvas,_N_*dim_canvas);  c_Amplitude->Divide(2,_N_);
  for(int kk=0; kk<_N_; ++kk){
    if(kk==iRF) continue;
    c_Amplitude->cd(ikk);
    h_Amp[kk]->SetLineColor(kk+1);   h_Amp[kk]->GetXaxis()->SetTitle("Amplitude [mV]");   h_Amp[kk]->Draw();
    c_Amplitude->cd(ikk+1);
    h_AmpNoise[kk]->SetLineColor(kk+1);   h_AmpNoise[kk]->GetXaxis()->SetTitle("Amplitude/Noise");   h_AmpNoise[kk]->Draw();
    ikk=ikk+2;
  }
  c_Amplitude->Print(filename+".pdf");
  c_Amplitude->Print(filename+".C");
  c_Amplitude->Print(filename+".root");
  return;  
}

  
void plot_Amplitude_vs_iwave(TH1F* h_Amp_vs_iwave[_N_],TString filename){
  int ikk=1;
  TCanvas *c_Amplitude = new TCanvas("c_Amplitude_vs_iwave","Amplitude_vs_iwave",1*dim_canvas,_N_*dim_canvas);  c_Amplitude->Divide(1,_N_);
  for(int kk=0; kk<_N_; ++kk){
    if(kk==iRF) continue;
    c_Amplitude->cd(ikk);
    h_Amp_vs_iwave[kk]->SetLineColor(kk+1);   h_Amp_vs_iwave[kk]->GetXaxis()->SetTitle("Amplitude [mV]");   h_Amp_vs_iwave[kk]->Draw();
    ikk=ikk+1;
  }
  c_Amplitude->Print(filename+".pdf");
  c_Amplitude->Print(filename+".C");
  c_Amplitude->Print(filename+".root");
  return;  
}

void plot_Landau(TH1F* h_Amp,TString filename, Float_t Noise){
  // ======================
  Float_t landau_fit_xlow=20;
  Float_t landau_fit_xhigh=160;
  
  TCanvas *c_Landau = new TCanvas("c_Landau","c_Landau",dim_canvas,dim_canvas);
  TF1Convolution *f_conv = new TF1Convolution("landau","gaus",landau_fit_xlow,landau_fit_xhigh,true);
  f_conv->SetRange(landau_fit_xlow*0.9,landau_fit_xhigh*1.1);     f_conv->SetNofPointsFFT(1000);

  TF1   *f = new TF1("f",*f_conv, landau_fit_xlow*0.9, landau_fit_xhigh*1.1, f_conv->GetNpar());      f->SetLineColor(1);    
  f->SetParameter(0,30);  // Ampiezza
  f->SetParameter(1,h_Amp->GetXaxis()->GetBinCenter(h_Amp->GetMaximumBin()));  // most Prob value
  f->SetParameter(2,2);  // width
  f->FixParameter(3,0);  // gauss mean
  f->FixParameter(4,Noise); // gauss sigma
  f->SetParNames("Norm","Amp(P_{max})","Scale","meanResol","sigmaResol");

  h_Amp->Fit(f,"QR","",landau_fit_xlow,landau_fit_xhigh);    
  h_Amp->Draw();

  TF1 *MYlandau = new TF1("MYlandau","landau",landau_fit_xlow,landau_fit_xhigh);
  MYlandau->SetParameters(f->GetParameter(0),f->GetParameter(1),f->GetParameter(2));
  MYlandau->SetLineColor(4);  MYlandau->SetLineStyle(2);  MYlandau->Draw("same");

  TCanvas *cLandau = new TCanvas("cLandau","cLandau",100,100); MYlandau->Draw();    
  Double_t ymax    = MYlandau->GetMaximum();
  Double_t x_ymax  = MYlandau->GetMaximumX();//GetX(ymax,landau_fit_xlow,landau_fit_xhigh);
  Double_t xmin    = MYlandau->GetX(ymax/2,landau_fit_xlow,x_ymax);
  Double_t xmax    = MYlandau->GetX(ymax/2,x_ymax,landau_fit_xhigh);
  //cout<<" Landau parameters "<< ymax<<" "<<x_ymax<<" "<<xmin<<" "<<xmax<<endl;

  c_Landau->cd();  
  TPaveText *pt = new TPaveText(0.65,0.55,0.85,0.70,"BRNDC");
  pt->SetBorderSize(0);    pt->SetFillColor(0);    pt->SetFillStyle(0);    pt->SetLineWidth(2);    pt->SetTextFont(42);   pt->SetTextColor(4);  
  TString MYstring("Max/FWHM =");
  TString name;
  name.Form("%2.2f", x_ymax/(xmax-xmin));    MYstring+=name; 
  TText *pt_LaTex = pt->AddText(MYstring);
  pt->Draw();
  cout<<"----> Max/FWHM ="<<x_ymax/(xmax-xmin)<<endl;

  
  c_Landau->Print(filename+".pdf");
  c_Landau->Print(filename+".C");
  c_Landau->Print(filename+".root");
  return;
}
void plot_FFT(TH1F* ProfilehFFT,TH1F* ProfilehFFT_noise,TH1F* ProfilehPHASE,TH1F* ProfilehPHASE_noise, TString filename){
  TCanvas *c_FFT = new TCanvas("c_FFT","c_FFT",2*dim_canvas,2*dim_canvas);  c_FFT->Divide(2,2);
  c_FFT->cd(1);  c_FFT->cd(1)->SetLogy();      ProfilehFFT->Draw();
  c_FFT->cd(2);  c_FFT->cd(2)->SetLogy(); ProfilehFFT_noise->Draw();
  c_FFT->cd(3);  c_FFT->cd(3)->SetLogy();      ProfilehPHASE->Draw();
  c_FFT->cd(4);  c_FFT->cd(4)->SetLogy(); ProfilehPHASE_noise->Draw();

  c_FFT->Print(filename+".pdf");
  c_FFT->Print(filename+".C");
  c_FFT->Print(filename+".root");
  return;
}
void plot_TOT(TH1F* h_TOT, TH2F* h2_TOTamp, TString filename){
  TCanvas *c_TimeOverThreshold = new TCanvas("c_TimeOverThreshold","c_TimeOverThreshold",dim_canvas*2,dim_canvas);
  c_TimeOverThreshold->Divide(2,1);
  
  h_TOT->GetXaxis()->SetTitle("Time over Threshold [ns]");
  c_TimeOverThreshold->cd(1);                 h_TOT->Draw();
  h2_TOTamp->GetYaxis()->SetTitle("Amplitude [mV]");
  h2_TOTamp->GetXaxis()->SetTitle("Time over Threshold [ns]");
  c_TimeOverThreshold->cd(2);                 h2_TOTamp->Draw("zcol");
  c_TimeOverThreshold->Print(filename+".pdf");
  c_TimeOverThreshold->Print(filename+".C");
  c_TimeOverThreshold->Print(filename+".root");
  return;
}
//======================================================================
Float_t  plot_TimeResolutions(TH1F*DeltaT[10] , TString filename, Float_t interval=-10., Float_t err_interval=-10.){
//======================================================================
int nhisto=8;
//#ifdef CH4
//nhisto=8;
//#endif
  TCanvas *c_TimeResolution = new TCanvas("c_TimeResolution","c_TimeResolution",dim_canvas*nhisto/2,2*dim_canvas); c_TimeResolution->Divide(nhisto/2,2);
#ifndef DueSENSORI
    nhisto = nhisto-1;
#endif
    c_TimeResolution->cd(1);
  TFitResultPtr r;
  for (int kk=0; kk<nhisto; ++kk)    {
      //cout<<"PLot DeltaT"<<kk<<endl;
    c_TimeResolution->cd(kk+1);
      if(DeltaT[kk]==NULL) continue;
      DeltaT[kk]->SetLineColor((kk%4)+1);
      
    if(DeltaT[kk]->GetEntries() <1) continue;
    DeltaT[kk]->Fit("gaus","Q");

    Float_t mean = DeltaT[kk]->GetFunction("gaus")->GetParameter(1);
    Float_t sigma = DeltaT[kk]->GetFunction("gaus")->GetParameter(2);
    DeltaT[kk]->Fit("gaus","Q","",mean-2.*sigma,mean+2.*sigma);

#ifdef BIFURGAUSS
    TF1    *MY2gauss = new TF1("BIFURGAUSS",BifurGauss,-10,10,4);       MY2gauss->SetLineColor(2);      
    Float_t Norm = DeltaT[kk]->GetFunction("gaus")->GetParameter(0);
    mean  = DeltaT[kk]->GetFunction("gaus")->GetParameter(1);
    sigma = fabs(DeltaT[kk]->GetFunction("gaus")->GetParameter(2));
    MY2gauss->SetParameters(Norm,mean,sigma,1.1);
    MY2gauss->SetParLimits(1,mean-0.3*sigma,mean+0.3*sigma);
    MY2gauss->SetParLimits(2,sigma*0.8,sigma*1.2);
    MY2gauss->SetParLimits(3,0.5,2.);
    r = DeltaT[kk]->Fit(MY2gauss,"SBEL","",mean-3.*sigma,mean+1.7*sigma);
#endif
      
#ifdef DUEGAUSS
    TF1    *MY2gauss = new TF1("MY2gauss",DueGaussiane,-10,10,6);       MY2gauss->SetLineColor(2);
    Float_t Norm = DeltaT[kk]->GetFunction("gaus")->GetParameter(0);
    mean  = DeltaT[kk]->GetFunction("gaus")->GetParameter(1);
    sigma = DeltaT[kk]->GetFunction("gaus")->GetParameter(2);
      MY2gauss->SetParameters(Norm,mean,sigma,0., 10., 0.8);
    MY2gauss->SetParLimits(2,sigma*0.5,sigma*2);
    //MY2gauss->SetParLimits(3,0.01,0.8);
    MY2gauss->SetParLimits(4,1.,20.);
    MY2gauss->SetParLimits(5,0.,1.);
    DeltaT[kk]->Fit(MY2gauss,"","",mean-5*sigma,DeltaT[kk]->GetXaxis()->GetXmax());//mean+7*sigma);
#endif
#ifdef EXPmoGAUSS
    TF1    *MYEXPmoGAUSS = new TF1("MYEXPmoGAUSS",emg_cost,-10,10,6);   MYEXPmoGAUSS->SetLineColor(2);  
    Float_t Norm = DeltaT[kk]->GetFunction("gaus")->GetParameter(0);
    mean  = DeltaT[kk]->GetFunction("gaus")->GetParameter(1);
    sigma = DeltaT[kk]->GetFunction("gaus")->GetParameter(2);
    MYEXPmoGAUSS->SetParameters(Norm,mean,sigma,0.05, 0.08,1);
    MYEXPmoGAUSS->SetParLimits(2,sigma*0.8,sigma*1.2);    
    MYEXPmoGAUSS->SetParLimits(4,0.,1.);
    MYEXPmoGAUSS->FixParameter(5,1); //MYEXPmoGAUSS->SetParLimits(5,0.5,10.);
    DeltaT[kk]->Fit(MYEXPmoGAUSS,"QLB","",mean-3.5*sigma,DeltaT[kk]->GetXaxis()->GetXmax());//mean+7*sigma);
#endif

  }
//for(int kk=0; kk<nhisto; kk++) {
//  c_TimeResolution->cd(kk+1);  DeltaT[kk]->SetLineColor((kk%4)+1);      DeltaT[kk]->Draw();
//}


  cout<<" ================ TIME RESOLUTION  ================== "<<endl;
  TString funString("gaus");
#ifdef BIFURGAUSS
  funString = "BIFURGAUSS";
#endif  
#ifdef DUEGAUSS
  funString = "MY2gauss";
#endif
#ifdef EXPmoGAUSS
  funString = "MYEXPmoGAUSS";
#endif
  
  cout<<" ============== Risoluzioni deconvolute =========================== "<<endl;
  Float_t sigma_M12 = DeltaT[3]->GetFunction(funString)->GetParameter(2);
  Float_t sigma_M1   = DeltaT[4]->GetFunction(funString)->GetParameter(2);
  Float_t sigma_M2   = DeltaT[5]->GetFunction(funString)->GetParameter(2);
    
    cout<<"time resolution M12 = "<<sigma_M12<<endl;
    cout<<"time resolution M1 = "<<sigma_M1<<endl;
    cout<<"time resolution M2 = "<<sigma_M2<<endl;
    
  Float_t DELTA = sigma_M1*sigma_M1 - sigma_M2*sigma_M2;  
  if(DELTA<0){
    DELTA = -DELTA;
    Float_t tmp = sigma_M1;
    sigma_M1 = sigma_M2;
    sigma_M2 = tmp;
  }
  Float_t alpha2 = ( sigma_M12*sigma_M12 - DELTA ) / (sigma_M12*sigma_M12 + DELTA );

  Float_t  sigma1 = sigma_M12/sqrt(1+alpha2);
  Float_t  sigma2 = sqrt(alpha2)*sigma1;
  Float_t  sigmaS = sqrt(sigma_M1*sigma_M1 - sigma1*sigma1);

  cout<<"time resolution MCP1 = "<<sigma1<<endl;
  cout<<"time resolution MCP2 = "<<sigma2<<endl;
  cout<<"time resolution Si = "<<sigmaS<<endl;
  
  Float_t sigma_MCP=DeltaT[3]->GetFunction(funString)->GetParameter(2);
  Float_t sigmaSS = DeltaT[7]->GetFunction(funString)->GetParameter(2);
  sigmaSS= sqrt(sigmaSS*sigmaSS - sigma_MCP*sigma_MCP/4.);
  cout<<"time resolution Si = "<<sigmaSS<<" (resol MCP1=MCP2)"<<endl;
  
				   
  c_TimeResolution->Print(filename+".pdf");
  c_TimeResolution->Print(filename+".C");
  c_TimeResolution->Print(filename+".root");
  
  if(interval>0){
    sigmaS = sqrt(interval*interval - sigma_MCP*sigma_MCP/4.);
    float err_sigmaS = err_interval;
    cout<<"time resolution Si [interval]"<<interval<<" +/- "<<err_interval<<" -- deconvoluto -- "<<sigmaS<< " +/- "<< err_sigmaS<<endl;
  }
#ifdef BIFURGAUSS
  Float_t a = DeltaT[3]->GetFunction("BIFURGAUSS")->GetParameter(2);
  Float_t b = fabs(DeltaT[3]->GetFunction("BIFURGAUSS")->GetParameter(3));
  Float_t ea = DeltaT[3]->GetFunction("BIFURGAUSS")->GetParError(2);
  Float_t eb = DeltaT[3]->GetFunction("BIFURGAUSS")->GetParError(3);
  Float_t sigmaW = a *(1.+b)/2.;
  TMatrixDSym corr = r->GetCorrelationMatrix();
  Float_t rho = corr[2][3];
  Float_t err_sigmaW = sqrt((ea/a)*(ea/a) + (eb/(1.+b))*(eb/(1.+b)) + 2.*rho*(ea/a)*(eb/(1.+b))) * a *(1.+b)/2.;  
  if(b<1.003) err_sigmaW = ea;
  sigmaS = sqrt(sigmaW*sigmaW - sigma_MCP*sigma_MCP/4.);
  float err_sigmaS = err_sigmaW;
  cout<<"time resolution Si sigma_ave= "<<sigmaW<<" +/-"<<err_sigmaW<<"  -- deconvoluto -- "<<sigmaS<< " +/- "<< err_sigmaS<<endl;
#endif
  
  //Float_t sigma_MCP=0;
    c_TimeResolution->Print(filename+".pdf");
    c_TimeResolution->Print(filename+".C");
    c_TimeResolution->Print(filename+".root");
 //return 0;
return sigma_MCP;
}
//======================================================================
Float_t  plot_TimeResolutions_SIM(TH1F*DeltaT[4] , TString filename,  Float_t interval=-10., Float_t err_interval=-10.){
//======================================================================

  TCanvas *c_TimeResolution = new TCanvas("c_TimeResolution","c_TimeResolution",dim_canvas*2,dim_canvas*2); c_TimeResolution->Divide(2,2);
  
  Float_t FWHM[4], asym[4];
  for (int kk=0; kk<4; ++kk)    {
    c_TimeResolution->cd(1+kk);
    if(DeltaT[kk]->GetEntries() <1) continue;
    DeltaT[kk]->Fit("gaus","Q");
    //if(kk==0) continue;
    Float_t mean = DeltaT[kk]->GetFunction("gaus")->GetParameter(1);
    Float_t sigma = DeltaT[kk]->GetFunction("gaus")->GetParameter(2);
    DeltaT[kk]->Fit("gaus","Q","",mean-3.*sigma,mean+1.2*sigma);
    cout<<"Time Resolution single gauss = "<<DeltaT[kk]->GetFunction("gaus")->GetParameter(2)<<" +/-"<<DeltaT[kk]->GetFunction("gaus")->GetParError(2)<<"  ---> RMS="<<DeltaT[kk]->GetRMS()<<" +/-"<<DeltaT[kk]->GetRMSError()<<endl;
#ifdef BIFURGAUSS
    TF1    *MY2gauss = new TF1("BIFURGAUSS",BifurGauss,-10,10,4);       MY2gauss->SetLineColor(2);      
    Float_t Norm = DeltaT[kk]->GetFunction("gaus")->GetParameter(0);
    mean  = DeltaT[kk]->GetFunction("gaus")->GetParameter(1);
    sigma = DeltaT[kk]->GetFunction("gaus")->GetParameter(2);
    MY2gauss->SetParameters(Norm,mean,sigma,1.1);
    MY2gauss->SetParLimits(1,mean-0.35*sigma,mean+0.35*sigma);
    MY2gauss->SetParLimits(2,sigma*0.8,sigma*1.2);
    MY2gauss->SetParLimits(3,0.6,2.);
    TFitResultPtr r = DeltaT[kk]->Fit(MY2gauss,"QSLBE","",mean-3.*sigma,mean+1.7*sigma);
    for (int iloop=0; iloop<nLoop; iloop++){
      if(DeltaT[kk]->GetFunction(MY2gauss->GetName())->GetProb() < 0.4 ) {
	r = DeltaT[kk]->Fit(MY2gauss,"QSLBE","",mean-3.*sigma,mean+1.7*sigma);
      }
    }
    Float_t a = MY2gauss->GetParameter(2);
    Float_t b = fabs(MY2gauss->GetParameter(3));
    Float_t ea = MY2gauss->GetParError(2);
    Float_t eb = MY2gauss->GetParError(3);
    TMatrixDSym corr = r->GetCorrelationMatrix();
    Float_t rho = corr[2][3];
    Float_t sigmaW = a *(1.+b)/2.;
    Float_t err_sigmaW = sqrt((ea/a)*(ea/a) + (eb/(1.+b))*(eb/(1.+b))+ 2.*rho*(ea/a)*(eb/(1.+b))) * a *(1.+b)/2.;
    if(b<1.003) err_sigmaW = ea;
    cout<<"Time Resolution sigma_ave= "<<sigmaW<<" +/-"<<err_sigmaW<<"  ---> RMS="<<DeltaT[kk]->GetRMS()<<" +/-"<<DeltaT[kk]->GetRMSError()<<" -- rho "<<rho<<endl;
#endif
#ifdef DUEGAUSS
    TF1    *MY2gauss = new TF1("MY2gauss",DueGaussiane,-10,10,6);       MY2gauss->SetLineColor(3);      
    Float_t Norm = DeltaT[kk]->GetFunction("gaus")->GetParameter(0);
    mean  = DeltaT[kk]->GetFunction("gaus")->GetParameter(1);
    sigma = DeltaT[kk]->GetFunction("gaus")->GetParameter(2);
    MY2gauss->SetParameters(Norm,mean,sigma,0.3, 3.5, 0.8);
    MY2gauss->SetParLimits(2,sigma*0.8,sigma*1.2);
    MY2gauss->SetParLimits(3,0.01,0.8);
    MY2gauss->SetParLimits(4,1.,6.);
    MY2gauss->SetParLimits(5,0.,1.);
    MY2gauss->SetRange(mean-2.5*sigma,DeltaT[kk]->GetXaxis()->GetXmax());
    DeltaT[kk]->Fit(MY2gauss,"QR","",mean-2.5*sigma,DeltaT[kk]->GetXaxis()->GetXmax());//mean+7*sigma);
    cout<<"Time Resolution 2gauss= "<<MY2gauss->GetParameter(2)<<" +/-"<<MY2gauss->GetParError(2)<<"  ---> RMS="<<DeltaT[kk]->GetRMS()<<" +/-"<<DeltaT[kk]->GetRMSError()<<endl;
#endif
#ifdef EXPmoGAUSS
    TF1    *MYEXPmoGAUSS = new TF1("MYEXPmoGAUSS",emg_cost,-10,10,6);   MYEXPmoGAUSS->SetLineColor(2);  
    Float_t Norm = DeltaT[kk]->GetFunction("gaus")->GetParameter(0);
    mean  = DeltaT[kk]->GetFunction("gaus")->GetParameter(1);
    sigma = DeltaT[kk]->GetFunction("gaus")->GetParameter(2);
    MYEXPmoGAUSS->SetParameters(Norm,mean,sigma,0.1, 0.8,1);
    MYEXPmoGAUSS->SetParLimits(2,sigma*0.8,sigma*1.2);    
    MYEXPmoGAUSS->SetParLimits(4,0.,1.);
    MYEXPmoGAUSS->FixParameter(5,1); //MYEXPmoGAUSS->SetParLimits(5,0.5,10.);
    MYEXPmoGAUSS->SetRange(mean-2.5*sigma,DeltaT[kk]->GetXaxis()->GetXmax());
    DeltaT[kk]->Fit(MYEXPmoGAUSS,"QR","",mean-2.5*sigma,DeltaT[kk]->GetXaxis()->GetXmax());//mean+7*sigma);
    cout<<"Time Resolution sigma_core= "<<MYEXPmoGAUSS->GetParameter(2)<<" +/-"<<MYEXPmoGAUSS->GetParError(2)<<"  ---> RMS="<<DeltaT[kk]->GetRMS()<<" +/-"<<DeltaT[kk]->GetRMSError()<<endl;
#endif
  c_TimeResolution->cd(kk+1);  DeltaT[kk]->SetLineColor(2);      DeltaT[kk]->Draw();

  int bin1 = DeltaT[kk]->FindFirstBinAbove(DeltaT[kk]->GetMaximum()/2);
  int bin2 = DeltaT[kk]->FindLastBinAbove(DeltaT[kk]->GetMaximum()/2);
  FWHM[kk] = DeltaT[kk]->GetBinCenter(bin2)- DeltaT[kk]->GetBinCenter(bin1);
  asym[kk] =
    DeltaT[kk]->Integral(DeltaT[kk]->GetMaximumBin()+1,DeltaT[kk]->GetNbinsX()) -
    DeltaT[kk]->Integral(1,DeltaT[kk]->GetMaximumBin());
    asym[kk] = asym[kk] / DeltaT[kk]->GetEntries();
  }
				   
  cout<<" ================ TIME RESOLUTION  ================== ";
  for (int kk=0; kk<4; kk++) 
    cout<<kk<<" FWHM "<<FWHM[kk]<<" Asym "<<asym[kk]<<endl;  
				   
  c_TimeResolution->Print(filename+".pdf");
  c_TimeResolution->Print(filename+".C");
  c_TimeResolution->Print(filename+".root");
  Float_t sigma_MCP=0.;
  return sigma_MCP;
}
//======================================================================
void plot_TimeResolution_vs_Amp(std::vector<Float_t> BinAmp, Float_t meanAmp, TString filename, Float_t sigma_MCP, TH1F* DeltaT_V[20], TH1F* Amp_V[20]){
  //======================================================================

  Int_t nBins = BinAmp.size()-1;
  int nc=nBins/3;
  if ((nBins % 3) > 0) nc = nc + 1;  

  TCanvas *c_TimeResolution_amp = new TCanvas("c_TimeResolution_amp","c_TimeResolution_amp",dim_canvas*3,dim_canvas*nc);
  c_TimeResolution_amp->Divide(3,nc);

  TH1F *h_TimeResVSAmpl = new TH1F ("h_TimeResVSAmpl", "TimeResVSAmpl", nBins, &BinAmp[0]);
  h_TimeResVSAmpl ->GetXaxis()->SetTitle("Amplitude [mV]");     h_TimeResVSAmpl ->GetYaxis()->SetTitle("time resolution [ns]");  
  h_TimeResVSAmpl->SetTitle("Time Resolution versus signal amplitude");

  TH1F *h_TimeMeanVSAmpl = new TH1F ("h_TimeMeanVSAmpl", "TimeMeanVSAmpl", nBins, &BinAmp[0]);
  h_TimeMeanVSAmpl ->GetXaxis()->SetTitle("Amplitude [mV]");     h_TimeMeanVSAmpl ->GetYaxis()->SetTitle("mean #Deltat [ns]");  
  h_TimeMeanVSAmpl->SetTitle("Mean of #Deltat versus signal amplitude");

  TH1F *h_SiResVSAmpl = new TH1F ("h_SiResVSAmpl", "Silicon TimeResVSAmpl (deconvoluto)", nBins, &BinAmp[0]);
  h_SiResVSAmpl ->GetXaxis()->SetTitle("Amplitude [mV]");     h_SiResVSAmpl ->GetYaxis()->SetTitle("time resolution [ns]");  
  h_SiResVSAmpl->SetTitle("Deconvoluted Time Resolution versus signal amplitude");

#if defined(DUEGAUSS) || defined(EXPmoGAUSS) 
  TH1F *h_TimeFracVSAmpl = new TH1F ("h_TimeFracVSAmpl", "TimeFracVSAmpl", nBins, &BinAmp[0]);
  h_TimeFracVSAmpl->GetXaxis()->SetTitle("Amplitude [mV]");     h_TimeFracVSAmpl ->GetYaxis()->SetTitle("fraction");  
  h_TimeFracVSAmpl->SetTitle("fraction of core gaussian versus signal amplitude");
#endif
  Double_t x[nBins];
  Double_t y[nBins];
  Double_t exl[nBins];
  Double_t eyl[nBins];
  Double_t exh[nBins];
  Double_t eyh[nBins];
  Double_t exm[nBins];
  Float_t min_sigma(1000.), max_sigma(-1000.);
  Float_t _frac(0.8), _tail(0.1);
  cout<<" sigma_MCP="<<sigma_MCP<<endl;
 for (int nn=0;nn<nBins;nn++){
    c_TimeResolution_amp->cd(nn+1);     
    DeltaT_V[nn]->GetXaxis()->SetTitle("#Deltat [ns]");     DeltaT_V[nn]->GetYaxis()->SetTitle("counts"); 
    DeltaT_V[nn]->Draw(); 
    if(DeltaT_V[nn]->GetSumOfWeights()>1) { //GetEntries() > 1) {
      DeltaT_V[nn]->Fit("gaus","Q");
      Double_t dtt_meanV =   DeltaT_V[nn]->GetFunction("gaus")->GetParameter(1);
      Double_t dtt_sigmaV = fabs( DeltaT_V[nn]->GetFunction("gaus")->GetParameter(2));      
      DeltaT_V[nn]->Fit("gaus","Q","",dtt_meanV-3*dtt_sigmaV,dtt_meanV+1.1*dtt_sigmaV);
      dtt_meanV =   DeltaT_V[nn]->GetFunction("gaus")->GetParameter(1);
      dtt_sigmaV = fabs( DeltaT_V[nn]->GetFunction("gaus")->GetParameter(2));      
#ifdef BIFURGAUSS
      TF1    *MY2gauss = new TF1("BIFURGAUSS",BifurGauss,-10,10,4);       MY2gauss->SetLineColor(2);      
      Float_t Norm = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(0);
      MY2gauss->SetParameters(Norm,dtt_meanV,dtt_sigmaV,1.1);
      MY2gauss->SetParLimits(1,dtt_meanV-0.35*dtt_sigmaV, dtt_meanV+0.35*dtt_sigmaV);
      MY2gauss->SetParLimits(2,dtt_sigmaV*0.6,dtt_sigmaV*1.3);
      MY2gauss->SetParLimits(3,0.5,2.);
      DeltaT_V[nn]->Fit(MY2gauss,"BLE","",dtt_meanV-3.*dtt_sigmaV,dtt_meanV+1.7*dtt_sigmaV);
      TFitResultPtr r = DeltaT_V[nn]->Fit(MY2gauss,"SQBLE","",dtt_meanV-3.*dtt_sigmaV,dtt_meanV+1.7*dtt_sigmaV);
#endif
#ifdef DUEGAUSS
      TF1    *MY2gauss = new TF1("MY2gauss",DueGaussiane,-10,10,6);       MY2gauss->SetLineColor(2);            
      Float_t Norm = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(0);
      dtt_meanV  = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(1);
      dtt_sigmaV = fabs(DeltaT_V[nn]->GetFunction("gaus")->GetParameter(2));
      MY2gauss->SetParameters(Norm,dtt_meanV,dtt_sigmaV,0.3, 3.5, 0.8);
      MY2gauss->SetParLimits(1, dtt_meanV-0.3*dtt_sigmaV, dtt_meanV+0.3*dtt_sigmaV);
      MY2gauss->SetParLimits(2,dtt_sigmaV*0.8,dtt_sigmaV*1.3);
      MY2gauss->SetParLimits(4,0.01,0.9);
      MY2gauss->SetParLimits(4,1.1,6.);
      MY2gauss->SetParLimits(5,0.,1.);
      DeltaT_V[nn]->Fit(MY2gauss,"QLBE","",dtt_meanV-3.5*dtt_sigmaV,DeltaT_V[nn]->GetXaxis()->GetXmax());
      for (int iloop=0; iloop<nLoop; iloop++){
	if(DeltaT_V[nn]->GetFunction(MY2gauss->GetName())->GetProb() < 0.4 ) {
	  DeltaT_V[nn]->Fit(MY2gauss,"QLBE","",dtt_meanV-3.5*dtt_sigmaV,DeltaT_V[nn]->GetXaxis()->GetXmax()-iloop*dtt_sigmaV);
	}
      }
#endif
#ifdef EXPmoGAUSS
      TF1    *MYEXPmoGAUSS = new TF1("MYEXPmoGAUSS",emg_cost,-10,10,6);   MYEXPmoGAUSS->SetLineColor(2);  
      Float_t Norm = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(0);
      dtt_meanV  = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(1);
      dtt_sigmaV = fabs(DeltaT_V[nn]->GetFunction("gaus")->GetParameter(2));      
      MYEXPmoGAUSS->SetParameters(Norm,dtt_meanV,dtt_sigmaV,0.1, _frac,1);
      MYEXPmoGAUSS->SetParLimits(1,dtt_meanV-0.3*dtt_sigmaV,dtt_meanV+0.3*dtt_sigmaV);     
      MYEXPmoGAUSS->SetParLimits(2,dtt_sigmaV*0.8,dtt_sigmaV*1.2);      
      MYEXPmoGAUSS->SetParLimits(3,min(0.001,_tail-0.2*fabs(_tail)),max(2.,_tail+0.5*fabs(_tail)));
      MYEXPmoGAUSS->SetParLimits(4,max(_frac*0.5,0.),min(_frac*2.,1.));
      MYEXPmoGAUSS->FixParameter(5,1); //MYEXPmoGAUSS->SetParLimits(5,0.5,10.);
      DeltaT_V[nn]->Fit(MYEXPmoGAUSS,"QLBE","",dtt_meanV-3.5*dtt_sigmaV,DeltaT_V[nn]->GetXaxis()->GetXmax());//mean+7*sigma);
      for (int iloop=0; iloop<nLoop; iloop++){
	if(DeltaT_V[nn]->GetFunction(MYEXPmoGAUSS->GetName())->GetProb() < 0.4 ) {
	  DeltaT_V[nn]->Fit(MYEXPmoGAUSS,"QLBE","",dtt_meanV-3.5*dtt_sigmaV,DeltaT_V[nn]->GetXaxis()->GetXmax()-iloop*dtt_sigmaV/2.);
	}
      }
      _frac = fabs(MYEXPmoGAUSS->GetParameter(4));
      _tail = MYEXPmoGAUSS->GetParameter(3);
#endif


      TString funString("gaus");
#ifdef BIFURGAUSS
      funString = "BIFURGAUSS";
#endif
#ifdef DUEGAUSS
      funString = "MY2gauss";
#endif
#ifdef EXPmoGAUSS
      funString = "MYEXPmoGAUSS";
#endif
      Float_t sigma=DeltaT_V[nn]->GetFunction(funString)->GetParameter(2);//DeltaT_V[nn]->GetRMS(); //
      Float_t sigma_err=DeltaT_V[nn]->GetFunction(funString)->GetParError(2);//DeltaT_V[nn]->GetRMSError(); //
    
#ifdef BIFURGAUSS
      funString = "BIFURGAUSS";
      Float_t a = MY2gauss->GetParameter(2);
      Float_t b = fabs(MY2gauss->GetParameter(3));
      Float_t ea = MY2gauss->GetParError(2);
      Float_t eb = MY2gauss->GetParError(3);
      TMatrixDSym corr = r->GetCorrelationMatrix();
      Float_t rho = corr[2][3];
      sigma = a *(1.+b)/2.;
      sigma_err = sqrt((ea/a)*(ea/a) + (eb/(1.+b))*(eb/(1.+b)) + 2.*rho*(ea/a)*(eb/(1.+b))) * a *(1.+b)/2.;
      if (b<1.005) sigma_err = ea;
#endif
#ifdef myRMS
      sigma=DeltaT_V[nn]->GetRMS(); //
      sigma_err=DeltaT_V[nn]->GetRMSError(); //
#endif  
         
      h_TimeResVSAmpl->SetBinContent(nn+1, sigma );
      h_TimeResVSAmpl->SetBinError(nn+1, sigma_err );
      min_sigma = min(min_sigma, sigma);
      max_sigma = max(max_sigma, sigma);
#ifndef conMCP
      if(sigma>sigma_MCP/2.)       sigma=sqrt(sigma*sigma-sigma_MCP*sigma_MCP/4.);
      else sigma=0.;
      /*
      sigma_err = sqrt((sigma*sigma*sigma_err*sigma_err+sigma_MCP*sigma_MCP*sigma_MCP_err*sigma_MCP_err/16) /
		       (sigma*sigma-sigma_MCP*sigma_MCP/4.));
      */
#endif      
      x[nn]  = Amp_V[nn]->GetMean();
      exm[nn]= Amp_V[nn]->GetMeanError();
      exl[nn]= x[nn]-BinAmp[nn];
      exh[nn]= BinAmp[nn+1]-x[nn];
      
      y[nn] = sigma;
      eyl[nn]=sigma_err;
      eyh[nn]=sigma_err;
      
      h_SiResVSAmpl->SetBinContent(nn+1, sigma );
      h_SiResVSAmpl->SetBinError(nn+1, sigma_err );

      h_TimeMeanVSAmpl->SetBinContent(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParameter(1) );
      h_TimeMeanVSAmpl->SetBinError(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParError(1) );      
      
#ifdef DUEGAUSS
      h_TimeFracVSAmpl->SetBinContent(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParameter(5) );
      h_TimeFracVSAmpl->SetBinError(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParError(5) );
#endif
#ifdef EXPmoGAUSS
      h_TimeFracVSAmpl->SetBinContent(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParameter(4) );
      h_TimeFracVSAmpl->SetBinError(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParError(4) );
#endif
      cout<<"time resolution Si in ["<<BinAmp[nn]<<","<<BinAmp[nn+1]<<"] = "<< h_TimeResVSAmpl->GetBinContent(nn+1)<<" ---> "<<sigma<<" +/-"<<sigma_err<<" =======> RMS/sigma "<<DeltaT_V[nn]->GetRMS()/sigma<<endl;
    }
  }
  
  c_TimeResolution_amp->Print(filename+".pdf");
  c_TimeResolution_amp->Print(filename+".C");
  c_TimeResolution_amp->Print(filename+".root");

  TCanvas *c_TimeResolution_amp_Plot = new TCanvas("c_TimeResolution_amp_Plot","c_TimeResolution_amp_Plot",dim_canvas,dim_canvas);
  h_TimeResVSAmpl->Draw("E1");
  
  TF1 *funzione = new TF1("funzione","TMath::Sqrt([0]*[0]+[1]*[1]/(x*x))",10,90);
  funzione->SetParNames("a [ns]","b [ns mV]");
  funzione->SetLineColor(1);
  funzione->SetLineStyle(2);
  funzione->SetLineWidth(2);
  funzione->SetParameters(0.015,0.1);
  
  h_TimeResVSAmpl ->GetYaxis()->SetRangeUser(min_sigma*0.8,max_sigma*1.2);  //(0.01,0.035);//
  h_TimeResVSAmpl->Fit("funzione","E","",BinAmp[0],BinAmp[nBins]);

  TH1F *h = new TH1F("h","",20,BinAmp[0],BinAmp[nBins]);
  TVirtualFitter *g = TVirtualFitter::GetFitter();
  g->GetConfidenceIntervals(h,0.68);
  h->SetStats(kFALSE);
  h->SetLineColor(kCyan);
  h->SetFillColor(kCyan);
  h->SetFillStyle(3001);
  h->SetMarkerStyle(20);
  h->SetMarkerSize(0.00001);
  h->Draw("e3 same");
  funzione->Draw("same"); 
  
  
  c_TimeResolution_amp_Plot->Print(filename+"_sigma.pdf");
  c_TimeResolution_amp_Plot->Print(filename+"_sigma.C");
  c_TimeResolution_amp_Plot->Print(filename+"_sigma.root");
  TFile *f = new TFile(filename+"_sigma.root","UPDATE");
  h_SiResVSAmpl->Write();
  funzione->Write();
  h->Write();
  f->Close();
  TCanvas *c_TimeMean_amp_Plot = new TCanvas("c_TimeMean_amp_Plot","c_TimeMean_amp_Plot",dim_canvas,dim_canvas);
  h_TimeMeanVSAmpl->Draw("E1");

  c_TimeMean_amp_Plot->Print(filename+"_mean.pdf");
  c_TimeMean_amp_Plot->Print(filename+"_mean.C");
  c_TimeMean_amp_Plot->Print(filename+"_mean.root");
  
#if defined(DUEGAUSS) || defined(EXPmoGAUSS)
  TCanvas *c_TimeFrac_amp_Plot = new TCanvas("c_TimeFrac_amp_Plot","c_TimeFrac_amp_Plot",dim_canvas,dim_canvas);
  h_TimeFracVSAmpl->Draw("E1");

  c_TimeFrac_amp_Plot->Print(filename+"_frac.pdf");
  c_TimeFrac_amp_Plot->Print(filename+"_frac.C");
  c_TimeFrac_amp_Plot->Print(filename+"_frac.root");
#endif  


  TCanvas *c_TimeResolutionSi_amp_Plot = new TCanvas("c_TimeResolutionSi_amp_Plot","c_TimeResolutionSi_amp_Plot",dim_canvas,dim_canvas);
  //h_SiResVSAmpl->Draw("E1");
  TGraphAsymmErrors *gr_fit = new TGraphAsymmErrors(nBins,x,y,exm,exm,eyl,eyh);
  gr_fit->SetMarkerColor(4);
  gr_fit->SetMarkerStyle(20);
  TGraphAsymmErrors *gr_plot = new TGraphAsymmErrors(nBins,x,y,exl,exh,eyl,eyh);
  gr_plot->SetMarkerColor(4);
  gr_plot->SetMarkerStyle(20);
  gr_plot->Draw("P");
  cout<<" Ampiezza media"<<meanAmp<<endl;
  //TF1    *kfunzione = new TF1("kfunzione",deconvolution_basic,10,90,5);   
  TF1 *kfunzione = new TF1("kfunzione","TMath::Sqrt([0]*[0]+[1]*[1]*([2]*[2])/(x*x))",10,90);
  //TF1 *kfunzione = new TF1("kfunzione","TMath::Sqrt([0]*[0]+[1]*[1]*([2]*[2])/(x*x))",10,90);
  //TF1 *kfunzione = new TF1("kfunzione","TMath::Sqrt([0]*[0]+[1]*[1]*([2]*[2])/(x*x)+2.*[0]*[1]*[2]*[3]/x)",10,90);
  kfunzione->SetParNames("#sigma_{un} [ns]","#sigma_{ej} [ns]", "meanAmp [mV]");
  //kfunzione->SetParNames("#sigma_{un} [ns]","#sigma_{ej} [ns]", "meanAmp [mV] ","#sigma_{un}^{Sim}");
  //kfunzione->SetParNames("#sigma_{un} [ns]","#sigma_{ej} [ns]", "meanAmp [mV]","#rho","#sigma_{#Delta MCP}");
  kfunzione->SetLineColor(1);
  kfunzione->SetLineStyle(2);
  kfunzione->SetLineWidth(2);
  kfunzione->SetParameters(0.0145,0.02,meanAmp);
  //kfunzione->FixParameter(1,0.0145);  //sigma_ej
  kfunzione->FixParameter(2,meanAmp);
  kfunzione->FixParameter(3,0.);
#ifdef conMCP
  kfunzione->FixParameter(4,sigma_MCP);
#else
  kfunzione->FixParameter(4,0);
#endif  
  h_SiResVSAmpl ->GetYaxis()->SetRangeUser(min_sigma*0.5,max_sigma*1.1);
  //h_SiResVSAmpl->Fit("kfunzione","","",BinAmp[0],BinAmp[nBins]);

  TH2F *ha = new TH2F("ha","",10,BinAmp[0],BinAmp[nBins],10,0.0,0.045);//20,60);
  ha->SetXTitle("Amplitude [mV]");
  ha->SetYTitle("time resolution, #sigma^{Si}_{t} [ns]");
  ha->GetYaxis()->SetRangeUser(0.0,0.045);
  ha->Draw();
  cout<<" ----------------------- fit classico  ----------------------- "<<endl;
  gr_fit->Fit("kfunzione","E B N");
  cout<<" ----------------------- fit alternativo  ----------------------- "<<endl;
  TF1 *gfunzione = new TF1("gfunzione","TMath::Sqrt([0]*[0]+[1]*[1]*([2]/x))",10,90);
  gfunzione->SetLineColor(2);
  gfunzione->SetLineStyle(3);
  gfunzione->SetParameters(0.010,0.0145,meanAmp);
  gfunzione->FixParameter(2,meanAmp);
  gr_fit->Fit("gfunzione","E B N");
  
  gr_plot->Draw("P");
  TVirtualFitter *kg = TVirtualFitter::GetFitter();
  TH1F *kh = new TH1F("kh","",20,BinAmp[0],BinAmp[nBins]);
  kg->GetConfidenceIntervals(kh,0.68);
  kh->SetStats(kFALSE);
  kh->SetLineColor(kCyan);
  kh->SetFillColor(kCyan);
  kh->SetFillStyle(3001);
  kh->SetMarkerStyle(20);
  kh->SetMarkerSize(0.00001);
  kh->Draw("e3 same");
  kfunzione->Draw("same"); 
  gfunzione->Draw("same"); 
  gr_plot->Draw("P same");

  
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR.pdf");
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR.C");
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR.root");
  TFile *f2 = new TFile(filename+"_sigmaCORR.root","UPDATE");
  kfunzione->Write("funzione");
  kh->Write();
  ha->Write();
  gr_fit->Write("gr_fit");
  gr_plot->Write("gr_plot");
  f2->Close();
  
  
  return;
}
//=========================================================
//======================================================================
void plot_TimeResolution_vs_Amp_Interval(std::vector<Float_t> BinAmp, Float_t meanAmp, TString filename, Float_t sigma_MCP, std::vector<Float_t> DeltaT_V, std::vector<Float_t> DeltaT_V_err, TH1F* Amp_V[20]){
//======================================================================

  Int_t nBins = BinAmp.size()-1;
  int nc=nBins/3;
  if ((nBins % 3) > 0) nc = nc + 1;  

  Double_t x[nBins];
  Double_t y[nBins];
  Double_t exl[nBins];
  Double_t eyl[nBins];
  Double_t exh[nBins];
  Double_t eyh[nBins];
  Double_t exm[nBins];
  Float_t min_sigma(1000.), max_sigma(-1000.);
  TH1F *h_SiResVSAmpl = new TH1F ("h_SiResVSAmpl", "Silicon TimeResVSAmpl (deconvoluto)", nBins, &BinAmp[0]);
  h_SiResVSAmpl ->GetXaxis()->SetTitle("Amplitude [mV]");     h_SiResVSAmpl ->GetYaxis()->SetTitle("time resolution [ns]");  
  h_SiResVSAmpl->SetTitle("Deconvoluted Time Resolution versus signal amplitude");

  cout<<" sigma_MCP="<<sigma_MCP<<endl;
  for (int nn=0; nn<nBins; nn++){
    x[nn]  = Amp_V[nn]->GetMean();
    exm[nn]= Amp_V[nn]->GetMeanError();
    exl[nn]= x[nn]-BinAmp[nn];
    exh[nn]= BinAmp[nn+1]-x[nn];
#ifdef conMCP
    float sigma = DeltaT_V.at(nn);
    float sigma_err = DeltaT_V_err.at(nn);
#else    
    float sigma = sqrt(DeltaT_V.at(nn)*DeltaT_V.at(nn) - sigma_MCP*sigma_MCP/4.);
    float sigma_err = DeltaT_V_err.at(nn);
#endif
    y[nn] = sigma;
    eyl[nn]=sigma_err;
    eyh[nn]=sigma_err;
    
    h_SiResVSAmpl->SetBinContent(nn+1, sigma );
    h_SiResVSAmpl->SetBinError(nn+1, sigma_err );
    
    cout<<"time resolution Si interval in  ["<<BinAmp[nn]<<","<<BinAmp[nn+1]<<"] = "<<DeltaT_V.at(nn)<<" ---> "<< sigma<<" +/-"<<sigma_err<<endl;    
  }
  
  


  TCanvas *c_TimeResolutionSi_amp_Plot = new TCanvas("c_TimeResolutionSi_amp_Plot_Interval","c_TimeResolutionSi_amp_Plot_Interval",dim_canvas,dim_canvas);
  //h_SiResVSAmpl->Draw("E1");
  TGraphAsymmErrors *gr_fit = new TGraphAsymmErrors(nBins,x,y,exm,exm,eyl,eyh);
  gr_fit->SetMarkerColor(4);
  gr_fit->SetMarkerStyle(20);
  TGraphAsymmErrors *gr_plot = new TGraphAsymmErrors(nBins,x,y,exl,exh,eyl,eyh);
  gr_plot->SetMarkerColor(4);
  gr_plot->SetMarkerStyle(20);
  gr_plot->Draw("P");
  cout<<" Ampiezza media"<<meanAmp<<endl;
  //TF1    *kfunzione = new TF1("kfunzione",deconvolution_basic,10,90,5);   
  //TF1 *kfunzione = new TF1("kfunzione","TMath::Sqrt([0]*[0]+[3]*[3]+[1]*[1]*([2]*[2])/(x*x))",10,90);
  //kfunzione->SetParNames("#sigma_{un} [ns]","#sigma_{ej} [ns]", "meanAmp [mV] ","#sigma_{un}^{Sim}");
  TF1 *kfunzione = new TF1("kfunzione","TMath::Sqrt([0]*[0]+[1]*[1]*([2]*[2])/(x*x))",10,90);
  //TF1 *kfunzione = new TF1("kfunzione","TMath::Sqrt([0]*[0]+[1]*[1]*([2]*[2])/(x*x)+2.*[0]*[1]*[2]*[3]/x)",10,90);
  kfunzione->SetParNames("#sigma_{un} [ns]","#sigma_{ej} [ns]", "meanAmp [mV]");
  //kfunzione->SetParNames("#sigma_{un} [ns]","#sigma_{ej} [ns]", "meanAmp [mV]","#rho","#sigma_{#Delta MCP}");
  kfunzione->SetLineColor(1);
  kfunzione->SetLineStyle(2);
  kfunzione->SetLineWidth(2);
  kfunzione->SetParameters(0.010,0.0145,meanAmp);
  //kfunzione->FixParameter(1,0.0145);  //sigma_ej
  kfunzione->FixParameter(2,meanAmp);
  kfunzione->FixParameter(3,0.);
#ifdef conMCP
  kfunzione->FixParameter(4,sigma_MCP);
#else
  kfunzione->FixParameter(4,0);
#endif  
  
  h_SiResVSAmpl ->GetYaxis()->SetRangeUser(min_sigma*0.5,max_sigma*1.1);
  //h_SiResVSAmpl->Fit("kfunzione","","",BinAmp[0],BinAmp[nBins]);

  TH2F *ha = new TH2F("ha","",10,BinAmp[0],BinAmp[nBins],10,0.0,0.045);//20,60);
  ha->SetXTitle("Amplitude [mV]");
  ha->SetYTitle("time resolution, #sigma^{Si}_{t} [ns]");
  ha->GetYaxis()->SetRangeUser(0.0,0.045);
  ha->Draw();
  cout<<" ----------------------- fit classico  ----------------------- "<<endl;
  gr_fit->Fit("kfunzione","E B N");
  cout<<" ----------------------- fit alternativo  ----------------------- "<<endl;
  TF1 *gfunzione = new TF1("gfunzione","TMath::Sqrt([0]*[0]+[1]*[1]*([2]/x))",10,90);
  gfunzione->SetLineColor(2);
  gfunzione->SetLineStyle(3);
  gfunzione->SetParameters(0.010,0.0145,meanAmp);
  gfunzione->FixParameter(2,meanAmp);
  gr_fit->Fit("gfunzione","E B N");
  
  gr_plot->Draw("P");
  TVirtualFitter *kg = TVirtualFitter::GetFitter();
  TH1F *kh = new TH1F("kh","",20,BinAmp[0],BinAmp[nBins]);
  kg->GetConfidenceIntervals(kh,0.68);
  kh->SetStats(kFALSE);
  kh->SetLineColor(kCyan);
  kh->SetFillColor(kCyan);
  kh->SetFillStyle(3001);
  kh->SetMarkerStyle(20);
  kh->SetMarkerSize(0.00001);
  kh->Draw("e3 same");
  kfunzione->Draw("same");
  gfunzione->Draw("same");
  gr_plot->Draw("P same");

  
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR.pdf");
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR.C");
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR.root");
  TFile *f2 = new TFile(filename+"_sigmaCORR.root","UPDATE");
  kfunzione->Write("funzione");
  kh->Write();
  ha->Write();
  gr_fit->Write("gr_fit");
  gr_plot->Write("gr_plot");
  f2->Close();

  return;
}
//=========================

void plot_dVdT(TH1F* h_RiseTime,TH1F* h_dVdT_CFD, TString filename){
  
  TCanvas *c_dVdT = new TCanvas("c_dVdT","c_dVdT",dim_canvas,2*dim_canvas);  c_dVdT->Divide(1,2);
  gStyle->SetOptStat("mr");
  c_dVdT->cd(1);  h_RiseTime->GetXaxis()->SetTitle("Rise time PSI 20-80 [ns]");   h_RiseTime->Draw();   
  c_dVdT->cd(2);  h_dVdT_CFD->GetXaxis()->SetTitle("dVdT CFD [mV/ns]");   h_dVdT_CFD->Draw();   

  c_dVdT->Print(filename+".pdf");
  c_dVdT->Print(filename+".C");
  c_dVdT->Print(filename+".root");
  return;
}


//======================================================================
void plot_TimeResolution_vs_AmpTEST(std::vector<Float_t> BinAmp, Float_t meanAmp, TString filename, Float_t sigma_MCP, TH1F* DeltaT_V[20], TH1F* Amp_V[20]){
  //======================================================================

  Int_t nBins = BinAmp.size()-1;
  int nc=nBins/3;
  if ((nBins % 3) > 0) nc = nc + 1;  
  TString name, title;
  name="c_TimeResolution_amp";
  TCanvas *c_TimeResolution_amp = new TCanvas(name,name,dim_canvas*3,dim_canvas*nc);     c_TimeResolution_amp->Divide(3,nc);

  name="h_TimeResVSAmpl";     title="Time Resolution versus signal amplitude";
  TH1F *h_TimeResVSAmpl = new TH1F (name,title, nBins, &BinAmp[0]);
  h_TimeResVSAmpl ->GetXaxis()->SetTitle("Amplitude [mV]");      h_TimeResVSAmpl ->GetYaxis()->SetTitle("time resolution [ns]");  
  
  name="h_TimeMeanVSAmpl";   title="Mean of #Deltat versus signal amplitude";
  TH1F *h_TimeMeanVSAmpl = new TH1F (name, title, nBins, &BinAmp[0]);
  h_TimeMeanVSAmpl ->GetXaxis()->SetTitle("Amplitude [mV]");     h_TimeMeanVSAmpl ->GetYaxis()->SetTitle("mean #Deltat [ns]");  

  name="h_SiResVSAmpl";      title="Deconvoluted Time Resolution versus signal amplitude";
  TH1F *h_SiResVSAmpl = new TH1F (name, title, nBins, &BinAmp[0]);
  h_SiResVSAmpl ->GetXaxis()->SetTitle("Amplitude [mV]");        h_SiResVSAmpl ->GetYaxis()->SetTitle("time resolution [ns]");  

#if defined(DUEGAUSS) || defined(EXPmoGAUSS)
  name="h_TimeFracVSAmpl";   title="fraction of core gaussian versus signal amplitude";
  TH1F *h_TimeFracVSAmpl = new TH1F (name, title, nBins, &BinAmp[0]);
  h_TimeFracVSAmpl->GetXaxis()->SetTitle("Amplitude [mV]");     h_TimeFracVSAmpl ->GetYaxis()->SetTitle("fraction");  
#endif
  Double_t x[nBins];
  Double_t y[nBins];
  Double_t exl[nBins];
  Double_t eyl[nBins];
  Double_t exh[nBins];
  Double_t eyh[nBins];
  Double_t exm[nBins];
  Float_t min_sigma(1000.), max_sigma(-1000.);
  Float_t _frac(0.8), _tail(0.1);
  cout<<" sigma_MCP="<<sigma_MCP<<endl;
  for (int nn=0;nn<nBins;nn++){
    c_TimeResolution_amp->cd(nn+1);     
    DeltaT_V[nn]->GetXaxis()->SetTitle("#Deltat [ns]");     DeltaT_V[nn]->GetYaxis()->SetTitle("counts"); 
    DeltaT_V[nn]->Draw(); 
    if(DeltaT_V[nn]->GetEntries() > 1) {
      DeltaT_V[nn]->Fit("gaus","Q");
      Double_t dtt_meanV =   DeltaT_V[nn]->GetFunction("gaus")->GetParameter(1);
      Double_t dtt_sigmaV = fabs( DeltaT_V[nn]->GetFunction("gaus")->GetParameter(2));      
      DeltaT_V[nn]->Fit("gaus","Q","",dtt_meanV-3*dtt_sigmaV,dtt_meanV+1.1*dtt_sigmaV);
      dtt_meanV =   DeltaT_V[nn]->GetFunction("gaus")->GetParameter(1);
      dtt_sigmaV = fabs( DeltaT_V[nn]->GetFunction("gaus")->GetParameter(2));      
#ifdef BIFURGAUSS
      TF1    *MY2gauss = new TF1("BIFURGAUSS",BifurGauss,-10,10,4);       MY2gauss->SetLineColor(2);      
      Float_t Norm = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(0);
      MY2gauss->SetParameters(Norm,dtt_meanV,dtt_sigmaV,1.1);
      MY2gauss->SetParLimits(1,dtt_meanV-0.35*dtt_sigmaV, dtt_meanV+0.35*dtt_sigmaV);
      MY2gauss->SetParLimits(2,dtt_sigmaV*0.6,dtt_sigmaV*1.3);
      MY2gauss->SetParLimits(3,0.5,2.);
      DeltaT_V[nn]->Fit(MY2gauss,"BLE","",dtt_meanV-3.*dtt_sigmaV,dtt_meanV+1.7*dtt_sigmaV);
      TFitResultPtr r = DeltaT_V[nn]->Fit(MY2gauss,"SQBLE","",dtt_meanV-3.*dtt_sigmaV,dtt_meanV+1.7*dtt_sigmaV);
#endif
#ifdef DUEGAUSS
      TF1    *MY2gauss = new TF1("MY2gauss",DueGaussiane,-10,10,6);       MY2gauss->SetLineColor(2);            
      Float_t Norm = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(0);
      dtt_meanV  = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(1);
      dtt_sigmaV = fabs(DeltaT_V[nn]->GetFunction("gaus")->GetParameter(2));
      MY2gauss->SetParameters(Norm,dtt_meanV,dtt_sigmaV,0.3, 3.5, 0.8);
      MY2gauss->SetParLimits(1, dtt_meanV-0.3*dtt_sigmaV, dtt_meanV+0.3*dtt_sigmaV);
      MY2gauss->SetParLimits(2,dtt_sigmaV*0.8,dtt_sigmaV*1.3);
      MY2gauss->SetParLimits(4,0.01,0.9);
      MY2gauss->SetParLimits(4,1.1,6.);
      MY2gauss->SetParLimits(5,0.,1.);
      DeltaT_V[nn]->Fit(MY2gauss,"QLBE","",dtt_meanV-3.5*dtt_sigmaV,DeltaT_V[nn]->GetXaxis()->GetXmax());
      for (int iloop=0; iloop<nLoop; iloop++){
	if(DeltaT_V[nn]->GetFunction(MY2gauss->GetName())->GetProb() < 0.4 ) {
	  DeltaT_V[nn]->Fit(MY2gauss,"QLBE","",dtt_meanV-3.5*dtt_sigmaV,DeltaT_V[nn]->GetXaxis()->GetXmax()-iloop*dtt_sigmaV);
	}
      }
#endif
#ifdef EXPmoGAUSS
      TF1    *MYEXPmoGAUSS = new TF1("MYEXPmoGAUSS",emg_cost,-10,10,6);   MYEXPmoGAUSS->SetLineColor(2);  
      Float_t Norm = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(0);
      dtt_meanV  = DeltaT_V[nn]->GetFunction("gaus")->GetParameter(1);
      dtt_sigmaV = fabs(DeltaT_V[nn]->GetFunction("gaus")->GetParameter(2));      
      MYEXPmoGAUSS->SetParameters(Norm,dtt_meanV,dtt_sigmaV,0.1, _frac,1);
      MYEXPmoGAUSS->SetParLimits(1,dtt_meanV-0.3*dtt_sigmaV,dtt_meanV+0.3*dtt_sigmaV);     
      MYEXPmoGAUSS->SetParLimits(2,dtt_sigmaV*0.8,dtt_sigmaV*1.2);      
      MYEXPmoGAUSS->SetParLimits(3,min(0.001,_tail-0.2*fabs(_tail)),max(2.,_tail+0.5*fabs(_tail)));
      MYEXPmoGAUSS->SetParLimits(4,max(_frac*0.5,0.),min(_frac*2.,1.));
      MYEXPmoGAUSS->FixParameter(5,1); //MYEXPmoGAUSS->SetParLimits(5,0.5,10.);
      DeltaT_V[nn]->Fit(MYEXPmoGAUSS,"QLBE","",dtt_meanV-3.5*dtt_sigmaV,DeltaT_V[nn]->GetXaxis()->GetXmax());//mean+7*sigma);
      for (int iloop=0; iloop<nLoop; iloop++){
	if(DeltaT_V[nn]->GetFunction(MYEXPmoGAUSS->GetName())->GetProb() < 0.4 ) {
	  DeltaT_V[nn]->Fit(MYEXPmoGAUSS,"QLBE","",dtt_meanV-3.5*dtt_sigmaV,DeltaT_V[nn]->GetXaxis()->GetXmax()-iloop*dtt_sigmaV/2.);
	}
      }
      _frac = fabs(MYEXPmoGAUSS->GetParameter(4));
      _tail = MYEXPmoGAUSS->GetParameter(3);
#endif

      TString funString("gaus");
#ifdef DUEGAUSS
      funString = "MY2gauss";
#endif
#ifdef EXPmoGAUSS
      funString = "MYEXPmoGAUSS";
#endif
      Float_t sigma=DeltaT_V[nn]->GetFunction(funString)->GetParameter(2);//DeltaT_V[nn]->GetRMS(); //
      Float_t sigma_err=DeltaT_V[nn]->GetFunction(funString)->GetParError(2);//DeltaT_V[nn]->GetRMSError(); //
    
#ifdef BIFURGAUSS
      funString = "BIFURGAUSS";
      Float_t a = MY2gauss->GetParameter(2);
      Float_t b = fabs(MY2gauss->GetParameter(3));
      Float_t ea = MY2gauss->GetParError(2);
      Float_t eb = MY2gauss->GetParError(3);
      TMatrixDSym corr = r->GetCorrelationMatrix();
      Float_t rho = corr[2][3];
      sigma = a *(1.+b)/2.;
      sigma_err = sqrt((ea/a)*(ea/a) + (eb/(1.+b))*(eb/(1.+b)) + 2.*rho*(ea/a)*(eb/(1.+b))) * a *(1.+b)/2.;
      if (b<1.005) sigma_err = ea;
#endif
#ifdef myRMS
      sigma=DeltaT_V[nn]->GetRMS(); //
      sigma_err=DeltaT_V[nn]->GetRMSError(); //
#endif  
         
      h_TimeResVSAmpl->SetBinContent(nn+1, sigma );
      h_TimeResVSAmpl->SetBinError(nn+1, sigma_err );
      min_sigma = min(min_sigma, sigma);
      max_sigma = max(max_sigma, sigma);
      
#ifndef conMCP
      if(sigma>sigma_MCP/2.)       sigma=sqrt(sigma*sigma-sigma_MCP*sigma_MCP/4.);
      else sigma=0.;
      
      //sigma_err = sqrt((sigma*sigma*sigma_err*sigma_err+sigma_MCP*sigma_MCP*sigma_MCP_err*sigma_MCP_err/16) /
      //	       (sigma*sigma-sigma_MCP*sigma_MCP/4.));
      
#endif
            
      x[nn]  = Amp_V[nn]->GetMean();
      exm[nn]= Amp_V[nn]->GetMeanError();
      exl[nn]= x[nn]-BinAmp[nn];
      exh[nn]= BinAmp[nn+1]-x[nn];
      
      y[nn] = sigma;
      eyl[nn]=sigma_err;
      eyh[nn]=sigma_err;
      
      h_SiResVSAmpl->SetBinContent(nn+1, sigma );
      h_SiResVSAmpl->SetBinError(nn+1, sigma_err );

      h_TimeMeanVSAmpl->SetBinContent(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParameter(1) );
      h_TimeMeanVSAmpl->SetBinError(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParError(1) );      
      
#ifdef DUEGAUSS
      h_TimeFracVSAmpl->SetBinContent(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParameter(5) );
      h_TimeFracVSAmpl->SetBinError(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParError(5) );
#endif
#ifdef EXPmoGAUSS
      h_TimeFracVSAmpl->SetBinContent(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParameter(4) );
      h_TimeFracVSAmpl->SetBinError(nn+1, DeltaT_V[nn]->GetFunction(funString)->GetParError(4) );
#endif
      cout<<"time resolution Si in ["<<BinAmp[nn]<<","<<BinAmp[nn+1]<<"] = "<< h_TimeResVSAmpl->GetBinContent(nn+1)<<" ---> "<<sigma<<" +/-"<<sigma_err<<" =======> RMS/sigma "<<DeltaT_V[nn]->GetRMS()/sigma<<endl;
    }
  }  
  c_TimeResolution_amp->Print(filename+"TEST.pdf");
  c_TimeResolution_amp->Print(filename+"TEST.C");
  c_TimeResolution_amp->Print(filename+"TEST.root");

  name="c_TimeMean_amp_Plot";  TCanvas *c_TimeMean_amp_Plot = new TCanvas(name,name,dim_canvas,dim_canvas);
  c_TimeMean_amp_Plot->Print(filename+"_meanTEST.pdf");
  c_TimeMean_amp_Plot->Print(filename+"_meanTEST.C");
  c_TimeMean_amp_Plot->Print(filename+"_meanTEST.root");
  
#if defined(DUEGAUSS) || defined(EXPmoGAUSS)
  name="c_TimeFrac_amp_Plot";  TCanvas *c_TimeFrac_amp_Plot = new TCanvas(name,name,dim_canvas,dim_canvas);
  h_TimeFracVSAmpl->Draw("E1");

  c_TimeFrac_amp_Plot->Print(filename+"_fracTEST.pdf");
  c_TimeFrac_amp_Plot->Print(filename+"_fracTEST.C");
  c_TimeFrac_amp_Plot->Print(filename+"_fracTEST.root");
#endif  

  name="c_TimeResolutionSi_amp_Plot";  TCanvas *c_TimeResolutionSi_amp_Plot = new TCanvas(name,name,dim_canvas,dim_canvas);
  h_SiResVSAmpl ->GetYaxis()->SetRangeUser(min_sigma*0.5,max_sigma*1.1);
  h_SiResVSAmpl->Draw("E1");

  TGraphAsymmErrors *gr_fit = new TGraphAsymmErrors(nBins,x,y,exm,exm,eyl,eyh);  gr_fit->SetMarkerColor(4);  gr_fit->SetMarkerStyle(20);
  TGraphAsymmErrors *gr_plot = new TGraphAsymmErrors(nBins,x,y,exl,exh,eyl,eyh);  gr_plot->SetMarkerColor(4);  gr_plot->SetMarkerStyle(20);
  gr_plot->Draw("P");
  cout<<" Ampiezza media"<<meanAmp<<endl;
  TF1    *kfunzione = new TF1("kfunzione",deconvolution,10,90,7);   

  kfunzione->SetLineColor(1);
  kfunzione->SetLineStyle(2);
  kfunzione->SetLineWidth(2);
  kfunzione->FixParameter(0,meanAmp);


  cout<<" Ampiezza media"<<meanAmp<<endl;
  
  /*
  // Opzione TEST
  kfunzione->SetParNames("meanAmp [mV]","#sigma_{MCP1} [ns]","sigma_{MCP2} [ns]","#sigma_{un} [ns]","#sigma_{ej} [ns]","#rho(MCP1,MCP2)","#rho(MCPave,Si)");
  kfunzione->SetParameters(meanAmp,0.0082, 0.0121,0.0074,0.0145,0.,0.);
  kfunzione->FixParameter(1,0.0082);
  kfunzione->FixParameter(2,0.0121);
  kfunzione->FixParameter(3,0.0074);
  kfunzione->FixParameter(4,0.0145);
  kfunzione->SetParLimits(5,-1.,1.);
  kfunzione->SetParLimits(6,-1.,1.);
  */
  // Opzione TEST2 -- sigma MCPave libera
  kfunzione->SetParNames("meanAmp [mV]","#sigma_{MCPave} [ns]"," dummy ","#sigma_{un} [ns]","#sigma_{ej} [ns]","dummy","#rho(MCPave,Si)");
  kfunzione->SetParameters(meanAmp,0.030, 0 ,0.0065,0.0145,0.,0.);
  kfunzione->SetParLimits(1,0.025/2, 0.024*5); 
  kfunzione->FixParameter(2,0.);
  kfunzione->FixParameter(3,0.0065);
  kfunzione->FixParameter(4,0.0145);
  kfunzione->FixParameter(5,0);
  kfunzione->FixParameter(6,0);
  //kfunzione->SetParLimits(6,-1.,1.);
  
  

  TH2F *ha = new TH2F("ha"," Amplitude [mV]; time resolution, #sigma [ns]",10,BinAmp[0],BinAmp[nBins],10,0.0,0.045);//20,60);
  ha->GetYaxis()->SetRangeUser(0.0,0.045);   ha->Draw();
  gr_fit->Fit("kfunzione","E B N");
  gr_plot->Draw("P");
  TVirtualFitter *kg = TVirtualFitter::GetFitter();
  TH1F *kh = new TH1F("kh","",20,BinAmp[0],BinAmp[nBins]);
  kg->GetConfidenceIntervals(kh,0.68);
  kh->SetStats(kFALSE);
  kh->SetLineColor(kCyan);
  kh->SetFillColor(kCyan);
  kh->SetFillStyle(3001);
  kh->SetMarkerStyle(20);
  kh->SetMarkerSize(0.00001);
  kh->Draw("e3 same");
  kfunzione->Draw("same"); 
  gr_plot->Draw("P same");

  
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR_TEST.pdf");
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR_TEST.C");
  c_TimeResolutionSi_amp_Plot->Print(filename+"_sigmaCORR_TEST.root");
  TFile *f2 = new TFile(filename+"_sigmaCORR_TEST.root","UPDATE");
  kfunzione->Write("funzione");
  kh->Write();
  ha->Write();
  gr_fit->Write("gr_fit");
  gr_plot->Write("gr_plot");
  f2->Close();
  
  
  return;
}
//=========================================================
