template<typename VECTOR>
inline double GetBestInterval(VECTOR& v, double frac){
    
    std::sort(v.begin(), v.end());
    
    int N = v.size();
    int M = frac*N;
    
    std::vector<double> temp;
    for(int i=0; i<N-M; ++i){
        double val = 0.5 * (v[i+M] - v[i]);
        temp.push_back(val);
    }
   
    auto min_k = std::min_element(temp.begin(), temp.end());
    int  k     = std::distance(temp.begin(), min_k);
    
    return temp[k];
}
Double_t DueGaussiane(Double_t *x, Double_t *par){
  Double_t N      = par[0];
  Double_t frac   = par[5];
  Double_t mu1    = par[1];
  Double_t sigma1 = par[2];
  Double_t mu2    = par[3]+mu1;
  Double_t sigma2 = par[4]*sigma1;
  return N*(frac*1./sqrt(6.28*sigma1*sigma1) * TMath::Exp(-pow((x[0]-mu1),2)/(2.*pow(sigma1,2))) +
	    (1-frac)*1./sqrt(6.28*sigma2*sigma2) * TMath::Exp(-pow((x[0]-mu2),2)/(2.*pow(sigma2,2)))
	    );
}

Double_t Gaussiana(Double_t *x, Double_t *par){
  Double_t N     = par[0];
  Double_t mu    = par[1];
  Double_t sigma = par[2];
  return N*TMath::Exp(-pow((x[0]-mu),2)/(2.*pow(sigma,2)));
}

Double_t BifurGauss(Double_t *x, Double_t *par){
  Double_t N      = par[0];
  Double_t mu     = par[1];
  Double_t sigma1 = par[2];
  Double_t sigma2 = par[3]*sigma1;
  Double_t val;
  if (x[0]<mu) val = N/sqrt(6.28*sigma1*sigma1)*TMath::Exp(-pow((x[0]-mu),2)/(2.*pow(sigma1,2)));
  else  val = N/sqrt(6.28*sigma1*sigma1)*TMath::Exp(-pow((x[0]-mu),2)/(2.*pow(sigma2,2)));
  return val;
}

Double_t GaussConst(Double_t *x, Double_t *par){
  Double_t N     = par[0];
  Double_t mu    = par[1];
  Double_t sigma = par[2];
  Double_t A     = par[3];
  return A+N*TMath::Exp(-pow((x[0]-mu),2)/(2.*pow(sigma,2)));
}

Double_t constant(Double_t *x, Double_t *par) 
{
  Double_t C = par[0];
  return C;
}
 
Double_t polinomio(Double_t *x, Double_t *par) 
{
  Double_t A = par[0];
  Double_t B = par[1];
  return A+B*x[0];
}

Double_t sinPIUconst(Double_t *x, Double_t *par)
{
  Double_t A = par[0];
  Double_t B = fabs(par[1]);
  Double_t C = par[2];
  Double_t D = par[3];
  
  return A+B*sin(C*(x[0]+D));
}
Double_t sinsinPIUconst(Double_t *x, Double_t *par)
{
  Double_t A = par[0];
  Double_t B = fabs(par[1]);
  Double_t C = par[2];
  Double_t D = par[3];
  Double_t B1 = fabs(par[4]);
  Double_t C1 = par[5];
  Double_t D1 = par[6];
  
  return A+B*sin(C*(x[0]+D))+B1*sin( C1*(x[0]+D1));
}
Double_t sinFMPIUconst(Double_t *x, Double_t *par)
{
  Double_t A = par[0]; // cost
  Double_t B = fabs(par[1]); // Ampiezza
  Double_t C = par[2]; // freq media
  Double_t E = par[3]; // modulazione in freq
  Double_t D = par[4]; //fase
  
  return A+B*sin(fabs(C+D*sin(x[0]))*x[0]+E);
}
Double_t ExpMoAsymGauss(Double_t *x, Double_t *par){

  Double_t pi = 3.14159;
  Double_t Norm = par[0];
  Double_t mu = par[1];
  Double_t sigma = fabs(par[2]);
  Double_t tau = par[3];
  Double_t frac = par[4];
  Double_t sigma3 = sigma*fabs(par[5]);
  Double_t sigma2 = sigma*fabs(par[6]);
  
  Double_t val;
  if(x[0]<mu ) val = 1./TMath::Sqrt(2*pi*sigma*sigma) * TMath::Exp(-(x[0]-mu)*(x[0]-mu)/(2.*sigma*sigma)) ;
  else val = val = 1./TMath::Sqrt(2*pi*sigma*sigma) * TMath::Exp(-(x[0]-mu)*(x[0]-mu)/(2.*sigma2*sigma2)) ;
  return Norm*((1-frac)/(2.*tau) * TMath::Exp((2.*mu+sigma3*sigma3/tau -2.*x[0])/(2.*tau)) * TMath::Erfc((mu+sigma3*sigma3/tau -x[0])/(TMath::Sqrt(2)*sigma3)) +
	       (frac) * val);
}

Double_t emg_cost(Double_t *x, Double_t *par)
{
  Double_t pi = 3.14159;
  Double_t Norm = par[0];
  Double_t mu = par[1];
  Double_t sigma = par[2];
  Double_t tau = par[3];
  Double_t frac = par[4];
  Double_t sigma2 = sigma*par[5];

  return  Norm*((1-frac)/(2.*tau) * TMath::Exp((2.*mu+sigma2*sigma2/tau -2.*x[0])/(2.*tau)) * TMath::Erfc((mu+sigma2*sigma2/tau -x[0])/(TMath::Sqrt(2)*sigma2)) +
		(frac)/TMath::Sqrt(2*pi*sigma*sigma) * TMath::Exp(-(x[0]-mu)*(x[0]-mu)/(2.*sigma*sigma))) ;
		//frac*(sigma/tau) * TMath::Sqrt(pi/2) * TMath::Exp(0.5*(sigma/tau)*(sigma/tau) - (x[0]-mu)/tau) * TMath::Erfc(1/TMath::Sqrt(2) * (sigma/tau - (x[0]-mu)/sigma)) +

  //  return  A + h*TMath::Exp(-pow((x[0]-mu),2)/(2.*pow(sigma,2)));
}
/*
Double_t emg_cost(Double_t *x, Double_t *par)
{
  Double_t pi = 3.14159;
  Double_t h = par[0];
  Double_t mu = par[1];
  Double_t sigma = par[2];
  Double_t tau = par[3];
  Double_t A = par[4];

  return  A + (h*sigma/tau) * TMath::Sqrt(pi/2) * TMath::Exp(0.5*(sigma/tau)*(sigma/tau) - (x[0]-mu)/tau) * TMath::Erfc(1/TMath::Sqrt(2) * (sigma/tau - (x[0]-mu)/sigma));

  //  return  A + h*TMath::Exp(-pow((x[0]-mu),2)/(2.*pow(sigma,2)));
}

*/

Double_t deconvolution(Double_t *x, Double_t *par)
{
  Double_t Amedio     = par[0];
  Double_t sigma_MCP1 = par[1];
  Double_t sigma_MCP2 = par[2];
  Double_t sigma_un     = par[3];
  Double_t sigma_ej     = par[4];
  Double_t rho_MCP1MCP2 = par[5];
  Double_t rho_MCPaveSi = par[6];

  Double_t sigma2_MCPave = (sigma_MCP1*sigma_MCP1 + sigma_MCP2*sigma_MCP2 + 2 *rho_MCP1MCP2*sigma_MCP1*sigma_MCP2)/4.;
  Double_t sigma2_Si = (sigma_un*sigma_un + sigma_ej*sigma_ej * Amedio * Amedio /(x[0]*x[0]) );
  Double_t sigma2 = sigma2_MCPave + sigma2_Si - 2*rho_MCPaveSi*TMath::Sqrt(sigma2_MCPave*sigma2_Si); 
  return TMath::Sqrt(sigma2);
}

Double_t deconvolution_basic(Double_t *x, Double_t *par)
{
  Double_t sigma_un     = par[0];
  Double_t sigma_ej     = par[1];
  Double_t Amedio     = par[2];
  Double_t rho_MCPaveSi = par[3];
  Double_t sigma_deltaMCP = par[4];

  Double_t sigma2_MCPave = (sigma_deltaMCP*sigma_deltaMCP)/4.;
  Double_t sigma2_Si = (sigma_un*sigma_un + sigma_ej*sigma_ej * Amedio * Amedio /(x[0]*x[0]) );
  Double_t sigma2 = sigma2_MCPave + sigma2_Si - 2*rho_MCPaveSi*TMath::Sqrt(sigma2_MCPave*sigma2_Si); 
  return TMath::Sqrt(sigma2);
}

inline bool file_exists(const std::string& name) {
    ifstream f(name.c_str());
    return f.good();
}

//------------------------------------------------
// FFT stuff - ACa added 29OCT19
//------------------------------------------------
//------------------------------------------------------------------------------------------------------------
// Properly set System Response for DFT
//------------------------------------------------------------------------------------------------------------
void ComputeCableResponse(Int_t size, Int_t fullSize, Double_t *rr, Double_t *ii)
{
  for (Int_t i=0; i<size; i++) {
    rr[fullSize-1-i] = rr[i];
    ii[fullSize-1-i] =-1.*ii[i]; // c'e' un -1, grazie Piero Olla
  }
  /*
  // estrapolazione
  for (Int_t i=size; i<fullSize/2; i++) { // Assume that beyond 8.5GHz the response is just the same as at 8.5GHz
    rr[i] = rr[size-1];
    rr[fullSize-1-i] = rr[size-1];
    ii[i] = ii[size-1];
    ii[fullSize-1-i] = -1.*ii[size-1]; // c'e' un -1, grazie Piero Olla
  }
  */
  TComplex C_cable(rr[size-1],ii[size-1]);
  Float_t phase = C_cable.Theta();
  //  cout<<"C_cable="<<C_cable<<" phase "<<phase<<endl;
  for (Int_t j=size; j<fullSize/2; j++) {
    Double_t x = (Double_t) j;
    Double_t den = 9.83782e-01*exp(-1.23296e-01-1.72074e-02*x+9.58531e-05*x*x-2.61067e-07*x*x*x);
    if (den<0.1) den = 0.1;
    
    Float_t delta=-2.10605; // nuovi dati
    phase = delta + phase;
    if(phase>3.1415) phase = phase - 6.283;
    else if(phase<-3.1415) phase = phase + 6.283;
    //cout<<"den="<<den<<" phase "<<phase<<endl;    
    rr[j] = den*cos(phase);
    ii[j] = den*sin(phase);
    rr[fullSize-1-j] = rr[j];
    ii[fullSize-1-j] = -1.*ii[j]; // c'e' un -1, grazie Piero Olla
  }
  
  return;
}
//------------------------------------------------------------------------------------------------------------
// Low Pass Filter
//------------------------------------------------------------------------------------------------------------
void LowPassFilter(Int_t fullSize, Double_t *LowPassFilter)
{
  // ************** LOW-PASS FILTER FILTER FILTER ***************
  /*
  TF1* turnOn=new TF1("turnOn","0.5*[2]*(1-TMath::Erf((x-[0])/([1]*TMath::Sqrt(x))))",0,fullSize/2);
  //turnOn->SetParameters(12,5,1);
  //turnOn->SetParameters(50,1,1);
  //turnOn->SetParameters(20,2,1);   // 1 GHz
  //turnOn->SetParameters(100,1,1);
  //turnOn->SetParameters(150,1,1);
  turnOn->SetParameters(40,2,1);
  */
  //   Butterworth function
  TF1* turnOn=new TF1("turnOn","TMath::Sqrt([0]/(1.+pow(x/[1],2*[2])))",0,fullSize/2);
  //turnOn->SetParameters(1,100,2);
  //turnOn->SetParameters(1,70,3);
  //turnOn->SetParameters(1,20,2);  // 2- == Cutoff 1 GHz (
  turnOn->SetParameters(1,40,2); 
  
  for (Int_t i=0; i<fullSize/2; i++) {
    Float_t errFunc = turnOn->Eval(i);
    //cout<<" "<<i<<" "<<errFunc;
    LowPassFilter[i] = errFunc;
    LowPassFilter[fullSize-1-i] = errFunc;
  }
  return;
}
//------------------------------------------------------------------------------------------------------------
// Properly set System Response for DFT
//------------------------------------------------------------------------------------------------------------
Int_t SetSysResp(Int_t size, Int_t fullSize, Double_t *rr, Double_t *ii)
{
  for (Int_t i=0; i<size; i++) {
    rr[fullSize-1-i] = rr[i];
    ii[fullSize-1-i] =-1.*ii[i]; // c'e' un -1, grazie Piero Olla
  }
  for (Int_t i=size; i<fullSize/2; i++) { // Assume that beyond 8.5GHz the response is just the same as at 8.5GHz
    rr[i] = rr[size-1];
    rr[fullSize-1-i] = rr[size-1];
    ii[i] = ii[size-1];
    ii[fullSize-1-i] = -1.*ii[size-1]; // c'e' un -1, grazie Piero Olla
  }
  /* 
  //Int_t stop = 50; // 1 GHz
  //Int_t stop = 100; // 2 GHz
  Int_t stop = 150; // 2 GHz
  for (Int_t i=stop; i<fullSize/2; i++) {
    rr[i] = 1.E6;
    rr[fullSize-1-i] = 1.E6;
    ii[i] = 0.;
    ii[fullSize-1-i] = 0.;
  }
  */
    // ************************************************************
  
  // Debug
  //  for (Int_t i=0; i<fullSize; i++)  cout << "*********** SetSysResp: " << i << " " << rr[i] << " " << ii[i] << endl;
  
  return fullSize;
}

//------------------------------------------------------------------------------------------------------------
// Read ZNB8 System Response (complex S21 parameter vs. frequency) 
// from .csv file and fill two vectors. Returns vector size
//------------------------------------------------------------------------------------------------------------
Int_t readSysResp(TString fname, Double_t *ff, Double_t *rr, Double_t *ii)
{
  Int_t j=0; // lines in .csv file with spectral response
  TString line;
  
  // Open input file
  if (file_exists(fname.Data())) { 
    ifstream myFile(fname.Data());        
    while (1) {
      line.ReadLine(myFile);
      if (!myFile.good()) break; // exit at end of file
      
      // Decode line
      TObjArray *tokens = line.Tokenize(";");
      Int_t nTokens = tokens->GetEntries();
      if (nTokens == 1) continue;     // skip header
      
      // Let's decode the variables
      ff[j] = atof( ((TObjString*) tokens->At(0))->GetString() ); // 1st token is frequency
      rr[j] = atof( ((TObjString*) tokens->At(1))->GetString() ); // 2nd token is S21 real part 
      ii[j] = atof( ((TObjString*) tokens->At(2))->GetString() ); // 3rd token is S21 imaginary part
      //      a[j] = 100. * sqrt( ra[j]*ra[j] + ia[j]*ia[j]); // S21 amplitude
      //      cout << j << " " << f[j] << endl;

      if ((ff[j]==0) && (rr[j]==0) && (ii[j]==0)) continue; // to avoid reading title line, which has the correct number of tokens
      
      // Some cleanup
      tokens->Delete();
      delete tokens;
      j++;  // Increase vector index

    } // end of while loop   
    cout << "********************** readSysResp: EOF found after " << j << " data points **********************" << endl;
    myFile.close();            
    //    for (Int_t i=0; i<j; i++)  cout << "*********** GetSysResp: " << i << " " << ff[i] << " " << rr[i] << " " << ii[i] << endl;    
  }
  return j;
}
//------------------------------------------------


