#include "TF1.h"

#define sqrt2pi 2.5066282746310002416123552393401041626930

// p1 background
Double_t pol1(Double_t *v, Double_t *par)
{

  Double_t mu=par[0];
  Double_t sigma=par[1]; if (sigma == 0 ) return 0.;
  Double_t B=par[2];
  Double_t p1=par[3];
  if (sigma == 0) return 0.;
  Double_t bw=par[4];
  Double_t xrange=par[5];
  Double_t x = v[0];
  Double_t fitval = B*bw/xrange + (x-mu)*p1;
  return fitval;
}

 Double_t singlegauss(Double_t *v, Double_t *par)
 {
   // if the sigma is zero then the gaussian is a delta function
   // I don't know how torepresent it other than the zero function

   //six parameters
   Double_t N=par[0];
   Double_t mu=par[1];
   Double_t sigma=par[2]; if (sigma == 0 ) return 0.;
   if (sigma == 0) return 0.;
   Double_t bw=par[3];
   //Double_t bw=5e-4;
   Double_t x = v[0];
   Double_t arg1 = (x - mu)/sigma;
   Double_t norm1=sigma*sqrt2pi;
   Double_t fitval = N*bw * TMath::Exp(-0.5*arg1*arg1) / norm1; 

   return fitval;
 }

//Double gaussian with same mean
Double_t doublegauss(Double_t *v, Double_t *par)
{
  //two gauss same mean
  if ((par[3] == 0 ) || (par[4] == 0)) return 0.;
  //five parameters
  Double_t N=par[0];
  Double_t mu1=par[1];
  Double_t frac=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t sigma2=par[4]; if (sigma2 == 0 ) return 0.;
  Double_t bw=par[5];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;
  Double_t arg2  = (x - mu1)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;

  Double_t fitval =N*bw * (frac     * TMath::Exp(-0.5*arg1*arg1) / norm1+
			   (1-frac) * TMath::Exp(-0.5*arg2*arg2) / norm2 );
  return fitval;
}

// Double Gaussian with different means
Double_t doublegauss2(Double_t *v, Double_t *par)
{
  if ((par[3] == 0 ) || (par[5] == 0)) return 0.;
  //six parameters
  Double_t N=par[0];
  Double_t frac=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t mu2=par[4];
  Double_t sigma2=par[5]; if (sigma2 == 0 ) return 0.;
  Double_t bw=par[6];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;
  Double_t arg2  = (x - mu2)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;
  //  Double_t bw=par[6];
  Double_t fitval =N*bw *(frac     * TMath::Exp(-0.5*arg1*arg1) / norm1+
			  (1-frac) * TMath::Exp(-0.5*arg2*arg2) / norm2 );
  return fitval;
}

// Gaussian  a p1 background
Double_t singlegaussp1(Double_t *v, Double_t *par)
{

  Double_t S=par[0];
  Double_t mu=par[1];
  Double_t sigma=par[2]; if (sigma == 0 ) return 0.;
  if (sigma == 0) return 0.;
  Double_t B=par[3];
  Double_t p1=par[4];
  Double_t bw=par[5];
  Double_t xrange=par[6];
  Double_t x = v[0];
  Double_t arg1 = (x - mu)/sigma;
  Double_t norm1=sigma*sqrt2pi;
  Double_t fitval = S*bw * TMath::Exp(-0.5*arg1*arg1) / norm1 + 
    B*bw/xrange + (x-mu)*p1;

  return fitval;
}


// Double Gaussian with same means over a p1 background
Double_t doublegaussp1(Double_t *v, Double_t *par)
{
  //seven parameters
  Double_t S=par[0];
  Double_t mu1=par[1];
  Double_t frac=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t sigma2=par[4]; if (sigma2 == 0 ) return 0.;
  Double_t B=par[5];
  Double_t p1=par[6];
  Double_t bw=par[7];
  Double_t xrange=par[8];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;
  Double_t arg2  = (x - mu1)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;
  Double_t rms=sqrt(frac*pow(sigma1,2) +(1-frac)*pow(sigma2,2)); 
  if (rms == 0 ) return 0.;

  Double_t fitval =S*bw*(frac     * TMath::Exp(-0.5*arg1*arg1) / norm1+
			 (1-frac) * TMath::Exp(-0.5*arg2*arg2) / norm2 ) +
    B*bw/xrange + (x-mu1)*p1;
  return fitval;
}

//Double Gaussian with different means over a p1 background
Double_t doublegauss2p1(Double_t *v, Double_t *par)
{
  //six parameters
  Double_t S=par[0];
  Double_t frac=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t mu2=par[4];
  Double_t sigma2=par[5]; if (sigma2 == 0 ) return 0.;
  Double_t B=par[6];
  Double_t p1=par[7];
  Double_t bw=par[8];
  Double_t xrange=par[8+1];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;
  Double_t arg2  = (x - mu2)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;
  Double_t rms=sqrt(frac*pow(sigma1,2) +(1-frac)*pow(sigma2,2)); 
  if (rms == 0 ) return 0.;
  double mu=frac*mu1+(1-frac)*mu2;
  //  Double_t bw=par[6];

  Double_t fitval =S*bw *(frac     * TMath::Exp(-0.5*arg1*arg1) / norm1+
			  (1-frac) * TMath::Exp(-0.5*arg2*arg2) / norm2 )+
    B*bw/xrange + (x-mu)*p1;

  
  return fitval;
}

// Gaussian + a p2 background
Double_t singlegaussp2(Double_t *v, Double_t *par)
{

  Double_t S=par[0];
  Double_t mu1=par[1];
  Double_t sigma1=par[2]; if (sigma1 == 0 ) return 0.;
  Double_t B=par[3];
  Double_t p1=par[4];
  Double_t p2=par[5];
  Double_t bw=par[6];
  Double_t xrange=par[6+1];
  Double_t x = v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;
  Double_t fitval = S*bw * TMath::Exp(-0.5*arg1*arg1) / norm1 + 
    B*bw/xrange + (x-mu1)*p1 + pow(x-mu1,2)*p2;

  return fitval;
}


// Double Gaussian with same means over a p2 background
Double_t doublegaussp2(Double_t *v, Double_t *par)
{

  //seven parameters
  Double_t S=par[0];
  Double_t mu1=par[1];
  Double_t frac=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t sigma2=par[4]; if (sigma2 == 0 ) return 0.;
  Double_t B=par[5];
  Double_t p1=par[6];
  Double_t p2=par[7];
  Double_t bw=par[8];
  Double_t xrange=par[8+1];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;
  Double_t arg2  = (x - mu1)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;
  Double_t rms=sqrt(frac*pow(sigma1,2) +(1-frac)*pow(sigma2,2)); 
  if (rms == 0 ) return 0.;

  Double_t fitval =S*bw*(frac     * TMath::Exp(-0.5*arg1*arg1) / norm1+
			 (1-frac) * TMath::Exp(-0.5*arg2*arg2) / norm2 ) +
    B*bw/xrange + (x-mu1)*p1 + pow(x-mu1,2)*p2;
  return fitval;
}

//Double Gaussian with different means over a p2 background
Double_t doublegauss2p2(Double_t *v, Double_t *par)
{
  //six parameters
  Double_t S=par[0];
  Double_t frac=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t mu2=par[4];
  Double_t sigma2=par[5]; if (sigma2 == 0 ) return 0. ; 
  Double_t B=par[6];
  Double_t p1=par[7];
  Double_t p2=par[8];
  Double_t bw=par[9];
  Double_t xrange=par[9+1];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;
  Double_t arg2  = (x - mu2)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;
  Double_t rms=sqrt(frac*pow(sigma1,2) +(1-frac)*pow(sigma2,2)); 
  if (rms == 0 ) return 0.;
  double mu=frac*mu1+(1-frac)*mu2;
  //  Double_t bw=par[6];

  Double_t fitval =S*bw *(frac     * TMath::Exp(-0.5*arg1*arg1) / norm1+
			  (1-frac) * TMath::Exp(-0.5*arg2*arg2) / norm2 )+
    B*bw/xrange + (x-mu)*p1 + (x-mu)*p2;

  
  return fitval;
}

Double_t triplegauss(Double_t *v, Double_t *par)
{
  //tree gaussians with different means
  //six parameters
  Double_t S=par[0];
  Double_t frac1=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t frac2=par[4];
  Double_t sigma2=par[5]; if (sigma2 == 0 ) return 0.;
  Double_t sigma3=par[6]; if (sigma3 == 0 ) return 0.;
  Double_t bw=par[7];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;

  Double_t arg2  = (x - mu1)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;

  Double_t arg3  = (x - mu1)/sigma3;
  Double_t norm3=sigma3*sqrt2pi;

  Double_t fitval =S*bw *(frac1          * TMath::Exp(-0.5*arg1*arg1) / norm1+
			  frac2          * TMath::Exp(-0.5*arg2*arg2) / norm2+
			  (1-frac1-frac2)* TMath::Exp(-0.5*arg3*arg3) / norm3) ;
  return fitval;
}

Double_t triplegauss2p1(Double_t *v, Double_t *par)
{
  //tree gaussians with different means
  //12 parameters
  Double_t S=par[0];
  Double_t frac1=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t frac2=par[4];
  Double_t mu2=par[5];
  Double_t sigma2=par[6]; if (sigma2 == 0 ) return 0.;
  Double_t mu3=mu1;//par[7];
  Double_t sigma3=par[8]; if (sigma3 == 0 ) return 0.;
  Double_t B=par[9];
  Double_t p1=par[10];
  Double_t bw=par[10+1];
  Double_t xrange=par[10+2];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;

  Double_t arg2  = (x - mu2)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;

  Double_t arg3  = (x - mu3)/sigma3;
  Double_t norm3=sigma3*sqrt2pi;

  Double_t rms=sqrt(frac1*pow(sigma1,2) +frac2*pow(sigma2,2)+(1-frac1-frac2)*pow(sigma3,2));
  if (rms == 0 ) return 0.;
  double mu=frac1*mu1+frac2*mu2+(1-frac1-frac2)*mu3;

  Double_t fitval =S*bw *(frac1          * TMath::Exp(-0.5*arg1*arg1) / norm1+
			  frac2          * TMath::Exp(-0.5*arg2*arg2) / norm2+
			  (1-frac1-frac2)* TMath::Exp(-0.5*arg3*arg3) / norm3)+
    B*bw/xrange + (x-mu)*p1;
  //  for (int i =0 ; i<11; i++){cout <<par[i]<<" ";}
  //  cout<<TMath::Exp(-0.5*arg1*arg1)<< " " <<TMath::Exp(-0.5*arg2*arg2)<< " " <<TMath::Exp(-0.5*arg3*arg3);
    //  cout<<norm1<<" "<<norm2<<" " <<norm3<<endl;
  //  cout<<arg1<<" " <<arg2<< " " <<arg3<<endl;
  //  cout<<fitval<<endl;
  return fitval;
}


Double_t triplegauss3(Double_t *v, Double_t *par)
{
  //tree gaussians with different means
  Double_t S=par[0];
  Double_t frac1=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t frac2=par[4];
  Double_t mu2=par[5];
  Double_t sigma2=par[6]; if (sigma2 == 0 ) return 0.;
  Double_t mu3=par[7];
  Double_t sigma3=par[8]; if (sigma3 == 0 ) return 0.;
  Double_t bw=par[9];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1=sigma1*sqrt2pi;

  Double_t arg2  = (x - mu2)/sigma2;
  Double_t norm2=sigma2*sqrt2pi;

  Double_t arg3  = (x - mu3)/sigma3;
  Double_t norm3=sigma3*sqrt2pi;

  Double_t fitval =S*bw *(frac1          * TMath::Exp(-0.5*arg1*arg1) / norm1+
			  frac2          * TMath::Exp(-0.5*arg2*arg2) / norm2+
			  (1-frac1-frac2)* TMath::Exp(-0.5*arg3*arg3) / norm3) ;
  return fitval;
}


//Gaussian + GaussExp convolution
Double_t gaussexpp1(Double_t *v, Double_t *par)
{
  //six parameters
  Double_t S=par[0];
  Double_t frac=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t tau=par[4]; if (tau == 0 ) return 0.;
  Double_t B=par[5];
  Double_t p1=par[6];
  Double_t bw=par[7];
  Double_t xrange=par[7+1];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1 = sigma1*sqrt2pi;
  Double_t argexp =  -1*(x - mu1)/tau + 0.5*pow(sigma1/tau,2);
  Double_t argerf  = ((x - mu1)/sigma1 - sigma1/tau)/pow(2,0.5);
  Double_t norm2 =2*tau;
  Double_t gauss = exp(-0.5*arg1*arg1) / norm1;
  Double_t conv = exp(argexp) * (1+erf(argerf)) / norm2;
  Double_t mu = frac*mu1+(1-frac)*(mu1+tau);
  Double_t fitval =S*bw *(frac * gauss + (1-frac) * conv )+
    B*bw/xrange + (x-mu)*p1;
  
  return fitval;
}

//Gaussian + GaussExp convolution + Erf background
Double_t gaussexperf(Double_t *v, Double_t *par)
{
  //six parameters
  Double_t S=par[0];
  Double_t frac=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t tau=par[4]; if (tau == 0 ) return 0.;
  Double_t B=par[5];
  Double_t p1=par[6];
  Double_t bw=par[7];
  Double_t xrange=par[7+1];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1 = sigma1*sqrt2pi;
  Double_t argexp =  -1*(x - mu1)/tau + 0.5*pow(sigma1/tau,2);
  Double_t argerf  = ((x - mu1)/sigma1 - sigma1/tau)/pow(2,0.5);
  Double_t norm2 =2*tau;
  Double_t gauss = exp(-0.5*arg1*arg1) / norm1;
  Double_t conv = exp(argexp) * (1+erf(argerf)) / norm2;
  Double_t mu = frac*mu1+(1-frac)*(mu1+tau);
  Double_t fitval =S*bw *(frac * gauss + (1-frac) * conv )+
    B*0.5*(1+TMath::Erf((x-mu)/sigma1))*bw/xrange + (x-mu)*p1;
  
  return fitval;
}


//Gaussian + GaussExp convolution with different means over a p1 background
Double_t gaussexp2mup1(Double_t *v, Double_t *par)
{
  //six parameters
  Double_t S=par[0];
  Double_t frac=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t mu2=par[4];
  Double_t tau=par[5]; if (tau == 0 ) return 0.;
  Double_t B=par[6];
  Double_t p1=par[7];
  Double_t bw=par[8];
  Double_t xrange=par[8+1];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1 = sigma1*sqrt2pi;
  Double_t argexp =  (-1*(x - mu2) + 0.5*pow(sigma1,2)/tau)/tau;
  Double_t argerf  = ((x - mu2)/sigma1 - sigma1/tau)/pow(2,0.5);
  Double_t norm2 =2*tau;
  Double_t gauss = exp(-0.5*arg1*arg1) / norm1;
  Double_t conv = exp(argexp) * (1+erf(argerf)) / norm2;
  Double_t mu = frac*mu1+(1-frac)*(mu2+tau);
  Double_t fitval =S*bw *(frac * gauss + (1-frac) * conv )+
    B*bw/xrange + (x-mu)*p1;
  
  return fitval;
}


//Gaussian + GaussExp convolution with different sigmas over a p1 background
Double_t gaussexp2sp1(Double_t *v, Double_t *par)
{
  //six parameters
  Double_t S=par[0];
  Double_t frac=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t tau=par[4]; if (tau == 0 ) return 0.;
  Double_t sigma2=par[5]; if (sigma2 == 0 ) return 0.;
  Double_t B=par[6];
  Double_t p1=par[7];
  Double_t bw=par[8];
  Double_t xrange=par[8+1];

  Double_t x=v[0];
  Double_t arg1 = (x - mu1)/sigma1;
  Double_t norm1 = sigma1*sqrt2pi;
  Double_t argexp =  -1*(x - mu1)/tau + 0.5*pow(sigma2/tau,2);
  Double_t argerf  = ((x - mu1)/sigma2 - sigma2/tau)/pow(2,0.5);
  Double_t norm2 =2*tau;
  Double_t gauss = exp(-0.5*arg1*arg1) / norm1 ;
  Double_t conv = exp(argexp) * (1+erf(argerf)) / norm2;
  double_t mu = frac*mu1+(1-frac)*(mu1+tau);
  Double_t fitval = S*bw *(frac * gauss + (1-frac) * conv ) +
    B*bw/xrange + (x-mu)*p1;
  
  return fitval;
}


//Gaussian + GaussExp convolution with different means and sigmas over a p1 background
Double_t gaussexp2mu2sp1(Double_t *v, Double_t *par)
{
  //six parameters
  Double_t S=par[0];
  Double_t frac=par[1];
  Double_t mu1=par[2];
  Double_t sigma1=par[3]; if (sigma1 == 0 ) return 0.;
  Double_t tau=par[4]; if (tau == 0 ) return 0.;
  Double_t mu2=par[5];
  Double_t sigma2=par[6]; if (sigma2 == 0 ) return 0.;
  Double_t B=par[7];
  Double_t p1=par[8];
  Double_t bw=par[9];
  Double_t xrange=par[9+1];

  Double_t x=v[0];
  Double_t arggauss = (x - mu1)/sigma1;
  Double_t normgauss = sigma1*sqrt2pi;
  Double_t argexp =  -1*(x - mu2)/tau + 0.5*pow(sigma2/tau,2);
  Double_t argerf  = (sigma2/tau - (x - mu2)/sigma2 )/pow(2,0.5);
  Double_t normconv =2*tau;
  Double_t gauss = exp(-0.5*pow(arggauss,2)) / normgauss ;
  Double_t conv;
  if (argerf > 6.71e7){
    Double_t arggauss2 = (x-mu2)/sigma2;
    Double_t normgauss2 = 1./(sigma2*sqrt2pi);
    conv = exp(-0.5*pow(arggauss2,2))/normgauss2/(1+(x-mu2)*tau/(sigma2*sigma2));
  }
  else
    conv = exp(argexp) * (1-erf(argerf)) / normconv;
  double_t mu = frac*mu1+(1-frac)*(mu2+tau);
  Double_t fitval = S * bw * ( frac * gauss + (1-frac) * conv ) +
    B*bw/xrange + (x-mu)*p1;
  
  return fitval;
}

Double_t landau(Double_t *v, Double_t *par)
{
  //three parameters
  Double_t x=v[0];
  Double_t N=par[0];
  Double_t mpv=par[1];
  Double_t sigma=par[2];
  Double_t bw=par[3];
  Double_t fitval=N*bw*TMath::Landau(x,mpv,sigma,kTRUE);
  return fitval;
}
