import subprocess
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from pprint import pprint

# Use this not to run the points again if you have already run them and
# you stored the results in the results list
RUN_POINTS = False            # Run the analysis or take the results from the results list
DO_SPECIAL_TREATMENT = False # This is using only one of the two efficiency options (effInt(S))
ROOT_PLOTS = True            # Make the plots with ROOT
PLT_PLOTS = False             # Make the plots with pyplot
CVD_FRIENDLY = False         # Use CVD-friendly colors in ROOT plots

deg_name_lut = {
    'zerodeg': 0,
    'fivedeg': 5,
    'tendeg': 10,
    'fifteendeg': 15,
    'twentydeg': 20
}

# Dataframe with past results
past_df = pd.DataFrame({

    'angle': [0,0,0,0,0,0,5,10,20],
    'voltage': [20,40,60,80,100,100.0001,100.0001,100.0001,100.0001], 
    'irradiation': [0,0,0,0,0,0,0,0,0], 
    'efficiency_S': [79.1,79.7,80.3,80.9,80.4,79.2,90.5,98.3,99.1], 
    'efficiency_S_error': [2,2,2,2,2,1.2,1.4,1.1,1.1], 
    'chi2NDF': [-1,-1,-1,-1,-1,-1,-1,-1,-1],
    'events_passed': [-1,-1,-1,-1,-1,-1,-1,-1,-1], 
    'efficiency_B': [79.1,79.7,80.3,80.9,80.4,79.2,90.5,98.3,99.1], 
    'efficiency_B_error': [2,2,2,2,2,1.2,1.4,1.1,1.1],
})

def orderArrays(x,y,ex,ey):
    # Order the arrays by x
    order = np.argsort(x)
    x = x[order]
    y = y[order]
    ex = ex[order]
    ey = ey[order]
    return x,y,ex,ey

def parseOutput(output):
    # Find the line in the output that contain the efficiency, its error,
    # the numerator and the denominator. It's redundant, but it avoids 
    # floating point errors in the calculation of the efficiency
    for line in output.split('\n'):
        if line.startswith('number of triggers'):
            # It has format: number of triggers = 1000
            # Split the line into numerator
            den = int(line.split('=')[1])
        if 'S ' in line and 'S1' in line:
            # It has format: S  = 17636.8 S1 = 17636.7
            # Split the line into the number of fitted events
            num = float(line.split('=')[1].split('S1')[0])
        if line.startswith('effInt(S)'):
            # It has format: effInt(S) = 66.73 +/- 0.49 % 
            # Split the line into the efficiency and error
            efficiency_S, error_S = line.split('=')[1].split('+/-')
            efficiency_S = float(efficiency_S)
            error_S = float(error_S.split('%')[0])
        if line.startswith('effInt(B)'):
            # It has format: effInt(B) = 66.73 +/- 0.49 % 
            # Split the line into the efficiency and error
            efficiency_B, error_B = line.split('=')[1].split('+/-')
            efficiency_B = float(efficiency_B)
            error_B = float(error_B.split('%')[0])
        if line.startswith('chi2/ndf'):
            # It has format: chi2/ndf = 1.3
            # Split the line into the chi2 and ndf
            chi2NDF = float(line.split('=')[1])
        if line.startswith('n_corr'):
            # It has format: n_corr = 1000
            # Split the line into denominator
            ncorr = int(line.split('=')[1])
    return efficiency_S, error_S, chi2NDF, num, den, ncorr, efficiency_B, error_B

def runPoint(data_point, cut_string='', cut_string_corr='', nbins = -1):
    # Call runEfficiency.C with root and the data point and get the output
        # If process fails, print output and exit
        try:
            print('Running: root -l -q runEfficiency.C("{}","{}","{}",{})'.format(data_point,cut_string,cut_string_corr,nbins))
            output = subprocess.check_output(
                ['root', '-l', '-q', 'runEfficiency.C("{}","{}","{}",{})'.format(data_point,cut_string,cut_string_corr,nbins)]
                ).decode('utf-8')
        except subprocess.CalledProcessError as e:
            print(e.output)
            exit(1)
        result = parseOutput(output)
        return result

# List of data points to analyze
data_points = [
    'zerodeg_100V_1e17',
    'zerodeg_150V_1e17',
    'zerodeg_175V_1e17',
    'zerodeg_200V_1e17',
    'zerodeg_250V_1e17',
    'zerodeg_270V_1e17',
    'twentydeg_100V_1e17',
    'twentydeg_150V_1e17',
    'twentydeg_175V_1e17',
    'twentydeg_200V_1e17',
    'twentydeg_250V_1e17',
    'twentydeg_270V_1e17',
    'fivedeg_250V_1e17',
    'tendeg_250V_1e17',
    # 'fifteendeg_250V_1e17',
    'zerodeg_200V_5e16',
    # 'zerodeg_250V_5e16',
    # 'zerodeg_270V_5e16',
]

default_binning = 2000

special_binning = {
    'zerodeg_100V_1e17': 10000,
    'zerodeg_150V_1e17': 10000,
    'zerodeg_175V_1e17': 10000,
    'zerodeg_200V_1e17': 9700,
    'zerodeg_250V_1e17': 9700,
    'zerodeg_270V_1e17': 9400,
    'twentydeg_100V_1e17': 10000,
    'twentydeg_150V_1e17': 10000,
    'twentydeg_175V_1e17': 9800,
    'twentydeg_200V_1e17': 9500,
    'twentydeg_250V_1e17': 7800,
    'twentydeg_270V_1e17': 1700,
    'fivedeg_250V_1e17': 9300,
    'tendeg_250V_1e17': 8500,
    # 'fifteendeg_250V_1e17': 8500,
    'zerodeg_200V_5e16': 9600,
    # 'zerodeg_250V_5e16': 9600,
    # 'zerodeg_270V_5e16': 9900
    }

default_cut_string = 'Amp[3]<230&&Amp[0]>40&&Amp[0]<172&&Amp[2]>40&&Amp[2]<172&&abs(tt[2]-tt[3]+6.44)<0.12'

# Additional sample-specific cut
special_cut = {
    # Example: 'zerodeg_150V_1e17': 'fabs(tt[2]-tt[3]+6.44)<0.12,
}
dut_amp_cut = '85' # mV

if __name__ == '__main__':
    uncorrected_results = []
    results = []
    if RUN_POINTS:
        # Loop over the data points
        for data_point in data_points:
            if data_point in special_cut:
                cut_string = default_cut_string + '&&{}'.format(special_cut[data_point])
            else:
                cut_string = default_cut_string
            cut_string_corr = ''
            if '5e16' in data_point and DO_SPECIAL_TREATMENT:
                # Do special treatment to treat saturation issues in DUT
                # Remove saturated events from eff computation and consider them fully efficient
                cut_string_corr = cut_string + '&&Amp[1]>{}'.format(dut_amp_cut)
                cut_string = cut_string + '&&Amp[1]<{}'.format(dut_amp_cut)
            if data_point in special_binning:
                result = runPoint(data_point,cut_string,cut_string_corr,special_binning[data_point])
            else:
                result = runPoint(data_point,cut_string,cut_string_corr,default_binning)
            uncorrected_results.append(result)
            if '5e16' in data_point and DO_SPECIAL_TREATMENT:
                # Apply special treatment
                result_corr = (
                    (result[3]+result[5])/(result[4]+result[5])*100, 
                    result[1], 
                    result[2],
                    result[3],
                    result[4],
                    result[5],
                    result[6],
                    result[7])
                results.append(result_corr)
            else:
                results.append(result)
    if len(results) != 0:
        print('Uncorrected results:')
        pprint(uncorrected_results)
        print('Corrected results:')
        pprint(results)
    else:
        print('Using previous results')

    if not RUN_POINTS:
        results = [(43.0, 0.41, 1.1707, 12140.9, 28235, 30000, 43.0, 0.35),
            (63.32, 0.49, 1.0535, 17692.9, 27940, 30000, 63.32, 0.28),
            (66.65, 0.5, 1.06417, 18594.3, 27897, 30000, 66.65, 0.27),
            (72.91, 0.54, 1.00198, 18509.1, 25387, 30000, 72.91, 0.26),
            (77.38, 0.53, 0.991682, 21576.8, 27884, 30000, 77.38, 0.22),
            (81.47, 0.54, 0.990716, 22720.1, 27888, 30000, 81.47, 0.2),
            (46.21, 0.43, 1.13814, 12917.5, 27955, 30000, 46.21, 0.35),
            (67.0, 0.5, 1.06896, 18729.1, 27954, 30000, 67.0, 0.27),
            (78.67, 0.54, 0.987498, 21943.8, 27895, 30000, 78.67, 0.23),
            (82.65, 0.55, 0.987526, 23110.3, 27962, 30000, 82.65, 0.21),
            (94.62, 0.58, 0.993196, 26373.6, 27872, 30000, 94.62, 0.12),
            (96.96, 0.59, 1.02498, 27075.2, 27924, 30000, 96.96, 0.09),
            (83.42, 0.55, 1.00858, 23831.4, 28567, 30000, 83.42, 0.2),
            (90.8, 0.57, 1.00215, 25249.1, 27808, 30000, 90.8, 0.15),
            # (90.53, 0.57, 1.00649, 25875.9, 28583, 30000, 90.53, 0.15),
            (76.95, 0.53, 1.00986, 21952.2, 28527, 30000, 76.95, 0.22)]
        
    # Extract angle,voltage and irradiation from names
    angles = [deg_name_lut[data_point.split('_')[0]] for data_point in data_points]
    voltages = [int(data_point.split('_')[1].replace('V', '')) for data_point in data_points]
    irradiations = [float(data_point.split('_')[2]) for data_point in data_points]

    # Put the results in a pandas df for easy manipulation
    df = pd.DataFrame({
        'angle': angles, 
        'voltage': voltages, 
        'irradiation': irradiations, 
        'efficiency_S': [result[0] for result in results], 
        'efficiency_S_error': [result[1] for result in results], 
        'chi2NDF': [result[2] for result in results],
        'events_passed': [result[4] for result in results], 
        'efficiency_B': [result[6] for result in results], 
        'efficiency_B_error': [result[7] for result in results], 
        })
        
    df = pd.concat([df, past_df], ignore_index=True)

    # Display the pandas dataframe
    print(df)

    # Make the plots with pyplot
    if PLT_PLOTS:

        # --------------------------------------------------------------
        # Plot the efficiency vs angle at 250 V for 1e17 irradiation
        fig_e_vs_angle, ax_e_vs_angle = plt.subplots()
        sel_1e17 = (df['irradiation']==1e17) & (df['voltage']==250)
        ax_e_vs_angle.errorbar(df[sel_1e17]['angle'].values,df[sel_1e17]['efficiency_S'].values,yerr=df[sel_1e17]['efficiency_S_error'].values,
                                    fmt='o', linewidth=1, markersize=3, label='1e17')
        sel_nonirr = (df['irradiation']==0) & (df['voltage']==100.0001)
        ax_e_vs_angle.errorbar(df[sel_nonirr]['angle'].values,df[sel_nonirr]['efficiency_S'].values,yerr=df[sel_nonirr]['efficiency_S_error'].values,
                                    fmt='o', linewidth=1, markersize=3, label='non-irr')
        # Set the labels and legend
        ax_e_vs_angle.set_xlabel('Angle [deg]')
        ax_e_vs_angle.set_ylabel('Efficiency [%]')
        ax_e_vs_angle.legend()

        # --------------------------------------------------------------
        # Plot the efficiency vs voltage at 0 and 20 deg for the 1e17 sensor
        fig_e_vs_v, ax_e_vs_v = plt.subplots()
        # Plot the 1e17 irradiation at 0 and 20 deg
        for angle in [0, 20]:
            sel = (df['irradiation']==1e17) & (df['angle']==angle)
            ax_e_vs_v.errorbar(df[sel]['voltage'].values,df[sel]['efficiency_S'].values,yerr=df[sel]['efficiency_S_error'].values, 
                                fmt='o', linewidth=1, markersize=3, label='1e17, {} deg'.format(angle))
        # Plot the 5e16 sensor at 0
        sel = (df['irradiation']==5e16) & (df['angle']==0) 
        ax_e_vs_v.errorbar(df[sel]['voltage'].values,df[sel]['efficiency_S'].values,yerr=df[sel]['efficiency_S_error'].values, 
                                fmt='o', linewidth=1, markersize=3, label='5e16, 0 deg'.format(angle))
        # Plot the non-irradiated sensor at 0
        sel = (df['irradiation']==0) & (df['angle']==0) & (df['voltage'] != 100.0001)
        ax_e_vs_v.errorbar(df[sel]['voltage'].values,df[sel]['efficiency_S'].values,yerr=df[sel]['efficiency_S_error'].values, 
                                fmt='o', linewidth=1, markersize=3, label='non-irr, 0 deg'.format(angle))
        
        # Set the labels and legend
        ax_e_vs_v.set_xlabel('Reverse bias voltage [V]')
        ax_e_vs_v.set_ylabel('Efficiency [%]')
        ax_e_vs_v.legend()

        # --------------------------------------------------------------
        # Control plot for the fitting
        # Create the figure and ax for the chi2NDF vs voltage and irradiation plot
        fig_chi2, ax_chi2 = plt.subplots()
        # The x axis will be the data point index
        # Plot of the chi2 for every point
        ax_chi2.plot(range(len(data_points)), [result[2] for result in results],linewidth=0, marker='o')
        # Change the name of each bin in a string with the angle, voltage and irradiation
        ax_chi2.set_xticks(range(len(data_points)))
        ax_chi2.set_xticklabels(data_points, rotation=45, ha='right')
        # Add more ticks to y axis
        ax_chi2.yaxis.set_major_locator(plt.MaxNLocator(10))
        # Add dashed grid lines for x and y
        ax_chi2.grid(axis='x', linestyle='--')
        ax_chi2.grid(axis='y', linestyle='--')
        # Set the labels
        ax_chi2.set_xlabel('Data point')
        ax_chi2.set_ylabel(r'$\chi^2$/NDF')
        # # Increase the bottom margin to fit the labels
        fig_chi2.subplots_adjust(bottom=0.4)

        # # Show the plots
        plt.show()

    if ROOT_PLOTS:
        import ROOT
        ROOT.gStyle.SetTextFont(132)
        ROOT.gStyle.SetLabelFont(132,"xyz")
        ROOT.gStyle.SetTitleFont(132,"xyz")
        ROOT.gStyle.SetLegendFont(132)

        if (CVD_FRIENDLY):
            # CVD-friendly color code
            col_non_irr = ROOT.TColor.GetColor('#7a21dd')
            col_1e17_0 = ROOT.TColor.GetColor('#f89c20')
            col_1e17_20 = ROOT.TColor.GetColor('#e42536')
            col_5e16 = ROOT.TColor.GetColor('#9c9ca1')
        else:
            # CVD-friendly color code
            col_non_irr = ROOT.kBlack
            col_1e17_0 = ROOT.kRed
            col_1e17_20 = ROOT.kRed
            col_5e16 = ROOT.kBlue

        # --------------------------------------------------------------
        # Plot of efficiency vs. angle
        # --------------------------------------------------------------
        c_e_vs_angle = ROOT.TCanvas('c_e_vs_angle', 'c_e_vs_angle', 800, 800)
        sel_1e17 = (df['irradiation']==1e17) & (df['voltage']==250)
        sel_nonirr = (df['irradiation']==0) & (df['voltage']==100.0001)
        
        x_1e17 = np.array(df[sel_1e17]['angle'].values, dtype=np.float64)
        y_1e17 = np.array(df[sel_1e17]['efficiency_S'].values, dtype=np.float64)
        yerr_1e17 = np.array(df[sel_1e17]['efficiency_S_error'].values, dtype=np.float64)
        xerr_1e17 = np.array(np.ones_like(x_1e17)*0, dtype=np.float64)
        x_1e17,y_1e17,xerr_1e17,yerr_1e17 = orderArrays(x_1e17, y_1e17, xerr_1e17, yerr_1e17)

        x_nonirr = np.array(df[sel_nonirr]['angle'].values, dtype=np.float64)
        y_nonirr = np.array(df[sel_nonirr]['efficiency_S'].values, dtype=np.float64)
        yerr_nonirr = np.array(df[sel_nonirr]['efficiency_S_error'].values, dtype=np.float64)
        xerr_nonirr = np.array(np.ones_like(x_nonirr)*0, dtype=np.float64)
        x_nonirr,y_nonirr,xerr_nonirr,yerr_nonirr = orderArrays(x_nonirr, y_nonirr, xerr_nonirr, yerr_nonirr)

        g_1e17_e_vs_angle = ROOT.TGraphErrors(len(x_1e17), x_1e17, y_1e17, xerr_1e17, yerr_1e17)
        g_1e17_e_vs_angle.SetMarkerStyle(20)
        g_1e17_e_vs_angle.SetMarkerSize(1.5)
        g_1e17_e_vs_angle.SetMarkerColor(col_1e17_0)
        g_1e17_e_vs_angle.SetLineColor(col_1e17_0)
        g_1e17_e_vs_angle.SetLineWidth(2)
        

        g_nonirr_e_vs_angle = ROOT.TGraphErrors(len(x_nonirr), x_nonirr, y_nonirr, xerr_nonirr, yerr_nonirr)
        g_nonirr_e_vs_angle.SetMarkerStyle(20)
        g_nonirr_e_vs_angle.SetMarkerSize(1.5)
        g_nonirr_e_vs_angle.SetMarkerColor(col_non_irr)
        g_nonirr_e_vs_angle.SetLineColor(col_non_irr)
        g_nonirr_e_vs_angle.SetLineWidth(2)

        mg_e_vs_angle = ROOT.TMultiGraph()
        mg_e_vs_angle.Add(g_1e17_e_vs_angle)
        mg_e_vs_angle.Add(g_nonirr_e_vs_angle)
        mg_e_vs_angle.Draw('ALP')

        mg_e_vs_angle.GetXaxis().SetTitle('Angle [deg]')
        mg_e_vs_angle.GetYaxis().SetTitle('Efficiency [%]')
        mg_e_vs_angle.GetYaxis().SetRangeUser(75, 101)


        leg_e_vs_angle = ROOT.TLegend(0.43, 0.12, 0.88, 0.2)
        leg_e_vs_angle.SetLineColor(0)
        leg_e_vs_angle.AddEntry(g_nonirr_e_vs_angle, "Not irradiated - 2022 data - 100 V", "lp")
        leg_e_vs_angle.AddEntry(g_1e17_e_vs_angle, "1 #upoint 10^{17} 1 MeV n_{eq} / cm^{2} - 250 V", "lp")
        # leg_e_vs_angle.AddEntry(gr2, "5 #upoint 10^{16} 1-MeV n_{eq} / cm^{2}", "lp")
        leg_e_vs_angle.Draw()
        c_e_vs_angle.Update()
        c_e_vs_angle.Draw()
        c_e_vs_angle.SaveAs('efficiency_vs_angle.pdf')
        c_e_vs_angle.SaveAs('efficiency_vs_angle.png')


        # --------------------------------------------------------------
        # Plot of efficiency vs. voltage
        # --------------------------------------------------------------
        # Set error to 0 for points at 1e17 and 20 degrees - they are smaller than the marker
        df.loc[(df['irradiation']==1e17) & (df['angle']==20), 'efficiency_S_error'] = 0

        c_e_vs_v = ROOT.TCanvas('c_e_vs_v', 'c_e_vs_v', 800, 800)
        sel_1e17_0 = (df['irradiation']==1e17) & (df['angle']==0)
        sel_1e17_20 = (df['irradiation']==1e17) & (df['angle']==20)
        sel_5e16 = (df['irradiation']==5e16) & (df['angle']==0)
        sel_nonirr = (df['irradiation']==0) & (df['voltage']!=100.0001) & (df['angle']==0)
        
        x_1e17_0 = np.array(df[sel_1e17_0]['voltage'].values, dtype=np.float64)
        y_1e17_0 = np.array(df[sel_1e17_0]['efficiency_S'].values, dtype=np.float64)
        yerr_1e17_0 = np.array(df[sel_1e17_0]['efficiency_S_error'].values, dtype=np.float64)
        xerr_1e17_0 = np.array(np.ones_like(x_1e17_0)*0, dtype=np.float64)
        x_1e17_0,y_1e17_0,xerr_1e17_0,yerr_1e17_0 = orderArrays(x_1e17_0, y_1e17_0, xerr_1e17_0, yerr_1e17_0)

        x_1e17_20 = np.array(df[sel_1e17_20]['voltage'].values, dtype=np.float64)
        y_1e17_20 = np.array(df[sel_1e17_20]['efficiency_S'].values, dtype=np.float64)
        yerr_1e17_20 = np.array(df[sel_1e17_20]['efficiency_S_error'].values, dtype=np.float64)
        xerr_1e17_20 = np.array(np.ones_like(x_1e17_20)*0, dtype=np.float64)
        x_1e17_20,y_1e17_20,xerr_1e17_20,yerr_1e17_20 = orderArrays(x_1e17_20, y_1e17_20, xerr_1e17_20, yerr_1e17_20)

        x_5e16 = np.array(df[sel_5e16]['voltage'].values, dtype=np.float64)
        y_5e16 = np.array(df[sel_5e16]['efficiency_S'].values, dtype=np.float64)
        yerr_5e16 = np.array(df[sel_5e16]['efficiency_S_error'].values, dtype=np.float64)
        xerr_5e16 = np.array(np.ones_like(x_5e16)*0, dtype=np.float64)
        x_5e16,y_5e16,xerr_5e16,yerr_5e16 = orderArrays(x_5e16, y_5e16, xerr_5e16, yerr_5e16)

        x_nonirr = np.array(df[sel_nonirr]['voltage'].values, dtype=np.float64)
        y_nonirr = np.array(df[sel_nonirr]['efficiency_S'].values, dtype=np.float64)
        yerr_nonirr = np.array(df[sel_nonirr]['efficiency_S_error'].values, dtype=np.float64)
        xerr_nonirr = np.array(np.ones_like(x_nonirr)*0, dtype=np.float64)
        x_nonirr,y_nonirr,xerr_nonirr,yerr_nonirr = orderArrays(x_nonirr, y_nonirr, xerr_nonirr, yerr_nonirr)

        g_nonirr_e_vs_v = ROOT.TGraphErrors(len(x_nonirr), x_nonirr, y_nonirr, xerr_nonirr, yerr_nonirr)
        g_nonirr_e_vs_v.SetMarkerStyle(20)
        g_nonirr_e_vs_v.SetMarkerSize(1.5)
        g_nonirr_e_vs_v.SetMarkerColor(col_non_irr)
        g_nonirr_e_vs_v.SetLineColor(col_non_irr)
        g_nonirr_e_vs_v.SetLineWidth(2)

        g_5e16_e_vs_v = ROOT.TGraphErrors(len(x_5e16), x_5e16, y_5e16, xerr_5e16, yerr_5e16)
        g_5e16_e_vs_v.SetMarkerStyle(20)
        g_5e16_e_vs_v.SetMarkerSize(1.5)
        g_5e16_e_vs_v.SetMarkerColor(col_5e16)
        g_5e16_e_vs_v.SetLineColor(col_5e16)
        g_5e16_e_vs_v.SetLineWidth(2)   

        g_1e17_0_e_vs_v = ROOT.TGraphErrors(len(x_1e17_0), x_1e17_0, y_1e17_0, xerr_1e17_0, yerr_1e17_0)
        g_1e17_0_e_vs_v.SetMarkerStyle(20)
        g_1e17_0_e_vs_v.SetMarkerSize(1.5)
        g_1e17_0_e_vs_v.SetMarkerColor(col_1e17_0)
        g_1e17_0_e_vs_v.SetLineColor(col_1e17_0)
        g_1e17_0_e_vs_v.SetLineWidth(2)

        g_1e17_20_e_vs_v = ROOT.TGraphErrors(len(x_1e17_20), x_1e17_20, y_1e17_20, xerr_1e17_20, yerr_1e17_20)
        g_1e17_20_e_vs_v.SetMarkerStyle(20)
        g_1e17_20_e_vs_v.SetMarkerSize(1.5)
        g_1e17_20_e_vs_v.SetMarkerColor(col_1e17_20)
        g_1e17_20_e_vs_v.SetLineColor(col_1e17_20)
        g_1e17_20_e_vs_v.SetLineWidth(2)

        if not CVD_FRIENDLY:
            g_1e17_20_e_vs_v.SetMarkerStyle(4)

        mg_e_vs_v = ROOT.TMultiGraph()
        mg_e_vs_v.Add(g_nonirr_e_vs_v)
        mg_e_vs_v.Add(g_5e16_e_vs_v)
        mg_e_vs_v.Add(g_1e17_0_e_vs_v)
        mg_e_vs_v.Add(g_1e17_20_e_vs_v)
        mg_e_vs_v.Draw('ALP')

        mg_e_vs_v.GetXaxis().SetTitle('Reverse Bias [V]')
        mg_e_vs_v.GetYaxis().SetTitle('Efficiency [%]')
        mg_e_vs_v.GetYaxis().SetRangeUser(30, 101)

        leg_e_vs_angle_1 = ROOT.TLegend(0.11, 0.12, 0.48, 0.2)
        leg_e_vs_angle_1.SetLineColor(0)
        leg_e_vs_angle_1.AddEntry(g_nonirr_e_vs_v, "Not irradiated - 2022 data - 0#circ", "lp")
        leg_e_vs_angle_1.AddEntry(g_5e16_e_vs_v, "5 #upoint 10^{16} 1 MeV n_{eq} / cm^{2} - 0#circ", "lp")
        leg_e_vs_angle_1.Draw()
        leg_e_vs_angle_2 = ROOT.TLegend(0.49, 0.12, 0.88, 0.2)
        leg_e_vs_angle_2.SetLineColor(0)
        leg_e_vs_angle_2.AddEntry(g_1e17_0_e_vs_v, "1 #upoint 10^{17} 1 MeV n_{eq} / cm^{2} - 0#circ", "lp")
        leg_e_vs_angle_2.AddEntry(g_1e17_20_e_vs_v, "1 #upoint 10^{17} 1 MeV n_{eq} / cm^{2} - 20#circ", "lp")
        leg_e_vs_angle_2.Draw()
        c_e_vs_v.Update()
        c_e_vs_v.Draw()
        c_e_vs_v.SaveAs('efficiency_vs_voltage.pdf')
        c_e_vs_v.SaveAs('efficiency_vs_voltage.png')

        input('Press enter to continue')
