
TTree* Make_OR_Read_Tree(TString dir, TString file) { 
  ifstream myFile((dir+file).Data());
  //cout << "Path: " << dir+file << endl;
    
  file.Remove(file.Length()-8, 8); //remove suffix
  file.Remove(0,13);               //remove prefix
  //TString treeFileName = "data/treeFile"+file;
  TString treeFileName = "/lhcbdata/timespot/testbeam_SPS2021/tree_SPS2021/treeFile"+file;
#ifdef INTERPOLAZIONE
  treeFileName+="_Interpola";
#endif
  treeFileName+=".root";

    cout << "Tree path: " << treeFileName << endl;
    
  bool makeTree=false;  
  if(!file_exists(treeFileName.Data())) makeTree=true;
  else {
    TFile *treeFile = new TFile(treeFileName,"READ");
    //if(!treeFile->Get("tree")) makeTree=true;
    if(!treeFile->Get("tree")) makeTree=true;
    else if(((TTree*) treeFile->Get("tree"))->GetEntries()==0)  makeTree=true;
  }
  if(makeTree){
    if (!myFile.is_open()) {
      cout<<" File doesn't exist "<<endl;
      return NULL; 
    }
    TString line;
    Int_t i_wave;
    Float_t  t_min=1000, t_max=-10000;
#ifdef INTERPOLAZIONE
    std::vector<Double_t> time;
    std::vector<Double_t> Signal_1;
    std::vector<Double_t> Signal_2;
    std::vector<Double_t> Signal_3;
    std::vector<Double_t> Signal_4;
#else
    std::vector<Float_t> time;
    std::vector<Float_t> Signal_1;
    std::vector<Float_t> Signal_2;
    std::vector<Float_t> Signal_3;
    std::vector<Float_t> Signal_4;
#endif    
    TFile *treeFile = new TFile(treeFileName,"RECREATE");  
    // Create a TTree
    TTree *tree = new TTree("tree","Tree from TestBeam @ SPS2021");
    tree->Branch("nwave",&i_wave);
    tree->Branch("time",&time);
    tree->Branch("Signal_1",&Signal_1);
    tree->Branch("Signal_2",&Signal_2);
    tree->Branch("Signal_3",&Signal_3);
    tree->Branch("Signal_4",&Signal_4);

    
    Int_t j=0;
    while (j>-1 /*&& j<10000*/) {                  // Loop for exctract data
      line.ReadLine(myFile);
      if (!myFile.good()) break;
      TObjArray *tokens = line.Tokenize(";");
    
      if (tokens->GetEntries()==1){ // New event found       
          if(j%10000==0)cout<<" Reading event n."<<j<<endl;
          if( j>0 ) tree->Fill();
          j++;
	time.clear();
	Signal_1.clear();
	Signal_2.clear();
	Signal_3.clear();
	Signal_4.clear();
	i_wave = j;       
      
      } else {      
	Float_t t  = atof( ((TObjString*) tokens->At(0))->GetString() )*1e9; // ns
	Float_t v1 = atof( ((TObjString*) tokens->At(1))->GetString() )*1e3; // mV
	Float_t v2 = atof( ((TObjString*) tokens->At(2))->GetString() )*1e3;
	Float_t v3 = atof( ((TObjString*) tokens->At(3))->GetString() )*1e3; // Attenzione!! 
#ifdef CH4
	Float_t v4 = atof( ((TObjString*) tokens->At(4))->GetString() )*1e3;
#else
        Float_t v4 = v1;
#endif
	if(j==1) {
	  if(t_min > t) t_min=t;
	  if(t_max < t) t_max=t;
	}
#ifdef INTERPOLAZIONE
	std::vector<Double_t> _time_1 = time;
	std::vector<Double_t> _Signal_1 = Signal_1;
	std::vector<Double_t> _Signal_2 = Signal_2;
	std::vector<Double_t> _Signal_3 = Signal_3;
	std::vector<Double_t> _Signal_4 = Signal_4;
	
	ROOT::Math::Interpolator inter1(_time_1.size(), ROOT::Math::Interpolation::kCSPLINE);
	inter1.SetData(_time_1.size(),&_time_1[0],&_Signal_1[0]); 
	
	ROOT::Math::Interpolator inter2(_time_1.size(), ROOT::Math::Interpolation::kCSPLINE);
	inter2.SetData(_time_1.size(),&_time_1[0],&_Signal_2[0]); 

	ROOT::Math::Interpolator inter3(_time_1.size(), ROOT::Math::Interpolation::kCSPLINE);
	inter3.SetData(_time_1.size(),&_time_1[0],&_Signal_3[0]); 

	ROOT::Math::Interpolator inter4(_time_1.size(), ROOT::Math::Interpolation::kCSPLINE);
	inter4.SetData(_time_1.size(),&_time_1[0],&_Signal_4[0]); 

	time.clear();
	Signal_1.clear();
	Signal_2.clear();
	Signal_3.clear();
	Signal_4.clear();
	//cout<<"------ Signal1 ------"<<_Signal_1.size()<<endl;
	//for(Int_t i_interpol=0; i_interpol<_Signal_1.size(); i_interpol++) cout<<_Signal_1.at(i_interpol)<<" ";
	//cout<<endl;
	//cout<<" ++++++ "<<endl;
	time.push_back(t_min);
	Signal_1.push_back(_Signal_1.at(0));  
	Signal_2.push_back(_Signal_2.at(0));  
	Signal_3.push_back(_Signal_3.at(0));  
	Signal_4.push_back(_Signal_4.at(0));  
	Int_t N1i = (_time_1.size()-1)*Ninterpol; // 3996
	for(Int_t i_interpol=1; i_interpol<N1i ; i_interpol++) {
	  Double_t t1i  = (Double_t) (i_interpol*(t_max-t_min)/((_time_1.size()-1)*Ninterpol) + t_min);
	  Double_t v1i = inter1.Eval(t1i);	  
	  Double_t v2i = inter2.Eval(t1i);	  
	  Double_t v3i = inter3.Eval(t1i);	  
	  Double_t v4i = inter4.Eval(t1i);	  
	  time.push_back(t1i);
      Signal_1.push_back(v1i);
      Signal_2.push_back(v2i);
      Signal_3.push_back(v3i);
      Signal_4.push_back(v4i);
	}      
	//time.push_back(t_max);   Signal_1.push_back(_Signal_1.at(_Signal_1.size()-1));      Signal_2.push_back(_Signal_2.size()-1);
#else          
	time.push_back(t);
    Signal_1.push_back(v1);
    Signal_2.push_back(v2);
    Signal_3.push_back(v3);
    Signal_4.push_back(v4);
          
#endif  
	
      }
      delete tokens;
    }
    tree->Fill();  

    tree->Write();
    treeFile->Close();
    cout << "EOF found after " << j << " waveforms " <<endl;
    cout<<"Time range: "<< t_min<<" - "<<t_max<<endl;    

    myFile.close();
  }

  TFile *treeFile = new TFile(treeFileName,"READ");
  // Read the TTree
  TTree *_tree = (TTree*) treeFile->Get("tree");
  cout<<"Reading tree: "<< treeFileName<<" with "<< _tree->GetEntries()<<endl;    
  _tree->Print("V");
  return _tree;
}


//**************************************************************
TTree* Make_OR_Read_Tree_SIM(TString dir, TString file) { 
//**************************************************************
  TString treeFileName = dir+"/treeFile"+file;
#ifdef noZONABALORDA
  treeFileName+="_noZONABALORDA";
  //treeFileName+="_soloCentralPIXEL";
#endif  
#ifdef siZONABALORDA
  treeFileName+="_soloZONABALORDA";
  //treeFileName+="_soloCentralPIXEL";
#endif  
#ifdef INTERPOLA
  treeFileName+="_Interpola";
#endif
  treeFileName+=".root";

  TString file_pos = "position.dat";

  bool makeTree=false;  
  if(!file_exists(treeFileName.Data())) makeTree=true;
  else {
    TFile *treeFile = new TFile(treeFileName,"READ");
    if(!treeFile->Get("tree")) makeTree=true;
    else if(((TTree*) treeFile->Get("tree"))->GetEntries()==0)  makeTree=true;
  }
  if(makeTree){
    TString line, line_pos;
    Int_t i_wave;
    Float_t  t_min=1000, t_max=-10000;
#ifdef INTERPOLA
    std::vector<Double_t> time;
    std::vector<Double_t> Signal_1;
#else
    std::vector<Float_t> time;
    std::vector<Float_t> Signal_1;
#endif    
    Float_t x;
    Float_t y;
    TFile *treeFile = new TFile(treeFileName,"RECREATE");  
    // Create a TTree
    TTree *tree = new TTree("tree","Tree from Simulated Waveforms");
    tree->Branch("nwave",&i_wave);
    tree->Branch("time",&time);
    tree->Branch("Signal_1",&Signal_1);
    tree->Branch("x",&x);
    tree->Branch("y",&y);



    
    Int_t nfile=0;

    Int_t j=0;
    /*
    void list_files(const char *dirname="C:/root/folder/", const char *ext=".root") {
      TSystemDirectory dir(dirname, dirname);
      TList *files = dir.GetListOfFiles();
      if (files) { TSystemFile *file;
	TString fname; TIter next(files);
	while ((file=(TSystemFile*)next())) {
	  fname = file->GetName();
	  if (!file->IsDirectory() && fname.EndsWith(ext)) {
	    cout << fname.Data() << endl; }
	}
      }
    }
    */
    TSystemDirectory mydir(dir, dir);
    cout<<" Reading from "<<dir.Data()<<endl;
    mydir.Print();
    TList *listOFfiles = mydir.GetListOfFiles();
    if (!listOFfiles) { cout<<" directory inaccessibile "<<dir.Data()<<endl; return 0;}
    TIter next(listOFfiles);
    TSystemFile *_file;
    TString fname;
    while ((_file=(TSystemFile*)next())) {
      fname = _file->GetName();
      if (_file->IsDirectory()) continue;
      //if(!fname.EndsWith(".txt")) continue;   
      if(!fname.BeginsWith("Simulation")) continue;   
      cout<<" Reading "<<(dir+fname).Data()<<endl;
      float xcoord = atof(fname(11,16).Data());
      float ycoord;
      if (xcoord>100.) ycoord = atof(fname(22,27).Data());
      else  ycoord = atof(fname(21,26).Data());
      //cout<<" ("<<xcoord<<","<<ycoord<<")"<<endl;
#ifdef noZONABALORDA      
      if (xcoord >165 ) continue;
#endif
#ifdef siZONABALORDA      
      if (xcoord <165 ) continue;
#endif
      ifstream    myFile((dir+fname).Data());
      delete _file;
      
      //for (int j=1; j<9999; j++) {
      //file.Form("Simulation%d_0.dat",j);
      //cout<<" Reading "<<(dir+file).Data()<<endl;
      //ifstream myFile((dir+file).Data());
      if (!myFile.is_open()) {
	cout<<" File doesn't exist "<<endl;
	continue;
      }
      j++;
      if( j>1) {  
	time.clear();
	Signal_1.clear();
      }
      bool flag=true;
      bool found=false;
      bool zona_balorda=false;
      /*
#ifdef noZONABALORDA      
      Float_t X(0.), Y(0.), Charge(0.);
      ifstream myFile_pos((dir+file_pos).Data());
      if (myFile_pos.is_open()) {
	while (!found) {
	  line_pos.ReadLine(myFile_pos);
	  line_pos.Data();
	  if (line_pos.IsNull()) {found=false; continue;}
	  if (!myFile_pos.good()) continue;
	  TObjArray *tokens_pos = line_pos.Tokenize(" ");      
	  Int_t n = atof( ((TObjString*) tokens_pos->At(0))->GetString() );
	  X = atof( ((TObjString*) tokens_pos->At(1))->GetString());
	  Y = atof( ((TObjString*) tokens_pos->At(2))->GetString());
	  Charge = atof( ((TObjString*) tokens_pos->At(3))->GetString());
	  if (n==j) {
	    found = true;
	    continue;
	  }
	}
	myFile_pos.close();
      } else { break;  }
      if( found && X > 165) {
	//if( found && (X < 55 || X > 110) ) {
	cout<<" Scartato file !! "<<j<<" "<<X<<endl;
	zona_balorda=true;
      }
#endif  
      */    
      if(!zona_balorda) {
	while (flag) {
	  line.ReadLine(myFile);
	  line.Data();
	  if (line.IsNull()) {flag=false; continue;}
	  if (!myFile.good()) continue;
	  TObjArray *tokens = line.Tokenize(" ");      
	  Float_t t  = atof( ((TObjString*) tokens->At(0))->GetString() )*1e9;// ns
	  //cout<<" t = "<<t;
	  t = float(int(t*100)/100.); // troncamento
	  //if (t>12.42 ) continue; //------------Vbias 20 
	  //cout<<" "<<t<<endl;
	  Float_t v1 = atof( ((TObjString*) tokens->At(1))->GetString() )*1e3; // 
	  //cout<< t<<" "<<v1<<" "<<time.size()<<endl;
	  if(nfile==0) {
	    if(t_min > t) t_min=t;
	    if(t_max < t) t_max=t;
	  }
	  i_wave = nfile;       
	  time.push_back(t);
	  Signal_1.push_back(SignalSign[0]*v1);
	  x = xcoord;
	  y = ycoord ;
	  delete tokens;
	  
	}
	
#ifdef INTERPOLA
	std::vector<Double_t> _time_1 = time;    std::vector<Double_t> _Signal_1 = Signal_1;
	
	ROOT::Math::Interpolator inter1(_time_1.size(), ROOT::Math::Interpolation::kCSPLINE);      inter1.SetData(_time_1.size(),&_time_1[0],&_Signal_1[0]); 
	
	time.clear();
	Signal_1.clear();
	//cout<<"------ Signal1 ------"<<_Signal_1.size()<<endl;
	//for(Int_t i_interpol=0; i_interpol<_Signal_1.size(); i_interpol++) cout<<_Signal_1.at(i_interpol)<<" ";
	//cout<<endl;
	//cout<<" ++++++ "<<endl;
	time.push_back(t_min);   Signal_1.push_back(_Signal_1.at(0));  
	Int_t N1i = (_time_1.size()-1)*Ninterpol; // 3996
	for(Int_t i_interpol=1; i_interpol<N1i ; i_interpol++) {
	  Double_t t1i  = (Double_t) (i_interpol*(t_max-t_min)/((_time_1.size()-1)*Ninterpol) + t_min);
	  Double_t v1i = inter1.Eval(t1i);
	  
	  time.push_back(t1i);      Signal_1.push_back(v1i);   
	}      
	//time.push_back(t_max);   Signal_1.push_back(_Signal_1.at(_Signal_1.size()-1));      Signal_2.push_back(_Signal_2.size()-1);
#endif  
	cout<<nfile<<" ------ "<<time.size()<<endl;
	tree->Fill();
      }
      nfile++;
      myFile.close();
    }
    tree->Write();
    treeFile->Close();
    cout << "EOF found after " << nfile << " waveforms " <<endl;
    if(nfile==0) return NULL;
    cout<<"Time range: "<< t_min<<" - "<<t_max<<endl;    

  }

  TFile *treeFile = new TFile(treeFileName,"READ");
  // Read the TTree
  TTree *_tree = (TTree*) treeFile->Get("tree");
  cout<<"Reading tree: "<< treeFileName<<" with "<< _tree->GetEntries()<<endl;    
  _tree->Print("V");
  return _tree;
}

//**************************************************************
TTree* Make_OR_Read_Tree_Laser(TString dir, TString file) { 
//**************************************************************  
  TString treeFileName = dir+"/treeFile"+file;
  treeFileName+=".root";


  bool makeTree=false;  
  if(!file_exists(treeFileName.Data())) makeTree=true;
  else {
    TFile *treeFile = new TFile(treeFileName,"READ");
    if(!treeFile->Get("tree")) makeTree=true;
    else if(((TTree*) treeFile->Get("tree"))->GetEntries()==0)  makeTree=true;
  }
  if(makeTree){
    TString line;
    Int_t i_wave;
    Float_t  t_min=1000, t_max=-10000;
    
    std::vector<Float_t> time;
    std::vector<Float_t> Signal_1, Signal_2;
    Float_t x;
    Float_t y;

    TFile *treeFile = new TFile(treeFileName,"RECREATE");  
    // Create a TTree
    TTree *tree = new TTree("tree","Tree from Laser mean Waveforms");
    tree->Branch("nwave",&i_wave);
    tree->Branch("time",&time);
    tree->Branch("Signal_1",&Signal_1);
    tree->Branch("Signal_2",&Signal_2);
    tree->Branch("x",&x);
    tree->Branch("y",&y);


    // Andrea Lampis==============================
    Int_t npos = 0;
    Int_t Nline =1e6;
    ifstream myFile((dir+file).Data());
    t_min=0;
    t_max=5.-5.e-3;
    if (!myFile.is_open()) {
      cout<<" File doesn't exist "<<endl;
      return NULL; 
    }
    while (!myFile.eof()&&npos<Nline)    {
      line.ReadLine(myFile);
      if (!myFile.good())
	break;
      TObjArray *tokens = line.Tokenize(",");
      Int_t Ncolumn = tokens->GetEntries();
      //cout<<Ncolumn<<endl;
      
      x=( (atof(((TObjString *)tokens->At(0))->GetString()))-12.54)*1000;
      y=((atof(((TObjString *)tokens->At(1))->GetString()))-1.83)*1000;
      for (Int_t i = 4; i < 1004; i++)   {
	time.push_back(5.e-3*(i-4.));//ns    ///t.push_back(5 * (i-4));	  
	Signal_1.push_back(SignalSign[0]*(1e3*atof(((TObjString *)tokens->At(i))->GetString())));
	Signal_2.push_back(SignalSign[1]*(1e3*atof(((TObjString *)tokens->At(i+2000))->GetString())));      			  
      }
      /*
      cout<<" X,Y=("<<x<<","<<y<<" \n";
      cout<<"Signal_1.size = "<<Signal_1.size();
      for(int i=0; i<10; i++) cout<<" "<<Signal_1.at(i);
      cout<<endl;
      cout<<"Signal_2.size = "<<Signal_2.size();
      for(int i=0; i<10; i++) cout<<" "<<Signal_2.at(i);
      cout<<endl;
      */
      Float_t max_2 = -1000;
      for(int i=0; i<Signal_2.size(); i++) max_2 = max(max_2, fabs(Signal_2.at(i)));
      if(max_2 > 15) { 
	i_wave = npos;
	tree->Fill();
      }
      Signal_1.clear();
      Signal_2.clear();
      time.clear();
      npos++;
      delete tokens;
      
    }    
    cout << "EOF found after " << npos << " waveforms " <<endl;        
    myFile.close();
    tree->Write();
    treeFile->Close();
    if(npos==0) return NULL;
    cout<<"Time range: "<< t_min<<" - "<<t_max<<endl;    
    
  }
  
  TFile *treeFile = new TFile(treeFileName,"READ");
  // Read the TTree
  TTree *_tree = (TTree*) treeFile->Get("tree");
  cout<<"Reading tree: "<< treeFileName<<" with "<< _tree->GetEntries()<<endl;    
  _tree->Print("V");
  return _tree;
  
}
//**************************************************************
Float_t eval_t0_Interpolator(TGraph *g, int kk){
  //std::tuple<Float_t, Float_t, Float_t, Float_t> eval_t0_Interpolator(Int_t kk, TGraph *g ){
//**************************************************************

  Float_t Amplitude = -1000;
  Float_t        c = 0.;
  Double_t *time   = g->GetX();
  Double_t *val    = g->GetY();
  Int_t Nbin       = g->GetN();
  Float_t     t_min = time[0];
  Float_t     t_max = time[Nbin-1];
  //==================== Parte di interpolazione ----- Codice di Andrea =========
  std::vector<Double_t> t, v;
    for(int i=0; i<Nbin; i++) {
    v.push_back(val[i]);
    t.push_back(time[i]);
  }
  ROOT::Math::Interpolator inter(v.size(), ROOT::Math::Interpolation::kCSPLINE);
  inter.SetData(t, v);
  Int_t points = v.size() * Ninterpol;
  vector<Double_t> v_int;
  vector<Double_t> t_int;
  // cout<<t.front()<<","<<t.back()<<","<<t.size()<<endl;
  for (Int_t i = 0; i < points; i++)  {
    t_int.push_back((Double_t)i * (t.back() - t.front()) / (points - 1) + t.front());
    v_int.push_back(inter.Eval(t_int[i]));
    //cout<<t_int[i]<<","<<v_int[i]<<endl;
  }
  
  //Int_t TOT=Ninterpol*10;
  Int_t TOT=Ninterpol*50;
  Int_t imax = std::max_element(v_int.begin()+TOT, v_int.end()) - v_int.begin();
  Double_t amplitude = *std::max_element(v_int.begin()+TOT, v_int.end());
  Amplitude = amplitude - c;
  Float_t TOA = -1000;
  
  for (Int_t j = imax; j > 0; j--)   {      
    if (v_int[j] < Amplitude * frac_thr + c)  {
            TOA = t_int[j];
            break;
    }
  }
  v_int.clear();
  t_int.clear();
  v_int.shrink_to_fit();
  t_int.shrink_to_fit();
  
  /*
  if(TOA<-50){
      cout << "canale: " << kk+1 << endl;
      cout << "TOA: " << TOA << endl;
      cout << "indice del massimo (eval_t0_interpolator): " << imax << endl;
      cout << "valore del massimo (eval_t0_interpolator): " << Amplitude << endl;
      cout << "valore della frazione del massimo (eval_t0_interpolator): " << Amplitude * frac_thr + c << endl;
  }
  */
  return TOA;  
  //return std::make_pair(TOA,Amplitude,c, sp);
}
  
//**************************************************************
std::tuple<Float_t, Float_t, Float_t, Float_t, Float_t> eval_Amp_PSI(Int_t kk, TGraph *g, Float_t Range, TF1* BaselineFIT ){
//**************************************************************

  Float_t Amplitude = -1000;
  Float_t min_Amplitude = 1000;
  Float_t        c = 0.;
  Double_t *time   = g->GetX();
  Double_t *val    = g->GetY();
  Int_t Nbin       = g->GetN();
  Float_t     t_min = time[0];
  Float_t     t_max = time[Nbin-1];
  Float_t     ymin = TMath::MinElement(Nbin,g->GetY());
  Float_t     ymax = TMath::MaxElement(Nbin,g->GetY());
  Long64_t yminloc = TMath::LocMin(Nbin,g->GetY());
  Long64_t ymaxloc = TMath::LocMax(Nbin,g->GetY());
  Float_t     xmax = time[ymaxloc];

  Long64_t ymaxloc1 = TMath::LocMax(300.,g->GetY());
    
  /*
  if(xmax < Range ) {
    cout<<" ALERT !!!!! Redefinition of Range["<<kk<<"] "<<Range<<" "<<xmax<<endl;
    //return std::make_pair(-10, 0.);

    //if(xmax-0.4<t_min) return std::make_pair(-10, 0.);
    //Range = xmax-0.4;
    //if(xmax-0.4<t_min) Range = xmax;

  }
  */

  if (BaselineFIT!=NULL) {
    TString fun_name("_BaselineFIT");
    TF1* _BaselineFIT = new TF1(fun_name,sinFMPIUconst,t_min,t_max,5);
    _BaselineFIT->SetParameter(0,BaselineFIT->GetParameter(0) );
#ifdef RADIOMARIA
    _BaselineFIT->SetParameter(1,0.3 );
#else
    _BaselineFIT->FixParameter(1,0); //////// Amplitude of sinus term FIXED to Zero
#endif
    _BaselineFIT->FixParameter(2,BaselineFIT->GetParameter(2) );
    _BaselineFIT->SetParameter(3,BaselineFIT->GetParameter(3) );
    _BaselineFIT->SetParameter(4,BaselineFIT->GetParameter(4) );
    //_BaselineFIT->FixParameter(5,BaselineFIT->GetParameter(5) );
    //_BaselineFIT->SetParameter(6,BaselineFIT->GetParameter(6) );
    _BaselineFIT->SetRange(t_min,Range);
   
    Float_t prob =0;
    _BaselineFIT->SetRange(t_min,Range);
    for(int iloop=0; iloop<nLoop; iloop++){
      if( prob < 0.4) {
    g->Fit(_BaselineFIT,"QR","",t_min,Range);
    prob = g->GetFunction(_BaselineFIT->GetName())->GetProb();
      }
    }
    c = _BaselineFIT->Eval(xmax,0,0);
    Amplitude = ymax - c;
    min_Amplitude = ymin - c;
    delete _BaselineFIT;
  } else {
    Amplitude = ymax;
    min_Amplitude = ymin;
    c = 0.;
  }
  //  cout<< " Amplitude = "<<Amplitude<<" ymax="<<ymax<<" c="<<c<<endl;
  //per calcolare la variabile spettro
  Float_t sp = 0;
  

  for( int i=ymaxloc-5 ; i<ymaxloc+5; i++) sp=sp+val[i];
  
  Float_t sp1 = 0;
  for( int i=ymaxloc1-5 ; i<ymaxloc1+5; i++) sp1=sp1+val[i];
    
  return std::make_tuple(Amplitude, c, sp, min_Amplitude, ymaxloc);
    
}



//**************************************************************
void fit_baseline(Int_t kk, TGraph *g, Float_t Range, TF1* BaselineFIT ){
  //std::pair<Float_t, Float_t> p = eval_Amp_PSI(kk, g, Range, BaselineFIT );
   std::tuple<Float_t, Float_t, Float_t, Float_t, Float_t> p = eval_Amp_PSI(kk, g, Range, BaselineFIT );
  return;
}
//**************************************************************
std::pair<Float_t, Float_t> eval_Noise(TGraph *g, Float_t Range){
//**************************************************************
  TH1F *Histo = new TH1F("Histo","Histo",100,0.,0.);          
  Double_t *time        = g->GetX();
  Double_t *_Signal     = g->GetY();
  Int_t Nbin=g->GetN();
  Float_t  t_min = time[0];
  Float_t  t_max = time[Nbin-1];
  Int_t Nnoise=0;
  
  for(int i=0; i<Nbin; i++) {
    if(Range > t_min) {
      //if( time[i] <t_min+6.37+1.257) // dati
      //if( time[i] < Range || time[i]>8.5) // 20V time[i]>10.) // simulazione
	if( time[i] < Range ) Histo->Fill(_Signal[i]);
    } else {   
      if( time[i] > t_max-0.8) Histo->Fill(_Signal[i]);    
    }
  }
  
  Float_t RMS=0.;
  for(int i=0; i<Nbin; i++) {
    //if(time[i] < t_min+6.37+1.257) { //dati
    //if(Range > t_min && (time[i] < Range || time[i]>8.5)) { // 20V time[i]>10.)) {  // Simulazione  //
    if(Range > t_min && time[i] < Range) {
      RMS += (_Signal[i])*(_Signal[i]);
      Nnoise+=1;
    }
  }
  RMS=sqrt(RMS/float(Nnoise));
  
  Float_t Noise = Histo->GetRMS();
  delete Histo;
  return std::make_pair(Noise,RMS);
}
//**************************************************************
std::pair<Float_t, Float_t> eval_Noise_SIM(TGraph *g, Float_t Range){
//**************************************************************
  TH1F *Histo = new TH1F("Histo","Histo",100,0.,0.);          
  Double_t *time        = g->GetX();
  Double_t *_Signal     = g->GetY();
  Int_t Nbin=g->GetN();
  Float_t  t_min = time[0];
  Float_t  t_max = time[Nbin-1];
  Int_t Nnoise=0;
  Float_t t_max_RMS = 10.;
  if(t_max<16) t_max_RMS = 9.4;
  //test rinormalizzazione campione Vbias=-20V  //t_max_RMS = 9.4;  //Nbin=621;
  for(int i=0; i<Nbin; i++) {
    if(Range > t_min) {
      if( time[i] < Range || time[i]> t_max_RMS) //time[i]>10.) // simulazione   // v20 >9.4-12.4 + 1.34    = 4.34
	Histo->Fill(_Signal[i]);
    } else {   
      if( time[i] > t_max-0.8) Histo->Fill(_Signal[i]);    
    }
  }
  
  Float_t RMS=0.;
  for(int i=0; i<Nbin; i++) {
    if(Range > t_min && (time[i] < Range || time[i]> t_max_RMS)) {//time[i]>10.)) {  // Simulazione  //time[i]>8.5)) { // 20V    //6.36 + Range=1.19
      RMS += (_Signal[i])*(_Signal[i]);
      Nnoise+=1;
    }
  }
  RMS=sqrt(RMS/float(Nnoise));

  Float_t Noise = Histo->GetRMS();
  delete Histo;
  return std::make_pair(Noise,RMS);

}
//**************************************************************
Float_t eval_BaselineLASER(TGraph *g){
//**************************************************************
  TH1F *Histo = new TH1F("Histo","Histo",100,0.,0.);          
  Double_t *time        = g->GetX();
  Double_t *_Signal     = g->GetY();
  Int_t Nbin=g->GetN();
  Float_t  t_min = time[0];
  Float_t  t_max = time[Nbin-1];
  
  for(int i=0; i<Nbin; i++) {
    if( time[i] < -28 || (time[i] > -11 && time[i] < -4) || time[i]>13 )
      Histo->Fill(_Signal[i]);
  }
  Float_t Baseline = Histo->GetMean();
  delete Histo;
  return Baseline;
}
//**************************************************************
TGraph* do_BASELINEfree(TGraph *g){
//**************************************************************

  Double_t *time        = g->GetX();
  Double_t *_Signal     = g->GetY();
  Int_t Nbin            = g->GetN();
  Float_t         t_min = time[0];
  Float_t         t_max = time[Nbin-1];

  TF1 *_BaselineFIT = (TF1*) g->GetFunction("_BaselineFIT");
  std::vector<Double_t> _Signal_baselineFREE;
  for (int j=0; j<Nbin; j++) _Signal_baselineFREE.push_back(_Signal[j] - _BaselineFIT->Eval(time[j])); // segnale sottratto 

  TGraph *g_baselineFREE = new TGraph(Nbin,&time[0],&(_Signal_baselineFREE.at(0)));  
  g_baselineFREE->GetXaxis()->SetRangeUser(t_min, t_max);
  g_baselineFREE->SetTitle("baselineFREE");                  g_baselineFREE->SetLineColor(4);
  g_baselineFREE->GetXaxis()->SetTitle("Time [ns]");         g_baselineFREE->GetYaxis()->SetTitle("subtracted signal [mV]");

  _Signal_baselineFREE.clear();
  _Signal_baselineFREE.shrink_to_fit();
  //  delete _BaselineFIT;
  return g_baselineFREE;
}

//**************************************************************
TGraph* do_BASELINEfree_LASER(TGraph *g, Float_t baseline){
//**************************************************************

  Double_t *time        = g->GetX();
  Double_t *_Signal     = g->GetY();
  Int_t Nbin            = g->GetN();
  Float_t         t_min = time[0];
  Float_t         t_max = time[Nbin-1];

  std::vector<Double_t> _Signal_baselineFREE;
  for (int j=0; j<Nbin; j++) _Signal_baselineFREE.push_back(_Signal[j] - baseline); // segnale sottratto 

  TGraph *g_baselineFREE = new TGraph(Nbin,&time[0],&(_Signal_baselineFREE.at(0)));  
  g_baselineFREE->GetXaxis()->SetRangeUser(t_min, t_max);
  g_baselineFREE->SetTitle("baselineFREE");                  g_baselineFREE->SetLineColor(4);
  g_baselineFREE->GetXaxis()->SetTitle("Time [ns]");         g_baselineFREE->GetYaxis()->SetTitle("subtracted signal [mV]");

  _Signal_baselineFREE.clear();
  _Signal_baselineFREE.shrink_to_fit();
  return g_baselineFREE;
}
//**************************************************************
TGraph* do_CFD(TGraph *g, Int_t delayCFD, Float_t factorCFD){
//**************************************************************
  Double_t *_time        = g->GetX();
  Double_t *_Signal     = g->GetY();
  Int_t Nbin=g->GetN();
  std::vector<Double_t> _Signal_CFD;
  std::vector<Double_t> _time_CFD;
#ifdef INTERPOLA
//==================== Parte di interpolazione ----- Codice di Andrea =========
  std::vector<Double_t> t, v;
    for(int i=0; i<Nbin; i++) {
    v.push_back(_Signal[i]);
    t.push_back(_time[i]);
  }
  ROOT::Math::Interpolator inter(v.size(), ROOT::Math::Interpolation::kCSPLINE);
  inter.SetData(t, v);
  Int_t points = v.size() * Ninterpol;

  for (Int_t i = 0; i < points; i++)  {
    _time_CFD.push_back((Double_t)i * (t.back() - t.front()) / (points - 1) + t.front());
    _Signal_CFD.push_back(inter.Eval(_time_CFD[i]));
  }
  v.clear();  
  t.clear();
  v.shrink_to_fit();
  t.shrink_to_fit();
  //  FINE ==================== Parte di interpolazione ----- Codice di Andrea ==
  for(int i=0; i<(Nbin-delayCFD)*Ninterpol; i++)  _Signal_CFD.at(i) = _Signal_CFD.at(i)-factorCFD*_Signal_CFD.at(i+delayCFD*Ninterpol);

#else
  for(int i=0; i<(Nbin-delayCFD)*Ninterpol; i++)  {    
    _Signal_CFD.push_back(_Signal[i]-factorCFD*_Signal[i+delayCFD*Ninterpol]);
    _time_CFD.push_back(_time[i]);
  }
#endif
  TGraph *g_CFD = new TGraph(_time_CFD.size(),&(_time_CFD.at(0)),&(_Signal_CFD.at(0)));  
  g_CFD->GetXaxis()->SetRangeUser(_time_CFD.at(0),_time_CFD.at(_time_CFD.size()-1));
  g_CFD->GetXaxis()->SetTitle("Time [ns]");    g_CFD->GetYaxis()->SetTitle("CFD processed signal [mV]");

  _Signal_CFD.clear();
  _time_CFD.clear();
  _Signal_CFD.shrink_to_fit();
  _time_CFD.shrink_to_fit();
  return g_CFD;  
}
//**************************************************************
//           Time Over Threshold method
//**************************************************************
std::pair<Float_t, Float_t> eval_t0_LE(TGraph *g, Float_t c){
//**************************************************************
  Float_t t_i(0.), t_f(0), dt_tot(0);
  Double_t *_time       = g->GetX();
  Double_t *_Signal     = g->GetY();
  Int_t Nbin            = g->GetN();
  Float_t         t_min = _time[0];
  Float_t         t_max = _time[Nbin-1];
  Long64_t yminloc = TMath::LocMin(Nbin,g->GetY());  
  Long64_t ymaxloc = TMath::LocMax(Nbin,g->GetY());
  /*
  if(yminloc>ymaxloc) yminloc=0;
  for(Int_t j=yminloc; j<Nbin; j++) {
    if (_Signal[j] - c < threshold_LE ) {
      t_f = _time[j];
      break;
    }
  }
  
  for(Int_t j=yminloc; j>0; j--) {
    if (_Signal[j] - c < threshold_LE ) {
      t_i = _time[j];
      break;
    }
  }
  */
  for(Int_t j=ymaxloc; j>0; j--) {
    t_i = _time[j];
    if (_Signal[j] - c < threshold_LE ) {
      break;    
    } 
  }
  Float_t t_bin=(t_max-t_min)/float(Nbin-1);
  Float_t t_range_min = t_i-3.*t_bin;
  Float_t t_range_max = t_i+3.*t_bin;

  TString fun_name("MYpol_LE");
  TF1 *MYpol = new TF1(fun_name,polinomio,t_min,t_max,2);       MYpol->SetLineColor(2);
  MYpol->SetRange(t_range_min,t_range_max);
  //cout<<" t_i determination: LE fit in "<<t_range_min<<"-"<<t_range_max<<endl;
  Float_t prob=0.;
  for(int iloop=0; iloop<nLoop; iloop++){
    if( prob < 0.4 ){
      g->Fit(MYpol,"QR","",t_range_min,t_range_max);
      t_range_min = t_range_min+t_bin;
      t_range_max = t_range_max-t_bin;
      prob = g->GetFunction(MYpol->GetName())->GetProb();
    }
  }
  t_i =  (c + threshold_LE - MYpol->GetParameter(0))/MYpol->GetParameter(1);

  for(Int_t j=ymaxloc; j<Nbin; j++) {
    t_f = _time[j];      
    if (_Signal[j] - c < threshold_LE ) {
      break;
    }
  }

  t_range_min = t_f-6.*t_bin;
  t_range_max = t_f+6.*t_bin;
  MYpol->SetRange(t_range_min,t_range_max); 
  prob=0.;
  //cout<<" t_f determination: LE fit in "<<t_range_min<<"-"<<t_range_max<<endl;
  for(int iloop=0; iloop<nLoop; iloop++){
    if( prob < 0.4 ){
      g->Fit(MYpol,"QR","",t_range_min,t_range_max);
      t_range_min = t_range_min+t_bin;
      t_range_max = t_range_max-t_bin;
      prob = g->GetFunction(MYpol->GetName())->GetProb();
    }
  }
  t_f =  (c + threshold_LE - MYpol->GetParameter(0))/MYpol->GetParameter(1);

  return std::make_pair(t_i, t_f - t_i);      
}

//**************************************************************
Float_t eval_trigger_time(TGraph *g, Float_t threshold){
//**************************************************************
  Double_t *_time   = g->GetX();
  Double_t *_Signal = g->GetY();
  Int_t        Nbin =  g->GetN();
  // ---------------------------------------------------------
  Float_t trigger_time(-1000);
  for(Int_t j=0; j<Nbin; j++) {
    if ( _Signal[j] >  threshold)  { 
      trigger_time = _time[j];
      break;
    }
  }
    
  return trigger_time;
}


//**************************************************************
//             PSI method  (with or without baseline)
//**************************************************************
//std::pair<Float_t, Float_t> eval_t0_PSI(TGraph *g, Float_t Amplitude, Float_t c){
std::tuple<Float_t, Float_t, Float_t> eval_t0_PSI(TGraph *g, Float_t Amplitude, Float_t c){
//**************************************************************
  Double_t *_time   = g->GetX();
  Double_t *_Signal = g->GetY();
  Int_t        Nbin =  g->GetN();
  Float_t     t_min = _time[0];
  Float_t     t_max = _time[Nbin-1];
  Float_t dt=(t_max-t_min)/float(Nbin);

  Long64_t ymaxloc = TMath::LocMax(Nbin,g->GetY());
  Float_t     xmax = _time[ymaxloc];

  // ---------------------------------------------------------
  //  -- Definition of ranges
  // ---------------------------------------------------------
  Float_t t_range_min(0), t_range_max(0);
  Int_t jsave(0);
  //for(Int_t j=0; j<Nbin; j++) {
  //  if ( _Signal[j] - c >  frac_max * Amplitude ){// && _time[j] > xmax-0.5 && _time[j] < xmax  )  { 
  for(Int_t j=ymaxloc; j>0; j--) {
    if ( _Signal[j] - c <  frac_max * Amplitude ){// && _time[j] > xmax-0.5 && _time[j] < xmax  )  { 
      t_range_max = _time[j];
      jsave = j;
      break;
    }
  }
  for(Int_t j=jsave; j>0; j--) {
    if ( _Signal[j] - c <  frac_min * Amplitude )  { 
      t_range_min = _time[j];
      break;
    }    
  } //========================= End definition of ranges
  Float_t riseTime=t_range_max-t_range_min;
  //  cout<<"PSI method:  t_range_max="<<t_range_max<<" t_range_min="<<t_range_min<<" riseTime="<<riseTime<<"ps ["<<int(riseTime/(t_max-t_min)*float(Nbin-1))<<" bins]"<<endl;

  TF1 *MYpol = new TF1("MYpol",polinomio,t_min,t_max,2);       MYpol->SetLineColor(2);
  Float_t prob=0.;
  for(int iloop=0; iloop<nLoop; iloop++){
    MYpol->SetRange(t_range_min,t_range_max); 
    if( prob < 0.4 ){
      g->Fit(MYpol,"QR","",t_range_min,t_range_max);
      t_range_min = t_range_min+dt;
      t_range_max = t_range_max-dt;
      if(t_range_max-t_range_min<3*dt) break;
      prob = g->GetFunction(MYpol->GetName())->GetProb();
    }
  }

  Float_t t0_PSI = (c + frac_thr * Amplitude - MYpol->GetParameter(0))/MYpol->GetParameter(1);	
  /*
  // --------- In alternativa al fit -----
  for (Int_t j = ymaxloc; j > 0; j--)   {      
    if (_Signal[j] - c <  frac_thr * Amplitude)  {
      t0_PSI = _time[j];
      break;
    }
  }
  */
  //return std::make_pair(t0_PSI, riseTime);
  return std::make_tuple(t0_PSI, riseTime, MYpol->GetParameter(1));
  
}

//**************************************************************
//              CFD method 
//**************************************************************
std::pair<Float_t, Float_t> eval_t0_CFD(TGraph *g_CFD, Float_t T_Range ){
//**************************************************************
  Double_t *_time       = g_CFD->GetX();
  Double_t *_Signal_CFD = g_CFD->GetY();
  Int_t Nbin            = g_CFD->GetN();
  Float_t     t_min = _time[0];
  Float_t     t_max = _time[Nbin-1];
  Double_t bin=(t_max-t_min)/float(Nbin);
  // Metodo soggetto a sbagli in caso il massimo e il minimo siano nella zona di rumore
  Float_t     ymin_CFD = TMath::MinElement(Nbin,g_CFD->GetY());
  Float_t     ymax_CFD = TMath::MaxElement(Nbin,g_CFD->GetY());
  Long64_t yminloc_CFD = TMath::LocMin(Nbin,g_CFD->GetY());     
  Long64_t ymaxloc_CFD = TMath::LocMax(Nbin,g_CFD->GetY());
  
  /*
  //Stefania 2)
  // Metodo piu robusto: cerca nell'intorno di T_Range
  Float_t ymin_CFD=100;
  Long64_t yminloc_CFD;
  for(int i=0; i<int(T_Range/bin); i++)  {
    if (_Signal_CFD[i]< ymin_CFD) {
      ymin_CFD = _Signal_CFD[i];
      yminloc_CFD = i;
    }
  }
  Long64_t ymaxloc_CFD;
  Float_t ymax_CFD=-100;
  for(int i=int(T_Range*bin); i<Nbin-1;i++)  {
    if (_Signal_CFD[i] > ymax_CFD) {
      ymax_CFD = _Signal_CFD[i];
      ymaxloc_CFD = i;
    }
  }
  */
  /*
  //Stefania 3)
  // Metodo piu robusto: cerca nell'intorno di T_Range
  Float_t     ymax_CFD = TMath::MaxElement(Nbin,g_CFD->GetY());
  Long64_t ymaxloc_CFD = TMath::LocMax(Nbin,g_CFD->GetY());
  Float_t ymin_CFD=100;
  Long64_t yminloc_CFD;
  for(int i=0; i<ymaxloc_CFD; i++)  {
    if (_Signal_CFD[i]< ymin_CFD) {
      ymin_CFD = _Signal_CFD[i];
      yminloc_CFD = i;
    }
  }
  
  //  cout<<" min_CFD = ("<<yminloc_CFD<<", "<<ymin_CFD<<") ymaxloc_CFD =("<<ymaxloc_CFD<<", "<<ymax_CFD<<")"<<endl;

  if(yminloc_CFD > ymaxloc_CFD ){
    Long64_t tmp = ymaxloc_CFD;
    ymaxloc_CFD = yminloc_CFD;
    yminloc_CFD = tmp;
  }
  */
  Int_t dbin=3*Ninterpol;    // NB era 3 bin nei dati del PSI
  if(yminloc_CFD < dbin) yminloc_CFD=dbin;
  if(ymaxloc_CFD > Nbin-dbin) ymaxloc_CFD =Nbin-dbin;

  Float_t t0_CFD, t0_CFDbis, t0_maxD(t_min), maxD(-1000);
  for(Int_t j=yminloc_CFD; j< ymaxloc_CFD; j++) {
    Float_t SumPIU(0.), SumMENO(0.);
    for (int jj=0; jj<3*Ninterpol-1; jj++) {
      SumPIU+=_Signal_CFD[j+jj+1];
      SumMENO+=_Signal_CFD[j-jj-1];
    }
    if ( SumPIU-SumMENO > maxD ) { // NB era 3 bin nei dati del PSI
      maxD =SumPIU-SumMENO ;
      t0_maxD = _time[j];
    }
  }

  t0_CFD = t_max;
  for(Int_t j=ymaxloc_CFD; j> 0; j--) {
    t0_CFD = _time[j];
    Float_t SumPIU(0.), SumMENO(0.);
    for (int jj=0; jj<3*Ninterpol-1; jj++) {
      SumPIU+=_Signal_CFD[j+jj+1];
      SumMENO+=_Signal_CFD[j-jj-1];
    }
    if ( _Signal_CFD[j] < 0  && SumPIU>0 && SumMENO<0 )     break;
    
  }
  
  t0_CFDbis = t_min;
  for(Int_t j=yminloc_CFD; j<Nbin; j++) {
    t0_CFDbis = _time[j];
    Float_t SumPIU(0.), SumMENO(0.);
    for (int jj=0; jj<3*Ninterpol-1; jj++) {
      SumPIU+=_Signal_CFD[j+jj+1];
      SumMENO+=_Signal_CFD[j-jj-1];
    }
    if ( _Signal_CFD[j] > 0  && SumPIU>0 && SumMENO<0 )    break;    
  }
  //if (t0_CFDbis>t0_CFD) t0_CFDbis=t0_CFD;
  //Float_t range_tt_CFD = 0.5 * min( fabs(_time_CFD.at( ymaxloc_CFD) - tt_CFD), fabs(_time_CFD.at( yminloc_CFD) - tt_CFD) );
  Float_t range_tt_CFD = 2.*bin*Ninterpol;  // era 5 nei dati del PSI
  
  //cout<< " ---> t0_CFD="<<t0_CFD<<" "<<t0_CFDbis<<" "<<t0_maxD<<endl;
  if(t0_CFD-range_tt_CFD<t_min || t0_CFD+range_tt_CFD>t_max) return std::make_pair(-1000., -1000.); // Protezione su fit impossibili

  if(fabs(t0_CFDbis-t0_CFD)>4*bin*Ninterpol) {
    //return std::make_pair(-1000., -1000.); // Protezione su fit impossibili
    //if( fabs(t0_CFD-t0_CFDbis) > fabs(t0_CFD-t0_maxD) ) {
      if( fabs(t0_CFD-t0_maxD) < fabs(t0_CFDbis-t0_maxD) ) t0_CFD = t0_CFD;
          else t0_CFD = t0_CFDbis;
    //} else t0_CFD = (t0_CFD+t0_CFDbis)/2.;
  } else t0_CFD = (t0_CFD +t0_CFDbis)/2.;
  
  TF1* MYpol = new TF1("MYpol",polinomio,t_min,t_max,2);   MYpol->SetLineColor(2);

  Float_t prob = 0;

  for(int iloop=0; iloop<nLoop; iloop++){
    if(prob<0.4) { 
      MYpol->SetRange(t0_CFD-range_tt_CFD,t0_CFD+range_tt_CFD);	
      g_CFD->Fit(MYpol,"QR","",t0_CFD-range_tt_CFD,t0_CFD+range_tt_CFD);
      prob = g_CFD->GetFunction(MYpol->GetName())->GetProb();
      range_tt_CFD = 0.9 * range_tt_CFD;
    }
  }
  t0_CFD = -MYpol->GetParameter(0)/MYpol->GetParameter(1);
  Float_t p1 = MYpol->GetParameter(1);

  //  delete MYpol;
  return std::make_pair(t0_CFD, p1);
  
  //return std::make_pair(t0_CFD, 0);
}
//**************************************************************
//          CFD2 Method (Margherita/``default'')
//**************************************************************


std::tuple<Float_t, Float_t, Float_t, Float_t, Float_t, Float_t> eval_t0_CFD2(TGraph *g, TGraph *g_CFD, Double_t t_rise,int kk){
//**************************************************************
  Double_t *_time        = g_CFD->GetX();
  Double_t *_Signal_CFD = g_CFD->GetY();
  Int_t Nbin            = g_CFD->GetN();
  Float_t     t_min = _time[0];
  Float_t     t_max = _time[Nbin-1];
  
  Double_t *_time_g        = g->GetX();
  Double_t *_Signal_g = g->GetY();
  Int_t Nbin_g            = g->GetN();
  
  Double_t *_Signal_g_n;
  //int interval= 50; //1bin=20ps senza interpolazione
  int interval= 50;
  int first = interval;
  int last=Nbin_g-10-first;
  //Long64_t interval= 50; //1bin=20ps senza interpolazione

  _Signal_g_n= &_Signal_g[first]; //parto dall'elemento numero "first" per la ricerca del massimo
  Long64_t i_max = TMath::LocMax(last, _Signal_g_n); //indice del massimo della forma d'onda prima del CFD
    //vector<double> signal_new;
    //vector<double> time_new;
    
    
    //for(int ik=first; ik<Nbin_g-10; ik++){
    //    signal_new.push_back( *( g->GetY()+ik ) );
    //    time_new.push_back( *( g->GetX()+ik ) );
    //}
      
    //std::vector<double>::iterator result1;
    //result1 = std::max_element(signal_new.begin(),signal_new.end());
    //int ymaxloc_g_r = std::distance(signal_new.begin(), result1);
  
  int ymaxloc_g_r =i_max;

    
    //Long64_t ymaxloc_g = ymaxloc_g_r + first;
    Long64_t ymaxloc_g = ymaxloc_g_r + interval;
    
    //Long64_t bin_rise = 500; //conversione 1bin=2ps => 1ns=500bin
    //Long64_t bin_rise = t_rise*500;

    //prima prova
    int start_CFD = (ymaxloc_g-interval)*Ninterpol;
    int end_CFD = interval*Ninterpol;
    
    //seconda prova, stringo il range di ricerca a 1 ns prima del massimo
    //int start_CFD = ymaxloc_g*10-bin_rise;
    //int end_CFD = bin_rise;
    
    if(start_CFD<0) start_CFD=0;
    
    
    Double_t *_Signal_CFD_n;
    _Signal_CFD_n= &_Signal_CFD[start_CFD]; //ricerco il minimo solo attorno al massimo della waveform
    Long64_t ymaxloc_CFD_r = TMath::LocMin(end_CFD, _Signal_CFD_n);
    Long64_t ymaxloc_CFD = ymaxloc_CFD_r + start_CFD;
    
    //vector<double> signal_CFD_new;
    //vector<double> time_CFD_new;
    //for(int ik=start_CFD; ik<ymaxloc_g*Ninterpol; ik++){
    //  signal_CFD_new.push_back(*(g_CFD->GetY()+ik));
    //  time_CFD_new.push_back(*(g_CFD->GetX()+ik));
    //}
    
    //std::vector<double>::iterator result;
    //result = std::min_element(signal_CFD_new.begin(),signal_CFD_new.end());
    //int ymaxloc_CFD_r = std::distance(signal_CFD_new.begin(), result);
    //Long64_t ymaxloc_CFD = ymaxloc_CFD_r + start_CFD;
    //cout << "canale: " << kk <<endl;
    //cout << "Tempo del minimo CFD con vettori: " << _time[ymaxloc_CFD_v] << endl;
    //cout << "Tempo del minimo CFD con array: " << _time[ymaxloc_CFD] << endl;
    //cout << "Channel " << kk << endl;
    //cout << "Risetime: " << t_rise << "ns" << endl;
    //cout << "Location of max of waveform: " << ymaxloc_g << endl;
    //cout << "time of max: " << _time_g[ymaxloc_g] << "and from CFD time: " << _time[ymaxloc_g*10] << endl;
    //cout << "Search for minimum in the bin range: " << start_CFD << " ; "<< ymaxloc_g*10 << endl;
    //cout << "In nanosecond: " << _time[start_CFD] << " ; "<< _time[ymaxloc_g*10] << endl;
    //cout << endl;
    //cout << "First and last time after selection: " << time_CFD_new[0] << " ; " << time_CFD_new[2*bin_rise-1] << endl;
    //cout << "time of the minimum: " << _time[ymaxloc_CFD] <<endl; //<< " " << time_CFD_new[ymaxloc_CFD_r] <<endl;
    //cout << endl;
    
  //Long64_t ymaxloc_CFD = TMath::LocMin(Nbin-100,g_CFD->GetY());  // segni rovesciati!!!, Nbin-100 per evitare che il minimo venga trovato al limite del doominio dei tempi (100 bin = 200ps con i dati interpolati)
  
    //cout << "Range of fit: " << _time[ymaxloc_CFD-2*Ninterpol] << " " << _time[ymaxloc_CFD+2*Ninterpol] << endl;
    
    
  g_CFD->Fit("pol2","QR","",_time[ymaxloc_CFD-2*Ninterpol],_time[ymaxloc_CFD+2*Ninterpol]);
    
  float xmin_fit_CFD= -(g_CFD->GetFunction("pol2")->GetParameter(1))/(2*(g_CFD->GetFunction("pol2")->GetParameter(2)));
  float ymax_fit_CFD= g_CFD->GetFunction("pol2")->GetMinimum();
  float xmax_fit_CFD= g_CFD->GetFunction("pol2")->GetMinimumX();
  Float_t AmpCFD=ymax_fit_CFD;
  Float_t MaxCFD=xmax_fit_CFD;

  float thresh= ymax_fit_CFD/2.; //CFD like threshold at ymax/2.

  Float_t  t0_CFD;
  Long64_t yminloc_CFD = 0;
  for(Int_t j=ymaxloc_CFD; j>yminloc_CFD; j--)   {
    if (_Signal_CFD[j] > thresh)   {
      t0_CFD = _time[j];
      break;
    }
  }
  
    
  //cout<<" CFD2 calcola il t0"<<endl;
#ifdef INTERPOLA
  Float_t range_tt_CFD =  1*Ninterpol*(t_max-t_min)/float(Nbin ); // NB era 5 nei dati del PSI
#else
  Float_t range_tt_CFD =  2*(t_max-t_min)/float(Nbin );
#endif

    
  //Float_t range_tt_CFD =  (g_CFD->GetXaxis()->GetXmin()-g_CFD->GetXaxis()->GetXmax())/float(Nbin );
    
  //cout<<" CFD2 Fit con la retta"<<endl;
  TF1*MYpol = new TF1("MYpol",polinomio,t_min,t_max,2);
  MYpol->SetLineColor(2);
  MYpol->SetRange(t0_CFD-range_tt_CFD,t0_CFD+range_tt_CFD);
  g_CFD->Fit(MYpol,"QR","",t0_CFD-range_tt_CFD,t0_CFD+range_tt_CFD);
  t0_CFD = (thresh-MYpol->GetParameter(0))/MYpol->GetParameter(1);
  Float_t p1=MYpol->GetParameter(1);
  Float_t p0=MYpol->GetParameter(0);
    
  //cout<<" CFD2 t0="<<t0_CFD<<" p1="<<p1<<" fit range "<<t0_CFD-range_tt_CFD<<","<<t0_CFD+range_tt_CFD<<endl;
  //  delete MYgaussiana;
  //  delete MYpol;
  return std::make_tuple(t0_CFD, p1, AmpCFD, ymaxloc_g, xmin_fit_CFD, p0);
}


/*
std::tuple<Float_t, Float_t, Float_t, Float_t, Float_t, Float_t> eval_t0_CFD2(TGraph *g, TGraph *g_CFD, Double_t t_rise,int kk){

  Double_t *_time        = g_CFD->GetX();
  Double_t *_Signal_CFD = g_CFD->GetY();
  Int_t Nbin            = g_CFD->GetN();
  Float_t     t_min = _time[0];
  Float_t     t_max = _time[Nbin-1];
  
  Long64_t ymaxloc_CFD = TMath::LocMin(Nbin-100,g_CFD->GetY());  // segni rovesciati!!!, Nbin-100 per evitare che il minimo venga trovato al limite del doominio dei tempi (100 bin = 200ps con i dati interpolati)

  g_CFD->Fit("pol2","QR","",_time[ymaxloc_CFD-2*Ninterpol],_time[ymaxloc_CFD+2*Ninterpol]);
    
  float xmin_fit_CFD= -(g_CFD->GetFunction("pol2")->GetParameter(1))/(2*(g_CFD->GetFunction("pol2")->GetParameter(2)));
  float ymax_fit_CFD= g_CFD->GetFunction("pol2")->GetMinimum();
  float xmax_fit_CFD= g_CFD->GetFunction("pol2")->GetMinimumX();
  
  Float_t AmpCFD=ymax_fit_CFD;
  Float_t MaxCFD=xmax_fit_CFD;

  float thresh= ymax_fit_CFD/2.; //CFD like threshold at ymax/2.

  Float_t  t0_CFD;
  Long64_t yminloc_CFD = 0;
  for(Int_t j=ymaxloc_CFD; j>yminloc_CFD; j--)   {
    if (_Signal_CFD[j] > thresh)   {
      t0_CFD = _time[j];
      break;
    }
  }
  
    
  //cout<<" CFD2 calcola il t0"<<endl;
#ifdef INTERPOLA
  Float_t range_tt_CFD =  1*Ninterpol*(t_max-t_min)/float(Nbin ); // NB era 5 nei dati del PSI
#else
  Float_t range_tt_CFD =  2*(t_max-t_min)/float(Nbin );
#endif

    
  //Float_t range_tt_CFD =  (g_CFD->GetXaxis()->GetXmin()-g_CFD->GetXaxis()->GetXmax())/float(Nbin );
    
  //cout<<" CFD2 Fit con la retta"<<endl;
  TF1*MYpol = new TF1("MYpol",polinomio,t_min,t_max,2);
  MYpol->SetLineColor(2);
  MYpol->SetRange(t0_CFD-range_tt_CFD,t0_CFD+range_tt_CFD);
  g_CFD->Fit(MYpol,"QR","",t0_CFD-range_tt_CFD,t0_CFD+range_tt_CFD);
  t0_CFD = (thresh-MYpol->GetParameter(0))/MYpol->GetParameter(1);
  Float_t p1=MYpol->GetParameter(1);
  Float_t p0=MYpol->GetParameter(0);
    
  //cout<<" CFD2 t0="<<t0_CFD<<" p1="<<p1<<" fit range "<<t0_CFD-range_tt_CFD<<","<<t0_CFD+range_tt_CFD<<endl;
  //  delete MYgaussiana;
  //  delete MYpol;
  return std::make_tuple(t0_CFD, p1, AmpCFD, ymaxloc_CFD, xmin_fit_CFD, p0);
}
*/

TH1D* do_FFT(std::vector<Float_t> *_Signal, Double_t Filter[N], Double_t rrACa[N], Double_t iiACa[N], Int_t NN, Float_t t_min, Float_t t_max){

  TString hname("hRev");
  TString htitle("deconvoluted shape");

  delete gROOT->FindObject(hname);
  TH1D *hRev = new TH1D(hname, htitle+hname, NN, t_min, t_max);  hRev->SetLineColor(2);	  

  Double_t *re_dec = new Double_t[NN];
  Double_t *im_dec = new Double_t[NN];

  // Compute the FFT transform
  TString opt("R2C P");
  TVirtualFFT *fft= TVirtualFFT::FFT(1, &NN, opt);
  //  cout << _Signal->at(0) << endl;
  for (Int_t i=0;   i<NN;  i++) fft->SetPoint(i,_Signal->at(i));  
  fft->Transform();	
    
  Double_t re,im;
  for (Int_t j=0; j<NN; j++) {
    Double_t den = 1.;
#ifdef CAVO
    den = sqrt(rrACa[j]*rrACa[j] + iiACa[j]*iiACa[j]);  // cable correction
#endif 	  
    fft->GetPointComplex(j,re,im);
    re_dec[j] = Filter[j] * re / den;
    im_dec[j] = Filter[j] * im / den;
    //    cout<<"Re=" <<re_dec[j]<<" Im="<<im_dec[j]<<" den="<<den<<endl; 
  }		

  TVirtualFFT *fft_back = TVirtualFFT::FFT(1, &NN, "C2R P"); 
  fft_back->SetPointsComplex(re_dec, im_dec);
  fft_back->Transform();
  //-----------------------------------------------------------------------------------------------------------------
  for(Int_t i=0; i<NN; i++) {
    //    cout<<" Signal="<<_Signal->at(i);	
    hRev->SetBinContent(i+1, fft_back->GetPointReal(i)*(1./NN));
    _Signal->at(i) = fft_back->GetPointReal(i)*(1./NN);
    //    cout<<" ////// "<<_Signal->at(i)<<endl;	
  }
  delete[] re_dec;
  delete[] im_dec;
  //delete fft;
  //delete fft_back;
  return hRev;
  
  
}

TH1D* do_FFT_spettro(std::vector<Float_t> *_Signal, Double_t Filter[N], Double_t rrACa[N], Double_t iiACa[N], Int_t NN, Float_t t_min, Float_t t_max){

  TString hname("hRev");
  TString htitle("deconvoluted shape");

  delete gROOT->FindObject(hname);
  TH1D *hRev = new TH1D(hname, htitle+hname, NN, t_min, t_max);  hRev->SetLineColor(2);	  

  Double_t *re_dec = new Double_t[NN];
  Double_t *im_dec = new Double_t[NN];

  // Compute the FFT transform
  TString opt("R2C P");
  TVirtualFFT *fft= TVirtualFFT::FFT(1, &NN, opt);
  for (Int_t i=0;   i<NN;  i++) fft->SetPoint(i,_Signal->at(i));  
  fft->Transform();	
    
  Double_t re,im;
  for (Int_t j=0; j<NN; j++) {
    Double_t den = 1.;
#ifdef CAVO
    den = sqrt(rrACa[j]*rrACa[j] + iiACa[j]*iiACa[j]);  // cable correction
#endif 	  
    fft->GetPointComplex(j,re,im);
    re_dec[j] = Filter[j] * re / den;
    im_dec[j] = Filter[j] * im / den;
    //    cout<<"Re=" <<re_dec[j]<<" Im="<<im_dec[j]<<" den="<<den<<endl; 
  }		

  TVirtualFFT *fft_back = TVirtualFFT::FFT(1, &NN, "C2R P"); 
  fft_back->SetPointsComplex(re_dec, im_dec);
  fft_back->Transform();
  //-----------------------------------------------------------------------------------------------------------------
  for(Int_t i=0; i<NN; i++) {
    //    cout<<" Signal="<<_Signal->at(i);	
    hRev->SetBinContent(i+1, fft_back->GetPointReal(i)*(1./NN));
    _Signal->at(i) = fft_back->GetPointReal(i)*(1./NN);
    //    cout<<" ////// "<<_Signal->at(i)<<endl;	
  }
  delete[] re_dec;
  delete[] im_dec;
  //delete fft;
  //delete fft_back;
  return hRev;
  
  
}/*     Metodo alternativo (ma identici risultati) per il calcolo della FFT
 	Double_t *re_full = new Double_t[NN];
 	Double_t *im_full = new Double_t[NN];
 	Double_t *re_dec  = new Double_t[NN];
 	Double_t *im_dec  = new Double_t[NN];
*/

