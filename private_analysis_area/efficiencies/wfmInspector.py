import ROOT
import os
import sys
import numpy as np

DUT_CH = 2 # Channels go from 1 to 4
DEL_CFD = 4 # points

NEWTREE_ROOTFILES = [
    '../data/newTree_RefCurve_2024-05-03_1_023204_0.root'
]
WFM_ROOTFILES_PATH = '../../../analysis_area/data/'
    
NEWTREE_CUT = 'Amp[3]<230&&Amp[0]>40&&Amp[2]>40&&Amp[1]>25'

N_EVENTS = -1
N_WFMS = 1
N_SKIP = 2

PLOT_TIMES = True

def makeWfmPlot(plot_type, canvas, wfm_graphs, wfm_tlines, wfm_cfd_graphs, wfm_cfd_tlines):
    
    if plot_type == 'spline':
        wfms = wfm_graphs
        tlines = wfm_tlines
        other_tlines = wfm_cfd_tlines
        title = 'Waveforms'
    elif plot_type == 'cfd':
        wfms = wfm_cfd_graphs
        tlines = wfm_cfd_tlines
        other_tlines = wfm_tlines
        title = 'CFD Waveforms'
    else:
        print('Error: plot type not recognized')
        return

    wfms.SetTitle(title)
    wfms.GetXaxis().SetTitle('Time [ns]')
    wfms.GetYaxis().SetTitle('Signal [mV]')
    wfms.Draw('A pmc plc')
    canvas.BuildLegend()

    if PLOT_TIMES:
        ymin = wfms.GetYaxis().GetXmin()
        ymax = wfms.GetYaxis().GetXmax()
        if plot_type == 'spline':
            for tline in tlines:
                tline.SetY1(ymin)
                tline.SetY2(ymax)
                tline.Draw()
                for pr in canvas.GetListOfPrimitives():
                    if isinstance(pr, ROOT.TLegend) and N_WFMS == 1:
                        pr.AddEntry(tline, 't spline', 'l')
            if N_WFMS == 1:
                # Add line to the legend
                for other_tline in other_tlines:
                    other_tline.SetY1(ymin)
                    other_tline.SetY2(ymax)
                    other_tline.Draw()
                    for pr in canvas.GetListOfPrimitives():
                        if isinstance(pr, ROOT.TLegend):
                            pr.AddEntry(other_tline, 't CFD', 'l')
        else:
            if N_WFMS == 1:
                # Add line to the legend
                for other_tline in other_tlines:
                    other_tline.SetY1(ymin)
                    other_tline.SetY2(ymax)
                    other_tline.Draw()
                    for pr in canvas.GetListOfPrimitives():
                        if isinstance(pr, ROOT.TLegend):
                            pr.AddEntry(other_tline, 't spline', 'l')
            for tline in tlines:
                tline.SetY1(ymin)
                tline.SetY2(ymax)
                tline.Draw()
                for pr in canvas.GetListOfPrimitives():
                    if isinstance(pr, ROOT.TLegend) and N_WFMS == 1:
                        pr.AddEntry(tline, 't CFD', 'l')
    canvas.Update()

if __name__ == '__main__':

    # Create the list of wfm files associated with the newTree files
    wfm_files = []
    for f in NEWTREE_ROOTFILES:
        newtree_file = os.path.basename(f)
        # Get the wfm file name from the newtree file name
        wfm_file = newtree_file.replace('newTree', 'treeFile').replace('_0.root', '.root')
        wfm_files.append(os.path.join(WFM_ROOTFILES_PATH, wfm_file))
    
    # Check that all files exist
    for f in NEWTREE_ROOTFILES:
        if not os.path.exists(f):
            print(f'Error: file {f} does not exist')
            sys.exit(1)
    for f in wfm_files:
        if not os.path.exists(f):
            print(f'Error: file {f} does not exist')
            sys.exit(1)

    # Open all the NEWTREE_ROOTFILES in a TChain
    nTree = ROOT.TChain('newTree')
    for f in NEWTREE_ROOTFILES:
        nTree.Add(f)

    # Open all the wfm_rootfiles in a TChain
    wfmTree = ROOT.TChain('tree')
    for f in wfm_files:
        wfmTree.Add(f)

    # Turn the cut in an expression that can be evaluated by the TTree
    cut = ROOT.TTreeFormula('cut', NEWTREE_CUT, nTree)
    # This is needed to properly initialize the TTreeFormula
    cut.GetNdata()

    # List of passing event numbers
    selected_events = []
    cfd_times = []
    spline_times = []

    # Loop over the events
    for i, event in enumerate(nTree):
        if i < N_SKIP:
            continue

        if i == N_EVENTS+N_SKIP:
            break

        # Evaluate the cut
        if cut.EvalInstance(i):
            selected_events.append(i)
            cfd_times.append(event.tt_CFD[DUT_CH-1])
            spline_times.append(event.tt[DUT_CH-1])

    print(f'Found {len(selected_events)} events passing the cut')

    wfm_graphs = ROOT.TMultiGraph()
    wfm_cfd_graphs = ROOT.TMultiGraph()
    
    wfm_tlines = []
    wfm_cfd_tlines = []

    for i, ev_num in enumerate(selected_events):
        if i == N_WFMS:
            break

        # Get the data for the selected event
        wfmTree.GetEntry(ev_num)
        time_vec = np.array(wfmTree.time, dtype=np.float64)
        signal_vec = np.array(getattr(wfmTree,f'Signal_{DUT_CH}'), dtype=np.float64)

        # Create the waveform graph
        g = ROOT.TGraph(len(time_vec),time_vec, signal_vec)
        g.SetTitle(f'Event {ev_num}')
        g.SetMarkerStyle(8)
        g.SetMarkerSize(.6)
        wfm_graphs.Add(g,'PL')

        # Create the CFD graph
        # signal at i = signal at i - signal at i+DEL_CFD, remove the last DEL_CFD points
        signal_cfd_vec = signal_vec[:-DEL_CFD] - signal_vec[DEL_CFD:]

        # Create the waveform graph
        g_cfd = ROOT.TGraph(len(signal_cfd_vec),time_vec[:-DEL_CFD], signal_cfd_vec)
        g_cfd.SetTitle(f'Event {ev_num}')
        g_cfd.SetMarkerStyle(8)
        g_cfd.SetMarkerSize(.6)
        wfm_cfd_graphs.Add(g_cfd,'PL')

        if PLOT_TIMES:
            # Create the time lines
            tline = ROOT.TLine(spline_times[i], -100, spline_times[i], 100)
            if N_WFMS > 1:
                tline.SetLineColor(g.GetMarkerColor())
            else:
                tline.SetLineColor(ROOT.kRed)
            tline.SetLineWidth(2)
            wfm_tlines.append(tline)

            tline_cfd = ROOT.TLine(cfd_times[i], -100, cfd_times[i], 100)
            if N_WFMS > 1:
                tline_cfd.SetLineColor(g_cfd.GetMarkerColor())
            else:
                tline_cfd.SetLineColor(ROOT.kBlue)
            tline_cfd.SetLineWidth(2)
            wfm_cfd_tlines.append(tline_cfd)

    # Create the wfm plot
    c = ROOT.TCanvas('cWfm', 'cWfm', 800, 600)
    makeWfmPlot('spline', c, wfm_graphs, wfm_tlines, wfm_cfd_graphs, wfm_cfd_tlines)
    c.Draw()

    # Create the CFD wfm plot
    c_cfd = ROOT.TCanvas('cWfmCFD', 'cWfmCFD', 800, 600)
    makeWfmPlot('cfd', c_cfd, wfm_graphs, wfm_tlines, wfm_cfd_graphs, wfm_cfd_tlines)
    c_cfd.Draw()

    input('Press enter to continue')