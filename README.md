# TB-tools

These tools have been originally developed for the TimeSPOT 2024 test beam.

## Installation instructions

The tools were developed and tested in Python 3.9.18 and Python 3.10.12 on a Ubuntu 22.04 and EL9 CERN Lxplus machine.

These applications require Python virtual environment and tkinter to be installed. Also, for the waveform processing, ROOT should be available.

To install the tools, you have to clone this repository, create a python virtual environment, and install the dependencies:
```bash
git clone https://gitlab.cern.ch/abellora/tb-tools.git
cd tb-tools
python3 -m venv .venv --system-site-packages
source .venv/bin/activate
pip install -r requirements.txt
```

The directories where waveform files are produced and waveform files need to be accessible. A guide to mount shared directories (from Windows) on a Ubuntu VM is also provided at the bottom.

## Running the tools

The tools must be executed within the virtual environment. </br>
To activate it, use:
```bash
source .venv/bin/activate
```
In case you need to deactivate it, just type the `deactivate` command.

### Automatic waveform processing

The script `--help` function lists all the command lines options that can be passed:
```
usage: monitorAndMakeTrees.py [-h] [--debug] [--check-rate CHECK_RATE] [--max-procs MAX_PROCS] [--analysis-dir ANALYSIS_DIR] [--log-dir LOG_DIR] [--wfms-dir WFMS_DIR]

Monitor waveform files produced by the oscilloscope and process them into trees.

optional arguments:
  -h, --help            show this help message and exit
  --debug               Enable debug mode.
  --check-rate CHECK_RATE
                        Seconds between file checks. Default: 30
  --max-procs MAX_PROCS
                        Maximum number of processes to run at the same time. Default: 2
  --analysis-dir ANALYSIS_DIR
                        Path to the folder containing the analysis_scripts and data sub-folders (where trees are stored). Default:
                        /afs/cern.ch/user/a/abellora/workarea/Work/TIMESPOT/timespot-tb-sps-2024-04/analysis_area
  --log-dir LOG_DIR     Path to the folder where all the logs are. Default: /afs/cern.ch/user/a/abellora/workarea/Work/TIMESPOT/timespot-tb-sps-2024-04/TS_macro/logs
  --wfms-dir WFMS_DIR   Path to the folder where all the waveform files are. Default: /afs/cern.ch/user/a/abellora/workarea/Work/TIMESPOT/timespot-tb-
                        sps-2024-04/analysis_area

Made by A. Bellora - 02/05/2024
``` 

Default parameters are defined as global variables at the beginning of the script.

**Important**

The monitor can also be submitted to HTCondor in lxplus. It can be achieved via `condor_submit monitorAndMakeTrees.sub`. </br>
The batch job calls `monitorAndMakeTrees.sh` and runs for a week. Make sure to update `monitorAndMakeTrees.sub` with the right path to the script (`argument` field)

### Mattermost alarm

The script `--help` function lists all the command lines options that can be passed:

```
usage: mattermostNotifier.py [-h] [--debug] [--webhook WEBHOOK] [--wfm-dir WFM_DIR] [--msg-interval MSG_INTERVAL] [--check-interval CHECK_INTERVAL]
                             [--events-to-completion EVENTS_TO_COMPLETION] [--log-file LOG_FILE]

Monitor a directory for waveform files and send alarms to a mattermost channel when the number of events is almost reached.

optional arguments:
  -h, --help            show this help message and exit
  --debug               Enable debug mode
  --webhook WEBHOOK     The webhook to use
  --wfm-dir WFM_DIR     The directory to watch for Wfm files
  --msg-interval MSG_INTERVAL
                        The interval to spam the messages
  --check-interval CHECK_INTERVAL
                        The interval to check the number of events
  --events-to-completion EVENTS_TO_COMPLETION
                        The number of events to completion
  --log-file LOG_FILE   The log file to write to

Made by A. Bellora - 04/05/2024
```
Default parameters are defined as global variables at the beginning of the script.

**Notable items:**
- The `countWfms`, which counts the number of waveforms in a file, can be configured in multiple ways to be more/less precise and fast
- The incoming webhook should be created in Mattermost and then propagated to the application (see [Mattermos docs](https://developers.mattermost.com/integrate/webhooks/incoming/)) 

### Mounting shared directories in a Ubuntu VM

With the following steps I could mount a folder shared in the local network by the oscilloscope on the Ubuntu VM running on VirtualBox:

1. Launch the VM and open the `Shared Folders Settings`.

![docs/step1.png](docs/step1.png)

2. Create a directory in the VM and add a new shared directory, selecting the one you want to be mounted.

![docs/step2.png](docs/step2.png)

3. Define the mount point as the path to the directory in the VM created in point 2. and make the mounting permanent (it will be preserved after a reboot).

![docs/step3.png](docs/step3.png)

## Running the analysis

This part is completely derived from the already existing analysis tools. All credits to the TIMESPOT team!

### Setting up the area

For data safety reasons, I have temporarily created a private analysis area. 
To run any analysis tool, you will have to link the `data` folder (the one with all the tree ROOT files) in the `private_analysis_area` directory.

To do that, do:

```
cd private_analysis_area
ln -s <path-to-data-folder> data
```

### Running the efficiency analysis

To run the efficiency analysis, go into the `private_analysis_area/efficiencies` directory.

You can run the efficiency tools by opening a ROOT interactive session (`root -l`) and:

```
.L graphs2022.C
Data d=getdata(<datId>)
get_efficiency(d)
```

where `datId` is one of the identifiers defined in [graphs2022.C](https://gitlab.cern.ch/abellora/tb-tools/-/blob/master/private_analysis_area/efficiencies/graphs2022.C?ref_type=heads#L9-14)

### Producing the plots

I prepared a macro to run all the points for the 1e17 and 5e16 strips at 0°, and plot the efficiency vs. voltage.

To do that, follow these two steps:

1. Move to the `private_analysis_area/efficiencies` directory.
2. Run the python script `python3 runEfficiency.py`
    - Remember that you have to re-activate the python virtual environment every time you log out and back in (see the [installation instructions](https://gitlab.cern.ch/abellora/tb-tools#installation-instructions)).

The script runEfficiency.py is fully commented and quite self-explanatory in its steps. Ask [me](mailto:andrea.bellora@cern.ch) if you need any further explanations!